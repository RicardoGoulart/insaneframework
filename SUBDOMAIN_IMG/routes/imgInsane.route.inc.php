<?php
// Vamos primeiro partir o URL
$pieces = explode( "/", trim( $_SERVER['REQUEST_URI'], "/" ) );
$decoded = array();
$encoded = array();
if( !empty( $pieces ) ):
  foreach( $pieces as $key => $piece ):
    $decoded[] = insaneJS::parseJSRoute( $piece );
    $encoded[] = $piece;
  endforeach;
endif;

if( empty( $encoded ) ):
  insaneLogger::log( "Error:: Encoded was empty" );
  die();
endif;

require_once dirname( __DIR__  ) . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'parser.img.class.inc.php';

insaneImage::registerImage( 
  $encoded[0], 
  ( count( $encoded ) > 1 ? $encoded[1] : null ), 
  implode( DIRECTORY_SEPARATOR, array( insaneCache::$cacheFolder, insaneCache::$uriPath ) ), 
  implode( DIRECTORY_SEPARATOR, array( insaneCache::$cacheFolder, insaneCache::$uriPath, insaneCache::$uri . ".cache" ) ) 
);

header( 'Content-type: image/png', true );

if( !insaneCache::showCacheFile() ):
  insaneImage::parseImage();
  insaneImage::printImage();
endif;
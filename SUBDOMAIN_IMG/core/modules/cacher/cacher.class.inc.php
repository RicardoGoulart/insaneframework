<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year
 * @since     3.0.0
 * @version   4.0.1
 * 
 * 
 * 
 * @todo      Write crontab to clean the cache files!
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneRouter" ), __FILE__ );

class insaneCache {
  /**
   * $insane
   *
   * @var string
   */
  private static $insane = '_TIMWE_';
  /**
   * $isSSL
   *
   * @var boolean
   */
  public static $isSSL = false;
  /**
   * $gZip
   *
   * @var boolean
   */
  public static $gZip  = false;
  /**
   * $url
   *
   * @var string
   */
  public static $url   = false;
  /**
   * $uri
   *
   * @var string
   */
  public static $uri   = false;
  /**
   * $post
   *
   * @var array
   */
  public static $post  = false;
  /**
   * $get
   *
   * @var array
   */
  public static $get   = false;
  /**
   * $refer
   *
   * @var string
   */
  public static $refer = false;
  /**
   * $year
   *
   * @var integer
   */
  public static $year  = false;
  /**
   * $month
   *
   * @var integer
   */
  public static $month = false;
  /**
   * $session
   *
   * @var mixed
   */
  public static $session = null;
  /**
   * $cTime Cache time defined in seconds
   *
   * @var integer
   */
  public static $cTime = 30;

  /**
   * $cacheVarName
   *
   * @var string
   */
  private static $cacheVarName = 'cx3';

  /**
   * $cacheFolder
   *
   * @var string
   */
  public static $cacheFolder = 'cache';

  /**
   * $uriPath
   *
   * @var string
   */
  public static $uriPath = null;

  /**
   * $compositeUri
   *
   * @var string
   */
  public static $compositeUri = null;

  /**
   * $fileTime
   *
   * @var integer
   */
  private static $fileTime = 0;

  /**
   * __construct
   *
   * @return void
   */
  public function __construct(){
    insaneLogger::log( "insaneCache Class initiated! " );
    
    self::$isSSL    = $_SERVER['SERVER_PORT'] == "443" ? true : false;
    self::$gZip     = isset( $_SERVER['HTTP_ACCEPT_ENCODING'] ) ? strstr( $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip' ) ? true : false : false;
    self::$gZip     = insaneConfig::STAGE === false ? false : self::$gZip;
    self::$url      = (string) $_SERVER['REQUEST_URI'];
    self::$post     = !empty( $_POST ) ? md5( serialize( $_POST ) ) : false;
    self::$get      = !empty( $_GET ) ? md5( serialize( $_GET ) ) : false;
    self::$refer    = isset( $_SERVER["HTTP_REFERER"] ) ? true : false;
    self::$year     = (int) date( "Y" );
    self::$month    = (int) date( "m" );
    self::$session  = ( insaneConfig::STAGE ? 'STAGE' : 'DEV' ) . session_id();
  }

  /**
   * configure
   *
   * @param mixed $cacheFolderDirectory
   * @param mixed $cacheTime
   * @return void
   */
  public static function configure( $cacheFolderDirectory = null, $cacheTime = null ){
    self::$cTime    = (int) self::getCacheTime( $cacheTime );

    self::setUpCachePath( $cacheFolderDirectory );
    self::buildCacheURL();
  }

  /**
   * checkCacheFile
   *
   * @return boolean
   */
  private static function checkCacheFile(){
    insaneLogger::log( "Checking if cache file exists." );
    if( !empty( self::$compositeUri ) ):
      if( file_exists( self::$compositeUri ) ):
        insaneRouter::$route['cache_fileSize'] = filesize( self::$compositeUri );
        insaneLogger::log( "Cache file exists." );
        return true;
      else:
        insaneLogger::log( "Cache file dosent exist." );
        return false;
      endif;
    else:
      insaneLogger::log( "No composite URL for the current file URI. Cant get the Path straigt." );
      return false;
    endif;
  }

  /**
   * checkCacheTime
   *
   * @return boolean
   */
  private static function checkCacheTime(){
    insaneLogger::log( "Checking the file cache time" );
    if( !empty( self::$compositeUri ) ):
      if( self::checkCacheFile() ):
        insaneLogger::log( "Reading the file creation date." );
        self::$fileTime = filemtime( self::$compositeUri );
        if( self::$fileTime && self::$fileTime > 0 ):
          insaneLogger::log( "Obtained: " . self::$fileTime );
          insaneLogger::log( "Comparing the file time with the max cache time (lifetime in seconds) of: " . self::$cTime );
          if( self::$fileTime + self::$cTime >= time() ):
            insaneRouter::$route['cache_fileTime'] = self::$fileTime;
            insaneRouter::$route['cache_validity'] = ( insaneRouter::$route['cache_fileTime'] + insaneRouter::$route['cache'] ) - time();
            insaneRouter::$route['cache_expiry']   = insaneRouter::$route['cache'] + insaneRouter::$route['cache_fileTime'];

            insaneLogger::log( "Cache is valid! File can be shown." );
            return true;
          else:
            insaneLogger::log( "Cache is invalid. Generate new one!");
            insaneRouter::$route['cache_fileTime'] = time();
            return false;
          endif;
        else:
          insaneLogger::log( "Was not able to read file time. Aborting." );
          return false;
        endif;
      else:
        insaneRouter::$route['cache_fileTime'] = time();
        insaneLogger::log( "Can't check the file time because the cache file dosent exist." );
        return false;
      endif;
    else:
      insaneLogger::log( "No composite URL for the current file URI. Cant get the Path straigt." );
      return false;
    endif;
  }

  /**
   * validateCache
   *
   * @return boolean
   */
  public static function validateCache(){
    if( self::checkCacheTime() ):
      return true;
    else:
      return false;
    endif;
  }

  /**
   * setCacheFile
   *
   * @param string $contents
   * @return boolean
   */
  public static function saveCacheFile( $contents = null ){
    insaneLogger::log( "Saving contents into a cache file..." );
    if( !empty( $contents ) && is_string( $contents ) ):
      insaneLogger::log( "Content is valid for saving..." );
      insaneLogger::log( "Checking directory..." );
      if( is_dir( self::$cacheFolder . DIRECTORY_SEPARATOR . self::$uriPath ) ):
        insaneLogger::log( "Directory exists." );
      else:
        insaneLogger::log( "Directory non existing, creating..." );
        // Creating directory recursively
        if( mkdir( self::$cacheFolder . DIRECTORY_SEPARATOR . self::$uriPath, 0775, true ) ):
          insaneLogger::log( "Folder structure created successfully." );
        else:
          insaneLogger::log( "Failed to create the necessary folder structure. Aborting..." );
        endif;
        insaneLogger::log( "Going to start the attempt to create of the file now." );
      endif;
      return self::createCacheFile( $contents );
    else:
      insaneLogger::log( "Could not save the cache into a file, because the contents where empty, or wherent a string." );
      return false;
    endif;  
  }

  /**
   * showCacheFile
   *
   * @return void
   */
  public static function showCacheFile( $cacheMiss = false ){
    if( self::validateCache() ):
      // Show cache
      header( "ETag: \"" . self::$uri . "\"", true );
      header( "Content-Length: " . insaneRouter::$route['cache_fileSize'], true );
      header( "Expires: " . gmdate( 'D, d M Y H:i:s', insaneRouter::$route['cache_expiry'] ) . ' GMT', true );
      header( "Last-Modified: " . gmdate( 'D, d M Y H:i:s', insaneRouter::$route["cache_fileTime"] ) . ' GMT', true );
      header( "Cache-Control: store, cache, post-check=0, pre-check=0, max-age=" . insaneRouter::$route["cache"], true );
      header( "Insane-Efficiency: " . date( "H:i:s", insaneRouter::$route["cache_validity"] ), true );
      if( $cacheMiss ):
        header( "Insane-Status: Generated", true );
      else:
        header( "Insane-Status: Cached", true );        
      endif;
      $buffer = file_get_contents( self::$compositeUri );
      echo $buffer;
      return true;
    else:
      // Cant show this cache
      return false;
    endif;
  }

  /**
   * buildCacheURL
   *
   * @return void
   */
  private static function buildCacheURL(){
    self::$uri = array();
    insaneLogger::log( "Building cache file encoded URL." );

    $pieces = array(
      array(
        'index'     => self::$isSSL,
        'present'   => "SECURE-by-SSL",
        'absent'    => "INSECURE",
        'read'      => "SSL Support"
      ),
      array(
        'index'     => self::$gZip,
        'present'   => "COMPRESSED-by-GunZip",
        'absent'    => "UNCOMPRESSED",
        'read'      => "GunZip compression"
      ),
      array(
        'index'     => self::$url,
        'present'   => self::$url,
        'absent'    => "EMPTY-URL",
        'read'      => "Requested Url"
      ),
      array(
        'index'     => self::$post,
        'present'   => self::$post,
        'absent'    => "EMPTY-POST",
        'read'      => "Request POST Variables"
      ),
      array(
        'index'     => self::$get,
        'present'   => self::$get,
        'absent'    => "EMPTY-GET",
        'read'      => "Request GET Variables"
      ),
      array(
        'index'     => self::$refer,
        'present'   => "REFERED",
        'absent'    => "EMPTY-REFERER",
        'read'      => "Request Referer"
      ),
      array(
        'index'     => self::$year,
        'present'   => self::$year,
        'absent'    => "NO-YEAR-ON-SYSTEM",
        'read'      => "Current Year"
      ),
      array(
        'index'     => self::$month,
        'present'   => self::$month,
        'absent'    => "NO-MONTH-ON-SYSTEM",
        'read'      => "Current Month"
      ),
      array(
        'index'     => self::$session,
        'present'   => self::$session,
        'absent'    => "NO-USER-SESSION",
        'read'      => "User Session"
      )
    );

    foreach( $pieces as $key => $value ):
      if( $value['index'] ):
        array_push( self::$uri, $value['present'] );
      else:
        array_push( self::$uri, $value['absent'] );
      endif;
      insaneLogger::log( "Checking: " . $value['read'] );
    endforeach;

    array_push( self::$uri, self::$insane );

    insaneLogger::log( "Generated URI: " . implode( "_", self::$uri ) );

    self::$uri = md5( implode( "_", self::$uri ) );
    insaneLogger::log( "Encoded URI: " . self::$uri );

    self::$uriPath = substr( self::$uri, 0, 2 ) . DIRECTORY_SEPARATOR . substr( self::$uri, 2, 2 );
    insaneLogger::log( "URI Path: " . self::$uriPath );

    self::$compositeUri = self::$cacheFolder . DIRECTORY_SEPARATOR . self::$uriPath . DIRECTORY_SEPARATOR . self::$uri . ".cache";
    insaneLogger::log( "Composite URI: " . self::$compositeUri );
  }

  /**
   * getCacheTime
   *
   * @param mixed $time
   * @return int
   */
  private static function getCacheTime( $time = false ){
    if( insaneConfig::STAGE === false ):
      return 5;
    endif;

    // Return if we got something.
    if( !empty( $time ) && is_numeric( $time ) && $time > 0 ):
      return $time;
    endif;

    insaneLogger::log( "Searching for cache max time variables..." );

    // Search in these containers for the cookie time 
    $places = array( $_GET, $_POST, $_COOKIE );
    // Loop them, because I dont like copy-paste things..
    foreach( $places as $key => $place ):
      if( !empty( $place ) ):
        if( isset( $place[self::$cacheVarName] ) && !empty( $place[self::$cacheVarName] ) ):
          if( is_numeric( $place[self::$cacheVarName] ) ):
            return $place[self::$cacheVarName];
          endif;
        endif;
      endif;
    endforeach;

    return self::$cTime;
  }

  /**
   * setUpCachePath
   *
   * @param mixed $baseDir
   * @return boolean
   */
  private static function setUpCachePath( $baseDir = null ){
    if( !empty( $baseDir ) ):
      self::$cacheFolder = rtrim( $baseDir, DIRECTORY_SEPARATOR ) . DIRECTORY_SEPARATOR . self::$cacheFolder;
      insaneLogger::log( "Setting up cache path to: " . self::$cacheFolder );
      return true;
    else:
      insaneLogger::log( "Unable to set base dir path. No path was provided." );
      return false;
    endif;
  }

  /**
   * createCacheFile
   *
   * @param string $contents
   * @return boolean
   */
  private static function createCacheFile( $contents = null ){
    insaneLogger::log( "Attempting to create the cache file." );
    if( !empty( $contents ) && file_exists( self::$cacheFolder . DIRECTORY_SEPARATOR . self::$uriPath ) ):
      insaneLogger::log( "Checking if the file exists..." );
      if( file_exists( self::$compositeUri ) ):
        insaneLogger::log( "The cache file exists. Removing it now..." );
        if( unlink( self::$compositeUri ) ):
          insaneLogger::log( "File was removed successfully." );
        else:
          insaneLogger::log( "Error while removing the file. Aborting..." );
          return false;
        endif;
      else:
        insaneLogger::log( "The cache file does not exist." );
      endif;

      //  Create the file with contents.
      insaneLogger::log( "Creating the file now..." );
      $fileHandler = fopen( self::$compositeUri, "w+" );
      insaneLogger::log( "File created!" );
      if( self::$gZip === true && insaneConfig::STAGE === true ):
        fwrite( $fileHandler, gzencode( $contents, 5, FORCE_GZIP ) );
      else:
        fwrite( $fileHandler, $contents );
      endif;
      insaneLogger::log( "File contents updated!" );
      fclose( $fileHandler );
      insaneLogger::log( "File handler closed the connection!" );
      return true;
    else:
      insaneLogger::log( "Cache folder structure does not exist at this point, and it should..." );
      return false;
    endif;
  }
}

new insaneCache();
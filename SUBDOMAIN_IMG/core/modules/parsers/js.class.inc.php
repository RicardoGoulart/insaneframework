<?php

use Tholu\Packer\Packer;
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;
  
// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneMinifier" ), __FILE__ );

class insaneJS {
  /**
   * $paths
   *
   * @var array
   */
  private static $paths = array();

  /**
   * $hasPacker
   *
   * @var boolean
   */
  private static $hasPacker = false;  

  /**
   * __construct
   *
   * @return void
   */
  public function __construct(){
    // Try to include Packer
    if( file_exists( dirname( dirname( __DIR__ ) ) . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'Packer.php' ) ):
      require_once dirname( dirname( __DIR__ ) ) . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'Packer.php';
      self::$hasPacker = true;
    endif;
  }

  /**
   * parser
   *
   * @param mixed $contents
   * @return void
   */
  private static function parser( $contents = null, $skipPacker = false ){
    if( !empty( $contents ) && self::$hasPacker === true && !$skipPacker ):
      $packer = new Packer( insaneMinifier::minifyJS( $contents ), 'Normal', true, false, true );
      return $packer->pack() . ";";
    elseif( !empty( $contents ) ):
      return insaneMinifier::minifyJS( $contents );
    else:
      insaneLogger::log( "Provided JS content was empty" );
      return "";
    endif;
  }
  
  /**
   * parseScript
   *
   * @param mixed $code
   * @return void
   */
  public static function parseScript( $code = null ){
    return self::parser( $code );
  }

  /**
   * getJSRoute
   *
   * @param mixed $path
   * @return void
   */
  public static function getJSRoute( $path = null, $final = false ){
    if( !empty( $path ) ):
      return ( $final ? insaneConfig::$jsProvider : '' ) . strrev( base64_encode( $path ) );
    else:
      insaneLogger::log( "No path provided for JS ROUTES." );
      return "NO-PATH";
    endif;
  }

  /**
   * parseJSRoute
   *
   * @param mixed $path
   * @return void
   */
  public static function parseJSRoute( $path = null ){
    if( !empty( $path ) ):
      return base64_decode( strrev( $path ) );
    else:
      insaneLogger::log( "No path provided for JS ROUTES." );
      return false;
    endif;
  }

  /**
   * Parses the JS
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     4.1.1
   * @param       string $parseKey
   * @since       1.0.0
   * @return      boolean
   */
  private static function insaneMediaParser( $parseKey = null ){
    if( strstr( self::$paths[$parseKey]->url, '.min.' ) ):
      self::$paths[$parseKey]->parsed = self::$paths[$parseKey]->contents;
    else:
      self::$paths[$parseKey]->parsed = self::parser( self::$paths[$parseKey]->contents );
    endif;
    return true;
  }

  /**
   * 
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     4.1.1
   * @param       string key
   * @since       1.0.0
   * @return      boolean
   */
  public static function parseMedia( $key = null ){
    if( !empty( $key ) ):
      if( array_key_exists( $key, self::$paths ) ):
        insaneLogger::log( "Provided key $key is valid and will be parsed." );
        if( self::insaneMediaParser( $key ) ):
          insaneLogger::log( "Provided key $key was processed successfully." );
          return true;
        else:
          insaneLogger::log( "Provided key $key failed to process." );
          return false;
        endif;
      else:
        insaneLogger::log( "Provided stack key is not valid because is not present in paths stack holder." );
        return false;
      endif;
    else:
      insaneLogger::log( "Provided stack key is not valid because was empty." );
      return false;
    endif;
  }

  /**
   * Adds a script to the stack
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @param       string  url
   * @return      boolean
   */
  public static function addScript( $url = null ){
    if( !empty( $url ) ):
      if( strlen( $url ) > 3 ):
        $encoded = md5( $url );
        $content = (object) array(
          'url'       => $url,
          'encoded'   => $encoded,
          'contents'  => "",
          'parsed'    => "",
          'minified'  => "",
          'extension' => substr( $url, -3 )
        );
        // Sort by extension
        switch( substr( $url, -3 ) ):
          case '.js':
            insaneLogger::log( sprintf( "JS File found! Url: %s", $url ) );
            $content->contents = file_get_contents( $url );
            insaneLogger::log( "JS File included successfully!" );
            break;
          case 'php':
            insaneLogger::log( sprintf( "PHP File found! Url: %s", $url ) );
            ob_start();
              include( $url );
              $content->contents = ob_get_contents();
            ob_end_clean();
            insaneLogger::log( "PHP File included successfully!" );
            break;
          default:
            insaneLogger::log( sprintf( "File extension was not recognised. Url: %s Extension: %s", $url, substr( $url, -3 ) ) );
            break;
        endswitch;
        self::$paths[$encoded] = $content;
      else:
        insaneLogger::log( "Given file has a name smaller than necessary. Can't parse the file because of that..." );
        return false;
      endif;
    else:
      return false;
    endif;
  }

  /**
   * Removes a script from the stack
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @param       string  url
   * @return      boolean
   */
  public static function removeScript( $url = null ){
    if( !empty( $url ) ):
      $encoded = md5( $url );
      if( array_key_exists( self::$paths, $encoded ) ):
        unset( self::$paths[$encoded] );
        insaneLogger::log( sprintf( "Will remove the file from the stack: %s", $url ) );
        return true;
      else:
        insaneLogger::log( sprintf( "Wanted to remove the file from the stack: %s but it failed because it dosent exists.", $url ) );
        return false;
      endif;
    else:
      insaneLogger::log( "Can't remove an empty key." );
      return false;
    endif;
  }

  /**
   * Orders the execution of the parse for the styles present in current stack
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @return      boolean
   */
  public static function parseScripts(){
    if( count( array_keys( self::$paths ) ) > 0 ):
      insaneLogger::log( "Parsing scripts..." );
      foreach( self::$paths as $key => $val ):
        if( self::parseMedia( $key ) ):
          insaneLogger::log( "Finished parsing $key successfully." );
        else:
          insaneLogger::log( "Failed parsing $key." );
        endif;
      endforeach;
      insaneLogger::log( "Finished parsing scripts..." );
      return true;
    else:
      insaneLogger::log( "Request to parse was ignored because there was nothing to parse in the stack." );
      return false;
    endif;
  }

  /**
   * Returns the parsed styles after parseStyles()
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @param       boolean $minify
   * @return      boolean
   */
  public static function getScripts( $minify = false ){
    if( count( array_keys( self::$paths ) ) > 0 ):
      insaneLogger::log( "Obtaining scripts..." );
      $buffer = "/* Parsed scripts */" . PHP_EOL;
      $minified = "/* Parsed scripts Min */" . PHP_EOL;
      foreach( self::$paths as $key => $val ):
        if( isset( $val->parsed ) && !empty( $val->parsed ) ):
          $buffer .= "/* INSANE FILE BUFFED :: " . $val->encoded . "*/" . $val->parsed . PHP_EOL;
          if( strstr( $val->url, '.min.' ) ):
            $minified .= "/* INSANE FILE ORIGINAL:: " . $val->encoded . "*/" . $val->parsed . PHP_EOL;
          else:
            $minified .= "/* INSANE FILE MINIFIED :: " . $val->encoded . "*/" . insaneMinifier::minifyJS( $val->parsed ) . PHP_EOL;
          endif;
        else:
          insaneLogger::log( "File was not parsed, so it wont show up... $key" );
        endif;
      endforeach;
      insaneLogger::log( "Scripts Obtained..." );
      if( $minify ):
        return $minified;
      else:
        return $buffer;
      endif;
    else:
      insaneLogger::log( "Request to obtain scripts was ignored..." );
      return false;
    endif;
  }
}

new insaneJS();
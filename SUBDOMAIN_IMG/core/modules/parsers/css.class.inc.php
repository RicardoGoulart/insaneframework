<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;
  
// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneMinifier" ), __FILE__ );

class insaneCSS {
  /**
   * $paths
   *
   * @var array
   */
  private static $paths = array();

  /**
   * Parses the CSS that beguins with /*@*\/
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     4.1.1
   * @param       string $parseKey
   * @since       1.0.0
   * @return      boolean
   */
  private static function insaneMediaParser( $parseKey = null ){
    self::$paths[$parseKey]->parsed = self::parser( self::$paths[$parseKey]->contents );
    return true;
  }

  /**
   * Confirms that the CSS beguins with /*@*\/
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     4.1.1
   * @param       string key
   * @since       1.0.0
   * @return      boolean
   */
  public static function parseMedia( $key = null ){
    if( !empty( $key ) ):
      if( array_key_exists( $key, self::$paths ) ):
        if( substr( self::$paths[$key]->contents, 0, 5 ) == "/*@*/" ):
          insaneLogger::log( "Provided key $key is valid and will be parsed." );
  				if( self::insaneMediaParser( $key ) ):
            insaneLogger::log( "Provided key $key was processed successfully." );
            return true;
          else:
            insaneLogger::log( "Provided key $key failed to process." );
            return false;
          endif;
        else:
          self::$paths[$key]->parsed = self::$paths[$key]->contents;
          insaneLogger::log( "Provided key $key is not a insaneMediaParser element." );
          return false;
  			endif;
      else:
        insaneLogger::log( "Provided stack key is not valid because is not present in paths stack holder." );
        return false;
      endif;
    else:
      insaneLogger::log( "Provided stack key is not valid because was empty." );
      return false;
    endif;
  }

  /**
   * parser
   *
   * @param mixed $contents
   * @return void
   */
  private static function parser( $contents = null ){
    if( !empty( $contents ) ):
      if( substr( $contents, 0, 5 ) == "/*@*/" ):
        $data = str_replace( "/*@*/", "", $contents );
        $search_results = array();
        $search_classes = preg_match_all( "/(.*?){(.*?)}/mis", $data, $search_results );

        if( $search_classes ):
          $prepare_output = array();
          foreach( $search_results[1] as $key => $val ):
            $className = trim( $val );
            $classContent = trim( $search_results[2][$key] );
            // Loop pelo classContent
            $search_content = preg_match_all( "/(\/\*\!\@\ (mobile|tablet|web|print)\ \*\/(.*?)\/\*\!\@ end \*\/)/mis", $classContent, $classQueries );
            if( count( $classQueries ) > 0 ):
              foreach( $classQueries[2] as $k => $v ):
                if( empty( $prepare_output[$v][$className] ) ):
                  $prepare_output[$v][$className] = "";
                endif;
                $prepare_output[$v][$className] .= trim( $classQueries[3][$k] );
              endforeach;
              // Remover as cenas
              foreach( $classQueries[1] as $s => $r ):
                $classContent = trim( str_replace( $r, "", $classContent ) );
              endforeach;
            endif;
            $prepare_output['original'][$className] = $classContent;
          endforeach;

          $toReturn = "";

          // Parse the parsed content :P
          foreach( $prepare_output['original'] as $ko => $vo ):
            $toReturn .= "$ko{\n$vo\n}\n";
          endforeach;

          // WEB
          if( !empty( $prepare_output['web'] ) ):
            $toReturn .= '@media screen
            and (min-width: 961px)
            and (max-width: 1600px) {';
            foreach( $prepare_output['web'] as $ko => $vo ):
              $toReturn .= "$ko{\n$vo\n}\n";
            endforeach;
            $toReturn .= '}';
          endif;

          // TABLET
          if( !empty( $prepare_output['tablet'] ) ):
            $toReturn .= '@media screen
            and (min-width: 768px)
            and (max-width: 960px) {';
            foreach( $prepare_output['tablet'] as $ko => $vo ):
              $toReturn .= "$ko{\n$vo\n}\n";
            endforeach;
            $toReturn .= '}';
          endif;

          // MOBILE
          if( !empty( $prepare_output['mobile'] ) ):
            $toReturn .= '@media screen
            and (min-width: 320px)
            and (max-width: 767px) {';
            foreach( $prepare_output['mobile'] as $ko => $vo ):
              $toReturn .= "$ko{\n$vo\n}\n";
            endforeach;
            $toReturn .= '}';
          endif;

          // Print
          if( !empty( $prepare_output['print'] ) ):
            $toReturn .= '@media print {';
            foreach( $prepare_output['print'] as $ko => $vo ):
              $toReturn .= "$ko{\n$vo\n}\n";
            endforeach;
            $toReturn .= '}';
          endif;

          // Add to the stack
          return insaneMinifier::minifyCSS( $toReturn );
        else:
          return insaneMinifier::minifyCSS( $contents );
        endif;
      else:
        return insaneMinifier::minifyCSS( $contents );
      endif;
    else:
      insaneLogger::log( "Provided CSS content was empty" );
      return "";
    endif;
  }

  /**
   * parseStyle
   *
   * @param mixed $code
   * @return void
   */
  public static function parseStyle( $code = null ){
    return self::parser( $code );
  }

  /**
   * getCSSRoute
   *
   * @param mixed $path
   * @return void
   */
  public static function getCSSRoute( $path = null, $final = false ){
    if( !empty( $path ) ):
      return ( $final ? '//' . insaneConfig::$cssProvider . '/' : '' ) . strrev( base64_encode( $path ) );
    else:
      insaneLogger::log( "No path provided for CSS ROUTES." );
      return "NO-PATH";
    endif;
  }

  /**
   * parseCSSRoute
   *
   * @param mixed $path
   * @return void
   */
  public static function parseCSSRoute( $path = null ){
    if( !empty( $path ) ):
      return base64_decode( strrev( $path ) );
    else:
      insaneLogger::log( "No path provided for CSS ROUTES." );
      return false;
    endif;
  }

  /**
   * Adds a style to the stack
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @param       string  url
   * @return      boolean
   */
  public static function addStyle( $url = null ){
    if( !empty( $url ) ):
      if( strlen( $url ) > 3 ):
        $encoded = md5( $url );
        $content = (object) array(
          'url'       => $url,
          'encoded'   => $encoded,
          'contents'  => "",
          'parsed'    => "",
          'minified'  => "",
          'extension' => substr( $url, -3 )
        );
        // Sort by extension
        switch( substr( $url, -3 ) ):
          case 'css':
            insaneLogger::log( sprintf( "CSS File found! Url: %s", $url ) );
            $content->contents = file_get_contents( $url );
            insaneLogger::log( "CSS File included successfully!" );
            break;
          case 'php':
            insaneLogger::log( sprintf( "PHP File found! Url: %s", $url ) );
            ob_start();
              include( $url );
              $content->contents = ob_get_contents();
            ob_end_clean();
            insaneLogger::log( "PHP File included successfully!" );
            break;
          default:
            insaneLogger::log( sprintf( "File extension was not recognised. Url: %s Extension: %s", $url, substr( $url, -3 ) ) );
            break;
        endswitch;
        self::$paths[$encoded] = $content;
      else:
        insaneLogger::log( "Given file has a name smaller than necessary. Can't parse the file because of that..." );
        return false;
      endif;
    else:
      return false;
    endif;
  }

  /**
   * Removes a style from the stack
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @param       string  url
   * @return      boolean
   */
  public static function removeStyle( $url = null ){
    if( !empty( $url ) ):
      $encoded = md5( $url );
      if( array_key_exists( self::$paths, $encoded ) ):
        unset( self::$paths[$encoded] );
        insaneLogger::log( sprintf( "Will remove the file from the stack: %s", $url ) );
        return true;
      else:
        insaneLogger::log( sprintf( "Wanted to remove the file from the stack: %s but it failed because it dosent exists.", $url ) );
        return false;
      endif;
    else:
      insaneLogger::log( "Can't remove an empty key." );
      return false;
    endif;
  }

  /**
   * Orders the execution of the parse for the styles present in current stack
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @return      boolean
   */
  public static function parseStyles(){
    if( count( array_keys( self::$paths ) ) > 0 ):
      insaneLogger::log( "Parsing styles..." );
      foreach( self::$paths as $key => $val ):
        if( self::parseMedia( $key ) ):
          insaneLogger::log( "Finished parsing $key successfully." );
        else:
          insaneLogger::log( "Failed parsing $key." );
        endif;
      endforeach;
      insaneLogger::log( "Finished parsing styles..." );
      return true;
    else:
      insaneLogger::log( "Request to parse was ignored because there was nothing to parse in the stack." );
      return false;
    endif;
  }

  /**
   * Returns the parsed styles after parseStyles()
   * @author      Ricardo Goulart <ricardo@goulart.pt>
   * @version     2.1
   * @param       boolean $minify
   * @return      boolean
   */
  public static function getStyles( $minify = false ){
    if( count( array_keys( self::$paths ) ) > 0 ):
      insaneLogger::log( "Obtaining styles..." );
      $buffer = "/* Parsed styles */" . PHP_EOL;
      $minified = "/* Parsed styles Min */" . PHP_EOL;
      foreach( self::$paths as $key => $val ):
        if( isset( $val->parsed ) && !empty( $val->parsed ) ):
          $buffer .= "/* INSANE FILE BUFFED :: " . $val->encoded . "*/" . $val->parsed . PHP_EOL;
          if( strstr( $val->url, 'min.css' ) ):
            $minified .= "/* INSANE FILE ORIGINAL:: " . $val->encoded . "*/" . $val->parsed . PHP_EOL;
          else:
            $minified .= "/* INSANE FILE MINIFIED :: " . $val->encoded . "*/" . insaneMinifier::minifyCSS( $val->parsed ) . PHP_EOL;
          endif;
        else:
          insaneLogger::log( "File was not parsed, so it wont show up... $key" );
        endif;
      endforeach;
      insaneLogger::log( "Styles Obtained..." );
      if( $minify ):
        return $minified;
      else:
        return $buffer;
      endif;
    else:
      insaneLogger::log( "Request to obtain styles was ignored..." );
      return false;
    endif;
  }
}

new insaneCSS();
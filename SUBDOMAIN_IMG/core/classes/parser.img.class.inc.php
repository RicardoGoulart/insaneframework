<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-05 Happy new Year!
 * @version   0.2
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
    die( "Unauthorized Insane Access! GTFO!" );
endif;

ini_set( 'memory_limit', '2048M' );
ini_set( 'allow_url_fopen', 'on' );

//Time Limit
set_time_limit( 30 );

class insaneImage {
  /**
   * $cacheDir
   *
   * @var null
   */
  private static $cacheDir = null;

  /**
   * $cacheFile
   *
   * @var null
   */
  private static $cacheFile = null;

  /**
   * $url
   *
   * @var null
   */
  private static $url = null;

  /**
   * $params
   *
   * @var null
   */
  private static $params = null;

  /**
   * $iSize
   *
   * @var integer
   */
  private static $iSize = 0;

  /**
   * $fSize
   *
   * @var integer
   */
  private static $fSize = 0;

  /**
   * $iContent
   *
   * @var null
   */
  private static $iContent = null;

  /**
   * $iQuality
   *
   * @var integer
   */
  private static $iQuality = 7;

  /**
   * $iMethod
   *
   * @var null
   */
  private static $iMethod = null;

  /**
   * $iUrl
   *
   * @var null
   */
  private static $iUrl = null;

  /**
   * $iWidth
   *
   * @var integer
   */
  private static $iWidth = 0;

  /**
   * $iHeight
   *
   * @var integer
   */
  private static $iHeight = 0;

  /**
   * $iCache
   *
   * @var integer
   */
  private static $iCache = 3600;

  /**
   * $fTime
   *
   * @var integer
   */
  private static $fTime = 0;

  /**
   * $mimetypes
   *
   * @var array
   */
  private static $mimetypes = array(
    'jpg'   => "image/jpg", 
    'gif'   => "image/gif", 
    'jpeg'  => "image/jpeg", 
    'png'   => "image/png"
  );
  
  /**
   * registerImage
   *
   * @param mixed $url
   * @param mixed $params
   * @return void
   */
  public static function registerImage( $url = null, $params = null, $cacheDir = null, $cacheFile = null ){
    try {
      $url = $url ? base64_decode( strrev( $url ) ) : null;
      $params = $params ? json_decode( base64_decode( strrev( $params ) ) ) : null;
      insaneLogger::log( "insaneImage got image: $url" );
      insaneLogger::log( "insaneImage got params: " . json_encode( $params ) );
      if( $url ):
        self::$url = $url;
      else:
        return false;
      endif;

      if( $params ):
        self::$params = $params;
        if( !isset( self::$params->quality ) ):
          self::$iQuality = 7;
        else:
          self::$iQuality = (int) self::$params->quality;
          self::$iQuality = (int) floor( self::$iQuality/10 );
          if( self::$iQuality < 3 ):
            self::$iQuality = 3;
          endif;
          if( self::$iQuality > 9 ):
            self::$iQuality = 9;
          endif;
        endif;
        if( !isset( self::$params->method ) ):
          self::$iMethod = 3;
        else:
          self::$iMethod = self::$params->method;
        endif;
        //if( !isset( self::$params->url ) ):
          //self::$iUrl = 3;
        //else:
          //self::$iUrl = self::$params->url;
        //endif;
        if( !isset( self::$params->calcWidth ) ):
          self::$iWidth = 3;
        else:
          self::$iWidth = self::$params->calcWidth;
        endif;

        if( !isset( self::$params->calcHeight ) ):
          self::$iHeight = null;
        else:
          self::$iHeight = self::$params->calcHeight;
        endif;

        if( !isset( self::$params->cache ) ):
          self::$iCache = 3600;
        else:
          self::$iCache = self::$params->cache;
        endif;
      endif;

      if( !empty( $cacheDir ) ):
        self::$cacheDir = $cacheDir;
      else:
        return false;
      endif;

      if( !empty( $cacheFile ) ):
        self::$cacheFile = $cacheFile;
      else:
        return false;
      endif;
    } catch( Exception $e ){
      insaneLogger::log( "Error on the CATCH ---- " . json_encode( $e ) );
      return false;
    }
  }

  /**
   * parseImage
   *
   * @return void
   */
  public static function parseImage(){
    self::$iSize = getimagesize( self::$url );
    self::$iSize = array(
      'width' => self::$iSize[0],
      'height'=> self::$iSize[1],
      'mime'  => self::$iSize['mime'],
      'quality'=> self::$iQuality
    );

    if( empty( self::$iWidth ) ):
      self::$iWidth = self::$iSize['width'];
    endif;

    //if( empty( self::$iHeight ) ):
      self::$iHeight = self::$iSize['height'];
    //endif;

    // Get image width curl... 
    self::$iContent = null;
    $isCurl = false;
    if( substr( self::$url, 0, 2 ) == "//" ):
      self::$url = "http:" . self::$url;
      $isCurl = true;
    elseif( substr( self::$url, 0, 4 ) == "http" ):
      // Get curl!
      $isCurl = true;
    endif;
    
    if( $isCurl == true ):
      self::$url = self::getCurlImage();
      if( self::$url == false ):
        insaneLogger::log( "Curl URL gave us nothing..." );
        return false;
      endif;
      return self::parseImage();
    endif;
    
    self::$fSize = filesize( self::$url );
    self::$iContent = file_get_contents( self::$url );
    self::$fTime = filemtime( self::$url );

    if( !$imageString = imagecreatefromstring( self::$iContent ) ):
      insaneLogger::log( "Error creating from string!" );
      return false;
    endif;
    
    imagealphablending( $imageString, false );
    imagesavealpha( $imageString, true );
    
    if( isset( self::$iMethod ) ):
      switch( self::$iMethod ):

        case 'width':
          if( isset( self::$iWidth ) ):
            if( self::$iSize['width'] > self::$iWidth ):
              self::$iHeight = floor( ( self::$iSize['height'] * self::$iWidth ) / self::$iSize['width'] );
              //self::$iWidth = self::$iSize['width'];
            endif;
          endif;
        break;

        case 'height':
          if( isset( self::$iHeight ) ):
            if( self::$iSize['height'] > self::$iHeight ):
              self::$iWidth = floor( ( self::$iSize['width'] * self::$iHeight ) / self::$iSize['height'] );
              //self::$iHeight = self::$iSize['height'];
            endif;            
          endif;
        break;

      endswitch;
      $imageTrueColor = imagecreatetruecolor( self::$iWidth, self::$iHeight );
    else:
      $imageTrueColor = imagecreatetruecolor( self::$iSize['width'], self::$iSize['height'] );
    endif;
    
    if( $imageTrueColor ):
      imagealphablending( $imageTrueColor, false );
      imagesavealpha( $imageTrueColor , true );
      imagecopyresampled( $imageTrueColor, $imageString, 0, 0, 0, 0, 
        // NEW
        self::$iWidth, self::$iHeight,
        // CURRENT
        self::$iSize['width'], self::$iSize['height']        
      );

      if( !is_dir( self::$cacheDir ) ):
        mkdir( self::$cacheDir, 755, true );
      endif;

      imagepng( $imageTrueColor, self::$cacheFile, self::$iQuality );

      if( insaneCache::$gZip == true ):
        $tmpImage = file_get_contents( self::$cacheFile );
        sleep( 1 );
        file_put_contents( self::$cacheFile, gzencode( $tmpImage, 9, FORCE_GZIP ) );
      endif;
    else:
      insaneLogger::log( "No imageTrueColor created" );
      return false;
    endif;
  }

  /**
   * printImage
   *
   * @return void
   */
  public static function printImage(){
    header( 'Content-type: image/png', true );
    header( 'Pragma: public', true );
    header( "Cache-Control: store, cache, max-age=" . self::$iCache, true );
    header( 'ETag: "' . insaneCache::$uri . '"', true );
    header( "Content-length: " . filesize( self::$cacheFile ), true );
    header( 'Last-Modified: ' . gmdate('D, d M Y H:i:s',  filemtime( self::$cacheFile ) ).' GMT', true );
    header( 'Expires: ' . gmdate('D, d M Y H:i:s',  strtotime( date( "Y-m-d H:i:s" ) ) + self::$iCache ).' GMT', true );
    header( 'Insane-Width: ' . self::$iWidth, true );
    header( 'Insane-Height: ' . self::$iHeight, true );
    header( 'Insane-Method: ' . self::$iMethod, true );
    header( 'Insane-Quality: ' . self::$iQuality, true );
    header( 'Insane-Cache: ' . self::$iCache, true );
    
    print file_get_contents( self::$cacheFile );
  }

  /**
   * getCurlImage
   *
   * @return void
   */
  private static function getCurlImage(){
    if( !empty( self::$url ) ):
      // MAKE CURL
      $ch = curl_init ();

      if( strstr( self::$url, "placehold.it/" ) ):
        self::$url = "http://placehold.it/" . self::$iWidth;
      endif;

      curl_setopt( $ch, CURLOPT_URL, self::$url );
      curl_setopt( $ch, CURLOPT_HEADER, false );
      curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

      $image = curl_exec( $ch );
      curl_close ( $ch );

      if( $image ):
          if( !is_dir( self::$cacheDir ) ):
            mkdir( self::$cacheDir, 755, true );
          endif;

          $theUrl = self::$cacheDir . DIRECTORY_SEPARATOR . md5( self::$url ) . ".original";

          file_put_contents( $theUrl, $image );
          
          return $theUrl;
      else:
          return false;
      endif;
      
    else:
      return false;
    endif;
  }
}

new insaneImage();
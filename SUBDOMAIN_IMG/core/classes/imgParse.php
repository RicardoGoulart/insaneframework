<?php

/**
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @date 2011-07-13
 * @desc Controller (Model) for Image Gallery
 */

ini_set( 'memory_limit', '256M' );
ini_set( 'allow_url_fopen', 'on' );

//Time Limit
set_time_limit(60);

if( !defined( "PROTECTED_SPEACH" ) ):
   header( "Location: " . $_SERVER["HTTP_HOST"], true, 301 );
   die();
endif;

class ImageGallery
{
    private $imageCacheDir  = 'cache/';
    private $imgQuality     = 80; // 0 -100
    private $imgCompression = 8; // 0 - 9
    private $imgCacheTime   = 15552000;
    private $imgCompleteName;
    private $imgName;
    private $imgFunction;
    private $imgSize;
    private $workingCrop;
    private $workingImg;
    private $page;
	private $mimetypes = array(
		//"image/jpeg", "image/gif", "image/jpeg", "image/png"
		'jpg' => "image/jpg", 'gif' => "image/gif", 'jpeg' => "image/jpeg", 'png' => "image/png"
	);

    private function getExternalImage( $url = null ){
        if( $url ){
            // MAKE CURL
            $ch = curl_init ();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_HEADER, false );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

            $image = curl_exec( $ch );
            curl_close ( $ch );
            if( $image ):
                if( !is_dir( $this->imageCacheDir ) ):
                    mkdir( $this->imageCacheDir, 755, true );
                endif;
                file_put_contents( $this->imageCacheDir . md5( $url ) . ".image", $image );
                return $this->imageCacheDir . md5( $url ) . ".image";
            else:
                return $url;
            endif;

        } else {
            return $url;
        }
    }

    public function __construct( $page, $options = null, $cache, $filecache, $expire, $compression )
    {
        //$this->imageCacheTime = (string) ( 3600 * 60 * 24 * 3 );
		$this->imageCacheDir = $cache . DIRECTORY_SEPARATOR;
		$this->page = (object) array();
		try {
            $tmp = ( base64_decode( strrev( $page ) ) );
            if( $options ):
                $tmpParams = json_decode( base64_decode( strrev( $options ) ) );

                if( isset( $tmpParams->iiQuality ) ):
                    $this->imgQuality = $tmpParams->iiQuality;
                    $this->imgCompression = ceil( $this->imgQuality/100 );
                endif;
            endif;
		} catch( Exception $e ){
			$this->imageCreateText();
			die;
		}

        if( substr( $tmp, 0, 4 ) == 'http' ){
            $this->page->page_controller = $this->getExternalImage( $tmp );
        } else {
            $this->page->page_controller = $tmp;
        }

        if( substr( $tmp, 0, 2 ) == '//' ){
            $tmp = str_replace( "//", "http://", $tmp );
            $this->page->page_controller = $this->getExternalImage( $tmp );
        } else {
            $this->page->page_controller = $tmp;
        }

		$this->page->page_options = !empty( $tmpParams->method ) ? $tmpParams->method : null;
        $this->page->page_options_expand = !empty( $tmpParams->calcWidth ) ? $tmpParams->calcWidth : null;
        $tmp_1 = explode( ".", $tmp );
		$this->page->page_complete_url = $filecache . "." . array_pop( $tmp_1 );
		$this->page->expiry = $expire;
		$this->page->compression = $compression;

		$t_tmp = explode( "/", $this->page->page_controller );
		$this->imgName = array_pop( $t_tmp );
var_dump( $this ); die;
        $this->parseRequest();
        $this->parseImage();

    }

    public function __destruct()
    {
        unset( $this->workingCrop );
        unset( $this->workingImg );
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc Inserts $page values into the class private Vars
     * @param object $page
     */
    private function parseRequest()
    {
        //$this->imgName          = $this->page->page_controller;
        $this->imgFunction      = $this->page->page_options;
        $this->imgSize          = $this->page->page_options_expand;
        $this->imgCompleteName  = $this->page->page_complete_url;
        if( $this->fileExistsOnDisk( $this->imageCacheDir . $this->imgCompleteName ) )
        {
            $this->printImageOnDisk( $this->imageCacheDir . $this->imgCompleteName, true );
        }
		if( !file_exists( $this->page->page_controller ) ):
			$this->imageCreateText();
			die;
		endif;
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc In case of error, it shows 404
     */
    private function show404()
    {
        //TODO: Print image?
        //require_once 'controllers/404.php';
        die;
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This controls the path/routing of the requests
     */
    private function parseImage()
    {
        switch( $this->imgFunction )
        {
            case 'width':
                $this->checkWidthParams();
                $this->parseImgWidth();
                $this->checkImgExtension();
                break;
            case 'height':
                $this->checkHeightParams();
                $this->parseImgHeight();
                $this->checkImgExtension();
                break;
            case 'resizeAndCrop':
                $this->checkResizeAndCropParams();
                $this->parseImgCrop();
                $this->checkImgExtension();
                break;
            case 'max':
                $this->checkMaxParams();
                $this->parseImgMax();
                $this->checkImgExtension();
                break;
            default:
                $this->printOriginalImage();
                break;
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This function only CHECKS if give Param is a number. Returns true or false on completion.
     * @param string/number $number
     * @return bool
     */
    private function ConfirmInt( $number )
    {
        if( preg_match('/(\d+)/', $number, $array ) )
        {
            return $array[1];
        }
        else
        {
            return false;
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This confirms that the /width/xxx is a number
     */
    private function checkWidthParams()
    {
        if( !$this->ConfirmInt( $this->imgSize ) )
        {
            $this->printOriginalImage();
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This confirms that the /height/xxx is a number
     */
    private function checkHeightParams()
    {
        if( !$this->ConfirmInt( $this->imgSize ) )
        {
            $this->printOriginalImage();
            //$this->show404();
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This confirms that the /resizeAndCrop/000x000 value has a number before and after the X
     */
    private function checkResizeAndCropParams()
    {
        //By far the hardest function here... :P
        if( !count( explode( 'x', $this->imgSize ) ) == 2 )
        {
            //Input params must be 000x000
            //This wasnt the case... :(
            $this->printOriginalImage();
            //$this->show404();
        }

        foreach( explode( 'x', $this->imgSize ) as $n )
        {
            if( !$this->ConfirmInt( $n ) )
            {
                $this->printOriginalImage();
                //$this->show404();
            }
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This confirms that the /max/xxx is a number
     */
    private function checkMaxParams()
    {
        if( !$this->ConfirmInt( $this->imgSize ) )
        {
            $this->printOriginalImage();
            //$this->show404();
        }
    }

	private function imageCreateText()
	{
		$this->imgSize = 600;

       
        $theFilename = dirname( __DIR__ ) . DIRECTORY_SEPARATOR . "cache" . DIRECTORY_SEPARATOR . date( "Y" ) . DIRECTORY_SEPARATOR . date( "m" ) . DIRECTORY_SEPARATOR . "image404_NOZIP.jpeg";
        
        //var_dump( $this ); die;

        if( !is_dir( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . "cache" . DIRECTORY_SEPARATOR . date( "Y" ) . DIRECTORY_SEPARATOR . date( "m" ) ) ):
            mkdir( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . "cache" . DIRECTORY_SEPARATOR . date( "Y" ) . DIRECTORY_SEPARATOR . date( "m" ), 755, true );
        endif;

		$this->workingImg = (object) array();
		if( !file_exists( $theFilename ) ):

			//Fetch Image Params from Database
			try {

				$this->workingImg->width = 600;
				$this->workingImg->height = 70;
				$this->workingImg->mime_type = "image/jpeg";
				$this->workingImg->date = date("D, d M Y H:i:s", time() );
				$this->workingImg->size = 0;

				$im = imagecreate( $this->workingImg->width, $this->workingImg->height );
				$background_color = imagecolorallocate( $im, 255, 255, 255 );
				$text_color = imagecolorallocate( $im, 0, 0, 0 );
				//imagestring( $im, 1, 5, 5,  "» 404 - Imagem inexistente...", $text_color );
				imagettftext( $im, 30, 0, 30, 50, $text_color, __DIR__ . "/CENTURY.TTF", "» 404 - No Img" );
				imagejpeg( $im, $theFilename, $this->imgQuality );
				header( "Parser-bot: RmG JPG", true, 200 );
                header( "Parser-bot-image-size: " . $this->workingImg->width . 'x' . $this->workingImg->height , true, 200 );
			} catch( Exception $e ){
				die;
			};
		else:
			$this->workingImg->width = 600;
			$this->workingImg->height = 70;
			$this->workingImg->mime_type = "image/jpg";
			$this->workingImg->date = date("D, d M Y H:i:s", time() );
			header( "Parser-bot: RmG Cached 100%", true, 200 );
            header( "Parser-bot-image-size: " . $this->workingImg->width . 'x' . $this->workingImg->height , true, 200 );
		endif;

		header('Content-type: ' . $this->workingImg->mime_type );
        //header('Last-Modified: ' . gmdate('D, d M Y H:i:s', strtotime( $this->workingImg->date ) ).' GMT');
        header('Expires: ' . gmdate('D, d M Y H:i:s',  strtotime( $this->workingImg->date ) + 86400*50).' GMT');
        header( "Last-Modified: " . gmdate( 'D, d M Y H:i:s', filemtime( $theFilename ) ) . ' GMT', true );
		header( "Cache-Control: store, cache, max-age=" . $this->imgCacheTime, true );
        header('Pragma: public');

		header( "ETag: \"" . $this->page->page_complete_url . '"', true );
		// Salva
		header('Content-Length: ' . filesize( $theFilename ), true, 404 );
        //imagejpeg( $this->workingCrop, null, $this->imgQuality );
        //Imprime
        
        if( $this->page->compression == true ):
			header( "Vary: Accept-Encoding", true );
			header( "Content-Encoding: gzip", true );
			// Converte
            $tmp_res = file_get_contents( $theFilename );
            $tmp_res = gzencode( $tmp_res, 5, FORCE_GZIP );
            header('Content-Length: ' . strlen( $tmp_res ), true, 404 );
            echo $tmp_res;
		else:
            echo file_get_contents( $theFilename );
        endif;
	}

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This crops the given image (ResizeAndCrop)
     */
    private function parseImgCrop()
    {
        //Parse Img new with and height
        $newImgParams = explode( 'x', $this->imgSize );
        $newWidth = (string) $this->ConfirmInt( $newImgParams[0] );
        $newHeight = (string) $this->ConfirmInt( $newImgParams[1] );

        try {
			$imageSizes = getimagesize( $this->page->page_controller );
			$this->workingImg = (object) array();
			$this->workingImg->width = $imageSizes[0];
			$this->workingImg->height = $imageSizes[1];
			//$this->workingImg->mime_type = $this->mimetypes[exif_imagetype( $this->page->page_controller )];
			$this->workingImg->mime_type = image_type_to_mime_type(exif_imagetype( $this->page->page_controller ));
			$this->workingImg->date = date("D, d M Y H:i:s", filemtime( $this->page->page_controller ).' GMT');
			$this->workingImg->size = filesize( $this->page->page_controller );
			$this->workingImg->image_blob = file_get_contents( $this->page->page_controller );
			//Create the image through its binary code...
			$imgContents = imagecreatefromstring( $this->workingImg->image_blob );
                imagealphablending( $imgContents, false );
                imagesavealpha( $imgContents , true );
		} catch( Exception $e ){
			// $this->printOriginalImage();
			die("Failed resizeAndCrop");
		}

        //We need to provide the original filepath because this function needs it to get it's properties (Yes we actually read the Image Twice...)
        //Can this be fixed?
        $width = (int) $this->workingImg->width;
        $height = (int) $this->workingImg->height;

        #list( $width, $height ) = getimagesize( $this->workingImg->path );

        //If somewone is trying to get the image BIGGER than its original size.
        $theOrig = false;
        if( $width < $newWidth )
        {
            if( $height > $newHeight )
            {
                $this->parseImgMax();
            }
            else
            {
                $theOrig = true;
                // $this->printOriginalImage();
            }
        }
        if( $height < $newHeight )
        {
            if( $width > $newWidth )
            {
                $this->parseImgMax();
            }
            else
            {
                $theOrig = true;
                // $this->printOriginalImage();
            }
        }

        if( $theOrig ){
            //Let us first create an BLACK/WHITE img...
            $crop = imagecreatetruecolor( $width, $height );
            imagealphablending( $crop, false );
            imagesavealpha( $crop , true );
            imagecopyresampled( $crop, $imgContents, 0, 0, 0, 0, $width, $height, $width, $height );


            $crop2 = imagecreatetruecolor( $width, $height );
            imagealphablending( $crop2, false );
            imagesavealpha( $crop2 , true );
            imagecopyresampled( $crop2, $crop, 0, 0, 0, 0, $width, $height, $width, $height );

            //unset( $crop );
            $this->workingCrop = $crop2;
            return;
        }

        $xscale = $width/$newWidth;
        $yscale = $height/$newHeight;

        if( $yscale<$xscale )
        {
            $width_f  = round( $width * ( 1 / $yscale ) );
            $height_f = round( $height * ( 1 / $yscale ) );
        }
        else
        {
            $width_f  = round( $width * ( 1 / $xscale ) );
            $height_f = round( $height * ( 1 / $xscale ) );
        }

        //Let us first create an BLACK/WHITE img...
        $crop = imagecreatetruecolor( $width_f, $height_f );
        imagealphablending( $crop, false );
        imagesavealpha( $crop , true );
        imagecopyresampled( $crop, $imgContents, 0, 0, 0, 0, $width_f, $height_f, $width, $height );


        //Image is resized WITH PROPORTION (ex. 133x100)
        //Now we must cut that extra 33px/2 from each side..

        //Find the middle
        //Math
        $widthStart = ( $width_f / 2 )-( $newWidth / 2 );
        $heightStart = ( $height_f / 2 )-( $newHeight / 2 );

        $crop2 = imagecreatetruecolor( $newWidth, $newHeight );
        imagealphablending( $crop2, false );
        imagesavealpha( $crop2 , true );
        imagecopyresampled( $crop2, $crop, 0, 0, $widthStart, $heightStart, $newWidth, $newHeight, $newWidth, $newHeight );

        //unset( $crop );
        $this->workingCrop = $crop2;

//        $dim = getimagesize( $this->workingImg->path );
//
//        //create from type handle
//        if($dim[2] == 1) $new = imagecreatefromjpeg( $this->workingImg->path );
//        elseif($dim[2] == 2) $new = imagecreatefromgif( $this->workingImg->path );
//        elseif($dim[2] == 3) $new = imagecreatefrompng( $this->workingImg->path );
//        else die("Unsupported format: ".$dim[2]);
//
//        //find colorcode
//        $palette = imagecreatetruecolor($dim[0], $dim[1]);
//        $found = false;
//        while($found == false) {
//
//            $r = rand(0, 255);
//            $g = rand(0, 255);
//            $b = rand(0, 255);
//
//            if(imagecolorexact($new, $r, $g, $b) != (-1)) {
//
//                $colorcode = imagecolorallocate($palette, $r, $g, $b);
//                $found = true;
//
//            }
//
//        }
//
//        //draw corners
//        imagearc($new, $corner-1, $corner-1, $corner*2, $corner*2, 180, 270, $colorcode);
//        imagefilltoborder($new, 0, 0, $colorcode, $colorcode);
//
//        imagearc($new, $dim[0]-$corner, $corner-1, $corner*2, $corner*2, 270, 0, $colorcode);
//        imagefilltoborder($new, $dim[0], 0, $colorcode, $colorcode);
//
//        imagearc($new, $corner-1, $dim[1]-$corner, $corner*2, $corner*2, 90, 180, $colorcode);
//        imagefilltoborder($new, 0, $dim[1], $colorcode, $colorcode);
//
//        imagearc($new, $dim[0]-$corner, $dim[1]-$corner, $corner*2, $corner*2, 0, 90, $colorcode);
//        imagefilltoborder($new, $dim[1], $dim[1], $colorcode, $colorcode);
//
//        imagecolortransparent($new, $colorcode); //make corners transparent
//
//        $this->workingCrop = $crop2;
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This parses the image with the "width" controler
     */
    private function parseImgWidth()
    {
        $this->imgSize = $this->ConfirmInt( $this->imgSize );

        //Fetch Image Params from Database
        //$this->workingImg = (object) $this->fetchImgParams();
		try {
			$imageSizes = getimagesize( $this->page->page_controller );
			$this->workingImg = (object) array();
			$this->workingImg->width = $imageSizes[0];
			$this->workingImg->height = $imageSizes[1];
			//$this->workingImg->mime_type = $this->mimetypes[exif_imagetype( $this->page->page_controller )];
			$this->workingImg->mime_type = image_type_to_mime_type(exif_imagetype( $this->page->page_controller ));
			$this->workingImg->date = date("D, d M Y H:i:s", filemtime( $this->page->page_controller ).' GMT');
			$this->workingImg->size = filesize( $this->page->page_controller );
			$this->workingImg->image_blob = file_get_contents( $this->page->page_controller );
			//Create the image through its binary code...
			$imgContents = imagecreatefromstring( $this->workingImg->image_blob );
                imagealphablending( $imgContents, false );
                imagesavealpha( $imgContents , true );
		} catch( Exception $e ){
			die("failed width ...");
		}

        //We need to provide the original filepath because this function needs it to get it's properties (Yes we actually read the Image Twice...)
        //Can this be fixed?
        $width = (int) $this->workingImg->width;
        $height = (int) $this->workingImg->height;

        //If somewone is trying to get the image BIGGER than its original size.
        if( $width < $this->imgSize )
        {
            //$this->printOriginalImage();
            //Let us first create an BLACK/WHITE img...
            $crop = imagecreatetruecolor( $this->workingImg->width, $this->workingImg->height );
            imagealphablending( $crop, false );
            imagesavealpha( $crop , true );
            imagecopyresampled( $crop, $imgContents, 0, 0, 0, 0, $this->workingImg->width, $this->workingImg->height, $width, $height );
            $this->workingCrop = $crop;
            unset( $crop );
            return true;
        }

        $width_f = $this->imgSize;
        $height_f = ( $height * $width_f ) / $width;

        //Let us first create an BLACK/WHITE img...
        $crop = imagecreatetruecolor( $width_f, $height_f );
        imagealphablending( $crop, false );
        imagesavealpha( $crop , true );
        imagecopyresampled( $crop, $imgContents, 0, 0, 0, 0, $width_f, $height_f, $width, $height );
        $this->workingCrop = $crop;
        unset( $crop );
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This parses the image with the "height" controler
     */
    private function parseImgHeight()
    {
        $this->imgSize = $this->ConfirmInt( $this->imgSize );

        //Fetch Image Params from Database
        try {
			$imageSizes = getimagesize( $this->page->page_controller );
			$this->workingImg = (object) array();
			$this->workingImg->width = $imageSizes[0];
			$this->workingImg->height = $imageSizes[1];
			//$this->workingImg->mime_type = $this->mimetypes[exif_imagetype( $this->page->page_controller )];
			$this->workingImg->mime_type = image_type_to_mime_type(exif_imagetype( $this->page->page_controller ));
			$this->workingImg->date = date("D, d M Y H:i:s", filemtime( $this->page->page_controller ).' GMT');
			$this->workingImg->size = filesize( $this->page->page_controller );
			$this->workingImg->image_blob = file_get_contents( $this->page->page_controller );
			//Create the image through its binary code...
            $imgContents = imagecreatefromstring( $this->workingImg->image_blob );
                imagealphablending( $imgContents, false );
                imagesavealpha( $imgContents , true );
		} catch( Exception $e ){
			//$this->printOriginalImage();
			die("Failed :(");
		};

        //We need to provide the original filepath because this function needs it to get it's properties (Yes we actually read the Image Twice...)
        //Can this be fixed?
        $width = (int) $this->workingImg->width;
        $height = (int) $this->workingImg->height;

        #list( $width, $height ) = getimagesize( $this->workingImg->path );

        //If somewone is trying to get the image BIGGER than its original size.
        if( $height < $this->imgSize )
        {
            //$this->printOriginalImage();
            //Let us first create an BLACK/WHITE img...
            $crop = imagecreatetruecolor( $this->workingImg->width, $this->workingImg->height );
            imagealphablending( $crop, false );
            imagesavealpha( $crop , true );
            imagecopyresampled( $crop, $imgContents, 0, 0, 0, 0, $this->workingImg->width, $this->workingImg->height, $width, $height );
            $this->workingCrop = $crop;
            unset( $crop );
            return true;
        }

        $height_f = $this->imgSize;
        $width_f = ( $width * $height_f ) / $height;

        //Let us first create an BLACK/WHITE img...
        $crop = imagecreatetruecolor( $width_f, $height_f );
        imagealphablending( $crop, false );
        imagesavealpha( $crop , true );
        imagecopyresampled( $crop, $imgContents, 0, 0, 0, 0, $width_f, $height_f, $width, $height );
        $this->workingCrop = $crop;
        unset( $crop );
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This parses the image with the "width" controler
     */
    private function parseImgMax()
    {
        $this->imgSize = $this->ConfirmInt( $this->imgSize );

        //Fetch Image Params from Database
        try {
			$imageSizes = getimagesize( $this->page->page_controller );
			$this->workingImg = (object) array();
			$this->workingImg->width = $imageSizes[0];
			$this->workingImg->height = $imageSizes[1];
			//$this->workingImg->mime_type = $this->mimetypes[exif_imagetype( $this->page->page_controller )];
			$this->workingImg->mime_type = image_type_to_mime_type(exif_imagetype( $this->page->page_controller ));
			$this->workingImg->date = date("D, d M Y H:i:s", filemtime( $this->page->page_controller ).' GMT');
			$this->workingImg->size = filesize( $this->page->page_controller );
			$this->workingImg->image_blob = file_get_contents( $this->page->page_controller );
			//Create the image through its binary code...
			//$imgContents = imagecreatefromstring( $this->workingImg->image_blob );
		} catch( Exception $e ){
			die("no cookie today");
		}

        //We need to provide the original filepath because this function needs it to get it's properties (Yes we actually read the Image Twice...)
        //Can this be fixed?
        $width = (int) $this->workingImg->width;
        $height = (int) $this->workingImg->height;

        #list( $width, $height ) = getimagesize( $this->workingImg->path );

        unset( $this->workingImg );
        $maxWillBe = max( $width, $height );
        if( $maxWillBe == $width )
        {
            //If somewone is trying to get the image BIGGER than its original size.
            if( $width < $this->imgSize )
            {
                //$this->printOriginalImage();
                //return true;
            }
            $this->parseImgWidth();
        }
        else
        {
            //If somewone is trying to get the image BIGGER than its original size.
            if( $height < $this->imgSize )
            {
                //$this->printOriginalImage();
                //return true;
            }
            $this->parseImgHeight();
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc In case of a JPEG image, this will print it.
     */
    private function printImgJPEG()
    {
        header('Content-type: ' . $this->workingImg->mime_type );
        //header('Last-Modified: ' . gmdate('D, d M Y H:i:s', strtotime( $this->workingImg->date ) ).' GMT');
        header('Expires: ' . gmdate('D, d M Y H:i:s',  strtotime( $this->workingImg->date ) + 86400*50).' GMT');

		header( "Cache-Control: store, cache, max-age=" . $this->imgCacheTime, true );
        header('Pragma: public');

		header( "ETag: " . $this->page->page_complete_url, true );
        header('Pragma: public');

		// Salva
		imagejpeg( $this->workingCrop, $this->imageCacheDir . $this->imgCompleteName, $this->imgQuality );
		if( $this->page->compression == true ):
			header( "Vary: Accept-Encoding", true );
			header( "Content-Encoding: gzip", true );
			// Converte
			$tmp_res = file_get_contents( $this->imageCacheDir . $this->imgCompleteName );
			file_put_contents( $this->imageCacheDir . $this->imgCompleteName, gzencode( $tmp_res, 5, FORCE_GZIP ) );
		endif;

		header('Content-Length: ' . filesize( $this->imageCacheDir . $this->imgCompleteName ) );
        header( "Last-Modified: " . gmdate( 'D, d M Y H:i:s', filemtime( $this->imageCacheDir . $this->imgCompleteName ) ) . ' GMT', true );

        //imagejpeg( $this->workingCrop, null, $this->imgQuality );
		//Imprime

        header( "Parser-bot: RmG PNG", true, 200 );
        header( "Parser-bot-image-size: " . $this->workingImg->width . 'x' . $this->workingImg->height );
		echo file_get_contents( $this->imageCacheDir . $this->imgCompleteName );
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc In case of a GIF image, this will print it.
     */
    private function printImgGIF()
    {
        $this->workingImg->mime_type = 'image/png';
        return $this->printImgPNG();
        //header('Content-Length: ' . strlen( $this->workingCrop ) );
        header('Content-type: ' . $this->workingImg->mime_type );
        //header('Last-Modified: ' . gmdate('D, d M Y H:i:s', strtotime( $this->workingImg->date ) ).' GMT');
        header('Expires: ' . gmdate('D, d M Y H:i:s',  strtotime( $this->workingImg->date ) + 86400*50).' GMT');
        header( "Cache-Control: store, cache, max-age=" . $this->imgCacheTime, true );
        header('Pragma: public');
		header( "ETag: " . $this->page->page_complete_url, true );
        header('Pragma: public');
		// Salva

		imagegif( $this->workingCrop, $this->imageCacheDir . $this->imgCompleteName, $this->imgQuality );
		if( $this->page->compression == true ):
			header( "Vary: Accept-Encoding", true );
			header( "Content-Encoding: gzip", true );
			// Converte
			$tmp_res = file_get_contents( $this->imageCacheDir . $this->imgCompleteName );
			file_put_contents( $this->imageCacheDir . $this->imgCompleteName, gzencode( $tmp_res, 5, FORCE_GZIP ) );
		endif;
		header('Content-Length: ' . filesize( $this->imageCacheDir . $this->imgCompleteName ) );
        header( "Last-Modified: " . gmdate( 'D, d M Y H:i:s', filemtime( $this->imageCacheDir . $this->imgCompleteName ) ) . ' GMT', true );
        //imagejpeg( $this->workingCrop, null, $this->imgQuality );
		//Imprime

        header( "Parser-bot: RmG GIF", true, 200 );
        header( "Parser-bot-image-size: " . $this->workingImg->width . 'x' . $this->workingImg->height );
		echo file_get_contents( $this->imageCacheDir . $this->imgCompleteName );
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc In case of a PNG image, this will print it.
     */
    private function printImgPNG()
    {
        //Antes, em vez de strlen estava filesize()
        //header('Content-Length: ' . strlen( $this->workingCrop ) );
        header('Content-type: ' . $this->workingImg->mime_type );
        //header('Last-Modified: ' . gmdate('D, d M Y H:i:s', strtotime( $this->workingImg->date ) ).' GMT');
        header('Expires: ' . gmdate('D, d M Y H:i:s',  strtotime( $this->workingImg->date ) + 86400*50).' GMT');
		header( "Cache-Control: store, cache, max-age=" . $this->imgCacheTime, true );
		header('Pragma: public');

		header( "ETag: " . $this->page->page_complete_url, true );
        header('Pragma: public');

        //zmorris at zsculpt dot com function, a bit completed
       /* function ImageTrueColorToPalette2($image, $dither, $ncolors,$w,$h) {
            $width = $w;
            $height = $h;
            $colors_handle = imagecreatetruecolor( $width, $height );
            ImageCopyMerge( $colors_handle, $image, 0, 0, 0, 0, $width, $height, 100 );
            ImageTrueColorToPalette( $image, $dither, $ncolors );
            ImageColorMatch( $colors_handle, $image );
            ImageDestroy($colors_handle);
            return $image;
        }

        $this->workingCrop = ImageTrueColorToPalette2($this->workingCrop,false,255,$this->workingImg->width,$this->workingImg->height);
        */
		// Salva
		imagepng( $this->workingCrop, $this->imageCacheDir . $this->imgCompleteName, $this->imgCompression );
		if( $this->page->compression == true ):
			header( "Vary: Accept-Encoding", true );
			header( "Content-Encoding: gzip", true );
			// Converte
			$tmp_res = file_get_contents( $this->imageCacheDir . $this->imgCompleteName );
			file_put_contents( $this->imageCacheDir . $this->imgCompleteName, gzencode( $tmp_res, 5, FORCE_GZIP ) );
		endif;

		header('Content-Length: ' . filesize( $this->imageCacheDir . $this->imgCompleteName ) );
        header( "Last-Modified: " . gmdate( 'D, d M Y H:i:s', filemtime( $this->imageCacheDir . $this->imgCompleteName ) ) . ' GMT', true );
        //imagejpeg( $this->workingCrop, null, $this->imgQuality );
		//Imprime

        header( "Parser-bot: RmG PNG", true, 200 );
        header( "Parser-bot-image-size: " . $this->workingImg->width . 'x' . $this->workingImg->height );

		echo file_get_contents( $this->imageCacheDir . $this->imgCompleteName );
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc This connects to tghe database, and returns the result from DB
     * @return array
     */
    private function fetchImgParams()
    {
        return;
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc Checks the image extension, and compares it.
     */
    private function checkImgExtension()
    {
		$r_tmp = explode( '.', $this->imgName );
        switch( strtolower( array_pop( $r_tmp ) ) )
        {
            case 'jpg':
                $this->printImgJPEG();
                break;
            case 'jpeg':
                $this->printImgJPEG();
                break;
            case 'gif':
                $this->printImgGIF();
                break;
            case 'png':
                $this->printImgPNG();
                break;
            default:
                $this->printImgJPEG();
                break;
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-07-13
     * @desc In case of no parameters given, this image will output the original
     */
    private function printOriginalImage()
    {
        $this->printImageOnDisk( $this->page->page_controller, false );
        //$this->printImageOnDisk( $this->imageCacheDir . $this->imgCompleteName, false );
		die;
		/*
		unset( $this->workingImg );
        $this->workingImg = (object) $this->fetchImgParams();

        header('Content-Length: '.$this->workingImg->size );
        header('Content-type: '.$this->workingImg->mime_type );
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', strtotime( $this->workingImg->date ) ).' GMT');
        header('Expires: '.gmdate('D, d M Y H:i:s',  strtotime( $this->workingImg->date ) + 86400*150).' GMT');
        header('Cache-Control: max-age=86400, must-revalidate');
        header('Pragma: public');

        //Outputing image
        print $this->workingImg->image_blob;
        die;
		*/
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-08-23
     * @desc This function checks if the image exists on disk
     */
    private function fileExistsOnDisk( $imagefullpath )
    {
        $fileExists = file_exists( $imagefullpath );
        if( $fileExists == true )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-08-23
     * @desc This function grabs the parsed image and saves it on disk for future use/output
     */
    private function saveImageOnDisk( $image = null )
    {
        if( !empty( $image ) )
        {
            $dir = $this->imageCacheDir . $this->imgCompleteName;
            $filename = fopen( $dir, 'w+' );
            fwrite( $filename, $image );
            fclose( $filename );
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @author Ricardo Goulart <ricardo.goulart@digital-work.com>
     * @date 2011-08-23
     * @desc This function grabs the parsed image on disk and prints it in case of existance
     */
    private function printImageOnDisk( $imagePath = null, $cached = false )
    {
        if( !empty( $imagePath ) )
        {
            //$this->workingImg = (object) $this->fetchImgParams();
            $handle = file_get_contents( $imagePath );
            $sizeOnFile = filesize( $imagePath );
            $time = filemtime( $imagePath );
            $fileDate = date("D, d M Y H:i:s", $time );
			header( "ETag: " . $this->page->page_complete_url, true );
            header( 'Content-Length: '.$sizeOnFile );
            //header( 'Content-type: '.$this->mimetypes[exif_imagetype($imagePath)] );
            header( "Expires: " . gmdate( 'D, d M Y H:i:s', $time+$this->page->expiry ) . ' GMT', true );
			header( "Last-Modified: " . gmdate( 'D, d M Y H:i:s', filemtime( $imagePath ) ) . ' GMT', true );
			header( "Cache-Control: store, cache, max-age=" . $this->imgCacheTime, true );
            header( 'Pragma: public', true, 200 );
			if( $cached === true ):
				header( "Parser-bot: Cached", true );
				if( $this->page->compression == true ):
					header( "Vary: Accept-Encoding", true );
					header( "Content-Encoding: gzip", true );
                    $findExt = array_pop(explode(".", $imagePath ));
                    header( 'Content-type: ' . $this->mimetypes[$findExt] );
                else:
                    header( 'Content-type: ' . image_type_to_mime_type(exif_imagetype($imagePath)) );
				endif;
			else:
				header( "Parser-bot: Original", true );
                if( $this->page->compression == true  ):
					//header( "Vary: Accept-Encoding", true );
					//header( "Content-Encoding: gzip", true );
                    $findExt = array_pop(explode(".", $imagePath ));
                    header( 'Content-type: ' . $this->mimetypes[$findExt] );
                    //$handle = gzencode( $handle, 9, FORCE_GZIP );
                else:
                    header( 'Content-type: ' . image_type_to_mime_type(exif_imagetype($imagePath)) );
				endif;
			endif;
            //Outputing image
            print $handle;
			die();
        }
    }
}

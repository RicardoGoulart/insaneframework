<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-04 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

define( "PROTECTED_SPEACH", true );

// Request insaneConfig file
require_once __DIR__ . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.interface.inc.php";

$autoloaders = array();

// Active modules
$autoloaders['logger']      = 'logger.class.inc.php';
$autoloaders['minifier']    = 'minifier.class.inc.php';
$autoloaders['robots']      = 'robots.class.inc.php';
$autoloaders['router']      = 'router.class.inc.php';
$autoloaders['cacher']      = 'cacher.class.inc.php';
//$autoloaders['parsers'][]   = 'css.class.inc.php';
$autoloaders['parsers'][]   = 'js.class.inc.php';
//$autoloaders['connectors']  = 'database.class.inc.php';
//$autoloaders['wp-contents'][] = 'header.class.inc.php';
//$autoloaders['wp-contents'][] = 'footer.class.inc.php';
//$autoloaders['wp-contents'][] = 'content.class.inc.php';

#TODO: language connects to Database (wordpress plugin dependent) 
#TODO: Database must be in PDO. Must accept others than MYSQL .... like mariaDb
#TODO: wordpress connector class. (make it simple plz)
#TODO: Curl connector new insaneCurl( URL, "JSON" = 1 )


// Request all files...
foreach( $autoloaders as $folder => $filename ):
  if( is_array( $filename ) ):
    foreach( $filename as $fileKey => $filePath ):
      require_once __DIR__ . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $filePath;
      insaneLogger::log( "Finished Loading the module: " . $folder . DIRECTORY_SEPARATOR . $filePath );
    endforeach;
  else: 
    require_once __DIR__ . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $filename;
    insaneLogger::log( "Finished Loading the module: " . $folder . DIRECTORY_SEPARATOR . $filename );
  endif;
endforeach;
<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   0.1
 * @since     4.0.1
 */

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

class insaneConfig {
  /**
  * Defines if in production or not.
  * @author     Ricardo Goulart <ricardo@goulart.pt>
  * @version    0.1
  * @since      4.0.1
  * @var        boolean STAGE constant
  */
  const STAGE   = true;

  /**
  * Defines if errors should be logged or not
  * @author     Ricardo Goulart <ricardo@goulart.pt>
  * @version    0.1
  * @since      4.0.1
  * @var        boolean ERRORS constant
  */
  const ERRORS  = false;

  /**
  * Defines the version
  * @author     Ricardo Goulart <ricardo@goulart.pt>
  * @version    0.1
  * @since      4.0.1
  * @var        boolean VERSION constant
  */
  const VERSION = "4.0.3";

  /**
  * Defines the author
  * @author     Ricardo Goulart <ricardo@goulart.pt>
  * @version    0.1
  * @since      4.0.1
  * @var        boolean AUTHOR constant
  */
  const AUTHOR  = "CrazZy // Ins@n0";

  /**
  * Defines the time limit of php execution
  * @author     Ricardo Goulart <ricardo@goulart.pt>
  * @version    0.1
  * @since      4.0.1
  * @var        integer TIMELIMIT constant
  */
  const TIMELIMIT = 30;

  /**
  * Defines the disallowed folders on robots.txt
  * Dont forget to update the .htaccess when changing these values
  * @author     Ricardo Goulart <ricardo@goulart.pt>
  * @version    0.1
  * @since      4.0.1
  * @var        array $disallow
  */
  public static $disallow = array(
    "core",
    "logs",
    "routes",
    "cache",
    "assets"
  );

  /**
   * $sitemapURL
   *
   * @var string
   */
  public static $sitemapURL = "sitemap.xml";
  
  /**
  * Defines the content type of the responses
  * @author     Ricardo Goulart <ricardo@goulart.pt>
  * @version    0.1
  * @since      4.0.1
  * @var        string CONTENT_TYPE constant
  */
  const CONTENT_TYPE = 'text/html';

  /**
   * PROD_DB_HOST
   *
   * @var string
   */
  const PROD_DB_HOST = 'localhost';

  /**
   * PROD_DB_TYPE
   *
   * @var string
   */
  const PROD_DB_TYPE = 'mysql';

  /**
   * PROD_DB_NAME
   *
   * @var string
   */
  const PROD_DB_NAME = '';

  /**
   * PROD_DB_USER
   *
   * @var string
   */
  const PROD_DB_USER = '';

  /**
   * PROD_DB_PASS
   *
   * @var string
   */
  const PROD_DB_PASS = '';

  /**
   * PROD_DB_PORT
   *
   * @var string
   */
  const PROD_DB_PORT = '3306';

  /**
   * PROD_DB_CSET
   *
   * @var string
   */
  const PROD_DB_CSET = 'utf8';

  /**
   * DEV_DB_HOST
   *
   * @var string
   */
  const DEV_DB_HOST = 'localhost';

  /**
   * DEV_DB_TYPE
   *
   * @var string
   */
  const DEV_DB_TYPE = 'mysql';

  /**
   * DEV_DB_NAME
   *
   * @var string
   */
  const DEV_DB_NAME = 'wdp_qa_ooredoo_extra';

  /**
   * DEV_DB_USER
   *
   * @var string
   */
  const DEV_DB_USER = 'root';

  /**
   * DEV_DB_PASS
   *
   * @var string
   */
  const DEV_DB_PASS = '';

  /**
   * DEV_DB_PORT
   *
   * @var string
   */
  const DEV_DB_PORT = '3306';

  /**
   * DEV_DB_CSET
   *
   * @var string 
   */
  const DEV_DB_CSET = 'utf8';

  /**
   * $cssProvider
   *
   * @var string
   * 
   */
  public static $cssProvider = '//css.insane.fw/';

  /**
   * $jsProvider
   *
   * @var string
   */
  public static $jsProvider = '//js.insane.fw/';
}

// Load the AUTOLOADERS
require_once __DIR__ . DIRECTORY_SEPARATOR . "loader.class.inc.php";
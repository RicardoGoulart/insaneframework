<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-05 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

$newUrl = array();
if( strstr( $_SERVER['REQUEST_URI'], '?' ) ):
  $newUrl = explode( "?", $_SERVER['REQUEST_URI'] );
  $newUrl = $newUrl[0];
  header( "Location: " . $newUrl, true, 302 );
  header( "Insane-Reason: We dont allow that here.", true );
  exit();
endif;
if( strstr( $_SERVER['REQUEST_URI'], '#' ) ):
  $newUrl = explode( "#", $_SERVER['REQUEST_URI'] );
  $newUrl = $newUrl[0];
  header( "Location: " . $newUrl, true, 302 );
  header( "Insane-Reason: We dont allow that here either.", true );
  exit();
endif;
unset( $newUrl );

// Require core
require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'core.inc.php';

if( insaneConfig::ERRORS === true ):
  error_reporting( E_ALL );
else:
  error_reporting( 0 );
endif;

insaneLogger::registerPath( __DIR__ );
insaneRobots::registerPath( __DIR__ );
insaneRobots::checkRobots();

// Create a buffer #Create insaneBuffer???
//ob_start();

// Load project config file.
require_once __DIR__ . DIRECTORY_SEPARATOR . 'config.php';

// Require Headers
require_once __DIR__ . DIRECTORY_SEPARATOR . 'headers.php';

// Load the starter file.
require_once __DIR__ . DIRECTORY_SEPARATOR . 'start.php';

// Load destruct sequence.
require_once __DIR__ . DIRECTORY_SEPARATOR . 'destruct.php';

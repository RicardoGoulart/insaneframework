  <?php 

    require_once __DIR__ . DIRECTORY_SEPARATOR . '_footer_content.inc.php';

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'atobbtoa.min.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'jquery-3.2.1.min.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'tether.min.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'popper.min.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'bootstrap.min.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'ie10-viewport-bug-workaround.min.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'font-awesome-5-all.min.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );

    // Lets add another one!
    insaneFooter::addScript( 
      // Define the URL
      __DIR__ . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'easyCore.js',
      // Is it external ? No!
      false,
      // We want more than just this file? No!
      false
    );
    
    echo insaneHeader::getStyles( true, false );
    echo insaneHeader::getCodes();

    echo self::getScripts();
    echo self::getCodes();
    
  ?>
  </body>
</html>
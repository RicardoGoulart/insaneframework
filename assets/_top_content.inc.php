<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

?>

<!-- First navbar /-->

<nav id="primary-top-navbar" class="navbar navbar-expand-lg navbar-dark bg-primary py-1">
  <div class="container justify-content-end">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link text-secondary" href="#" title="<?=__( "Login Text Top Menu" );?>">
          <?=__( "Login Text Top Menu" );?>
        </a>
      </li>
      <li class="nav-item">
        <div class="form-group p-0 m-0">
          <form action="#">
            <select name="test" class="custom-select bg-secondary text-white">
              <?php
                foreach( insaneLanguage::$languages as $index => $language ):
                  ?>
                    <option value="<?=$language[0];?>" title="<?=$language[3];?>"><?=$language[3];?></option>
                  <?php
                endforeach; 
              ?>
            </select>
          </form>
        </div>
      </li>
    </ul>

  </div>
</nav>

<!-- Second navbar /-->
<nav id="primary-menu-top-navbar" class="navbar navbar-expand-lg navbar-light py-0">
  <div class="container">

    <a title="uCell" class="navbar-brand" href="/<?=insaneLanguage::getCurrentLanguageKey();?>">
      <img class="img-fluid insaneImage" data-ii-url="//img.insane.fw/<?=strrev( base64_encode( __DIR__ . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'Ucell_logo.png' ) );?>" data-ii-method="width" data-ii-quality="90" src="//img.insane.fw/<?=strrev( base64_encode( __DIR__ . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'Ucell_logo.png' ) );?>/<?=strrev( base64_encode( json_encode( array( 'calcWidth' => 90, 'method' => 'width', 'quality' => 2 ) ) ) )?>" alt="uCell" style="max-height: 100%; height: 100%;" />
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainTopMenu" aria-controls="mainTopMenu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="mainTopMenu">
      <ul class="col-12 navbar-nav nav-fill">
        
        <li class="nav-item bg-light py-3">
          <div class="showTrianglesOnMenu"></div>
          <a style="text-transform: uppercase; opacity: 0.7;" class="nav-link disabled text-primary">
            <strong><?=__("Top Menu Text Education");?></strong>
          </a>
        </li>

        <?php
          insaneHeader::addCode( 
            "
              #primary-menu-top-navbar .showTrianglesOnMenu {
                position: relative;
                float: left;
                width: 100%;
              }

              #primary-menu-top-navbar .showTrianglesOnMenu:before {
                content: '';
                position: absolute;
                left: -8px;
                top: 12px;
                background-color: #FFF;
                width: 15px;
                height: 15px;
                -moz-transform: rotate(45deg);
                -webkit-transform: rotate(45deg);
                -o-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
              }

              #primary-menu-top-navbar .showTrianglesOnMenu:after {
                content: '';
                position: absolute;
                right: -8px;
                top: 12px;
                background-color: #F9F8FC;
                width: 15px;
                height: 15px;
                -moz-transform: rotate(45deg);
                -webkit-transform: rotate(45deg);
                -o-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
              }
            "
          );
        ?>

        <?php
          $topMenuResult = insaneWordpress::getMenu( "top" );
          if( $topMenuResult->items ):
            foreach( $topMenuResult->items as $key => $item ):
              if( $item->children ):
                ?>
                <li class="nav-item dropdown <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?> py-3">
                  <a class="nav-link dropdown-toggle text-primary" id="<?=md5( $topMenuResult->name . $item->ID );?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$item->title;?></a>
                    <div class="dropdown-menu" aria-labelledby="<?=md5( $topMenuResult->name . $item->ID );?>">
                    <?php
                    foreach( $item->children as $childKey => $childItem ):
                      ?>
                        <a class="dropdown-item <?=insaneWordpress::urlMatch( $childItem ) ? 'active' : ''?>" href="<?=$childItem->url;?>"><?=$childItem->title;?></a>
                      <?php
                    endforeach;
                    ?>
                    </div>
                </li>
                <?php
              else:
                ?>
                  <li class="nav-item <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?> py-3">
                    <a class="nav-link text-primary" href="<?=$item->url;?>"><?=$item->title;?></a>
                  </li>
                <?php
              endif;
            endforeach;
          else:
            ?>
            <li class="nav-item active py-3">
              <a class="nav-link" href="#">Menu has no content</a>
            </li>
            <?php
          endif;
        ?>
        <li class="nav-item py-3">
          <a class="nav-link text-primary" href="#"><i class="fas fa-search"></i></a>
        </li>
      </ul>
    </div>

  </div>
</nav>

<?php
  insaneHeader::addCode(
    "
    #primary-menu-top-navbar {
      border-bottom: 2px solid #DDD;
    }
    "
  );
?>

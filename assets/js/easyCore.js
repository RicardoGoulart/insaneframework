

var insaneScript = {
    image : {
        minPixel : 5,

        interval : 0,

        init : function(){
            $( window ).resize( insaneScript.image.get );
            setTimeout(function(){
                insaneScript.image.get();
            }, 100 );
        },

        get : function(){
            var execute = function(){
                clearInterval( insaneScript.image.interval );
                $(".insaneImage").each(function(){
                    var self = this;
                    var parent = $(self).parent();
                    var calcWidth = Math.floor( parent.innerWidth() );
                    var calcHeight = Math.floor( parent.innerHeight() );
                    var confirmedWidth = calcWidth >= insaneScript.image.minPixel;
                    var method = $(self).data( "iiMethod" );
                    var url = $(self).data( "iiUrl" );
                    var quality = $(self).data( "iiQuality" );
                    if( confirmedWidth && url && method ){
                        var params = {
                            'url' : url,
                            'method' : method,
                            'quality' : quality,
                            'calcWidth' : calcWidth,
                            'calcHeight' : calcHeight
                        };
                        setTimeout(function(){
                            insaneScript.image.encode( params, self );
                        }, 100 );                    
                    }
                });  
            };

            if( insaneScript.image.interval > 0 ){
                clearInterval( insaneScript.image.interval );
                insaneScript.image.interval = setInterval( execute, 2500 );
                return false;
            } else {
                insaneScript.image.interval = setInterval( execute, 500 );
                return false;
            }
        },

        reverse : function( input ){
            if( typeof( input ) == "string" ){
                return input.split("").reverse().join("");
            }
        },

        encode : function( params, context ){
            if( typeof( params ) == 'object' ){
                var stringifyed = JSON.stringify( params );
                var encoded = window.btoa( stringifyed );
                var reversed = insaneScript.image.reverse( encoded );
                var url = params.url + "/" + reversed;
                insaneScript.image.apply( url, context );
            } else {
                console.warn( "Type not recognized." );
                return false;
            }
        },

        apply : function( url, context ){
            $(context).prop( "src", url );
        }
    }
};

$(document).ready(function(){
    insaneScript.image.init();
});
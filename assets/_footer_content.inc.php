<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

?>
<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

?>
<div class="concreteBackground">
  <div class="container py-3">
    
    <div class="row">
      <div class="col-12 col-xs-12  pt-3">
        <h2 style="text-transform: uppercase;"><?=__( "Top Weekly" );?></h2>
      </div>
    </div>

    <div id="front-page-carousel-2" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">

        <div class="carousel-item active" style="padding: 0.5%;">
          <div class="card-deck">
            <?php
              foreach( array( '', '' ,'' ) as $index => $serviceResponse ):
              ?>
                <div class="card">
                  <?php
                    if( $index == 0 || $index == 2 ):
                  ?>
                    <div class="ribbon"><span>New</span></div>
                  <?php
                    endif;
                  ?>
                  <img class="card-img-top img-fluid insaneImage" data-ii-url="//img.insane.fw/<?=strrev( base64_encode( '//placehold.it/2001x1001' ) );?>" data-ii-method="width" data-ii-quality="80" src="//img.insane.fw/<?=strrev( base64_encode( 'http://placehold.it/2000x1000' ) );?>/<?=strrev( base64_encode( json_encode( array( 'calcWidth' => 100, 'method' => 'width', 'quality' => 90 ) ) ) )?>" alt="Card image cap" />
                  <div class="card-body">
                    <h3 class="card-title text-primary text-center">Card title</h3>
                    <p class="card-text text-center">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                  <div class="card-footer text-center">
                    <a href="#" class="btn btn-success btn-lg">Subscribe</a>
                  </div>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              <?php
              endforeach;
            ?>
          </div>
        </div>
        
      </div>

      <a title="<?=__( "&laquo; Previous" );?>" class="carousel-control-prev bg-primary" href="#front-page-carousel-2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"><?=__( "&laquo; Previous" );?></span>
      </a>

      <a title="<?=__( "Next &raquo;" );?>" class="carousel-control-next bg-primary" href="#front-page-carousel-2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"><?=__( "Next &raquo;" );?></span>
      </a>
      
    </div>
  </div>


  <?php
  insaneHeader::addCode(
    "
    #front-page-carousel-2 .carousel-control-prev, 
    #front-page-carousel-2 .carousel-control-next {
      width: 3%;
      min-width: 15px;
      opacity: 1;
      top: 20%;
      bottom: auto;
      padding-top: 1%;
      padding-bottom: 1%;
      font-weight: light;
    }

    #front-page-carousel-2 .carousel-control-prev {
      left: -1.5%;
    }

    #front-page-carousel-2 .carousel-control-next {
      right: -1.5%;
    }

    #front-page-carousel-2 .card {
      background-color: #EEE;
    }

    #front-page-carousel-2 .card .progress-bar {
      width: 0%;
    }

    #front-page-carousel-2 .card:hover .progress-bar {
      width: 100%;
    }

    #front-page-carousel-2 .card:hover {
      cursor: pointer;
      background-color: #FFF;

      -webkit-transition: all 550ms ease-in-out;
      -moz-transition: all 550ms ease-in-out;
      -ms-transition: all 550ms ease-in-out;
      -o-transition: all 550ms ease-in-out;
      transition: all 550ms ease-in-out;
    } 
    "
  );

  insaneHeader::addCode(
    ".concreteBackground {
      background-color: #DDD;
      background-image: url( '//img.insane.fw/" . strrev( base64_encode( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'concrete_bg.jpg' ) ) . "/" . strrev( base64_encode( json_encode( array( 'calcWidth' => 80, 'method' => 'width', 'quality' => 89 ) ) ) ) . "' );
      background-position: center center;
      
      background-repeat: repeat;
    }
    "
  );

  ?>
</div>

<div id="footer-widgets" class="wrapper navbar navbar-dark bg-primary bg-primary">
  <div class="container">
    <div class="row">

        <div class="col-12 col-xs-12  col-sm-3">
          <ul class="navbar-nav">
          <?php
            $footerMenu = insaneWordpress::getMenu( "footer_menu_1" );
            if( $footerMenu->items ):
              ?>
                <li class="nav-item active title">
                  <a class="nav-link" href="#"><?=$footerMenu->menu->name?></a>
                </li>
              <?php
              foreach( $footerMenu->items as $key => $item ):
                if( $item->children ):
                  ?>
                  <li class="nav-item dropdown <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                    <a class="nav-link dropdown-toggle" id="<?=md5( $footerMenu->name . $item->ID );?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$item->title;?></a>
                      <div class="dropdown-menu" aria-labelledby="<?=md5( $footerMenu->name . $item->ID );?>">
                      <?php
                      foreach( $item->children as $childKey => $childItem ):
                        ?>
                          <a class="dropdown-item <?=insaneWordpress::urlMatch( $childItem ) ? 'active' : ''?>" href="<?=$childItem->url;?>"><?=$childItem->title;?></a>
                        <?php
                      endforeach;
                      ?>
                      </div>
                  </li>
                  <?php
                else:
                  ?>
                    <li class="nav-item <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                      <a class="nav-link" href="<?=$item->url;?>"><?=$item->title;?></a>
                    </li>
                  <?php
                endif;
              endforeach;
            else:
              ?>
              <li class="nav-item active">
                <a class="nav-link" href="#">Menu has no content</a>
              </li>
              <?php
            endif;
            ?>
          </ul>
        </div>

        <div class="col-12 col-xs-12  col-sm-3">
          <ul class="navbar-nav">
          <?php
            $footerMenu = insaneWordpress::getMenu( "footer_menu_2" );
            if( $footerMenu->items ):
              ?>
                <li class="nav-item active title">
                  <a class="nav-link" href="#"><?=$footerMenu->menu->name?></a>
                </li>
              <?php
              foreach( $footerMenu->items as $key => $item ):
                if( $item->children ):
                  ?>
                  <li class="nav-item dropdown <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                    <a class="nav-link dropdown-toggle" id="<?=md5( $footerMenu->name . $item->ID );?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$item->title;?></a>
                      <div class="dropdown-menu" aria-labelledby="<?=md5( $footerMenu->name . $item->ID );?>">
                      <?php
                      foreach( $item->children as $childKey => $childItem ):
                        ?>
                          <a class="dropdown-item <?=insaneWordpress::urlMatch( $childItem ) ? 'active' : ''?>" href="<?=$childItem->url;?>"><?=$childItem->title;?></a>
                        <?php
                      endforeach;
                      ?>
                      </div>
                  </li>
                  <?php
                else:
                  ?>
                    <li class="nav-item <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                      <a class="nav-link" href="<?=$item->url;?>"><?=$item->title;?></a>
                    </li>
                  <?php
                endif;
              endforeach;
            else:
              ?>
              <li class="nav-item active">
                <a class="nav-link" href="#">Menu has no content</a>
              </li>
              <?php
            endif;
            ?>
          </ul>
        </div>

        <div class="col-12 col-xs-12  col-sm-3">
          <ul class="navbar-nav">
          <?php
            $footerMenu = insaneWordpress::getMenu( "footer_menu_3" );
            if( $footerMenu->items ):
              ?>
                <li class="nav-item active title">
                  <a class="nav-link" href="#"><?=$footerMenu->menu->name?></a>
                </li>
              <?php
              foreach( $footerMenu->items as $key => $item ):
                if( $item->children ):
                  ?>
                  <li class="nav-item dropdown <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                    <a class="nav-link dropdown-toggle" id="<?=md5( $footerMenu->name . $item->ID );?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$item->title;?></a>
                      <div class="dropdown-menu" aria-labelledby="<?=md5( $footerMenu->name . $item->ID );?>">
                      <?php
                      foreach( $item->children as $childKey => $childItem ):
                        ?>
                          <a class="dropdown-item <?=insaneWordpress::urlMatch( $childItem ) ? 'active' : ''?>" href="<?=$childItem->url;?>"><?=$childItem->title;?></a>
                        <?php
                      endforeach;
                      ?>
                      </div>
                  </li>
                  <?php
                else:
                  ?>
                    <li class="nav-item <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                      <a class="nav-link" href="<?=$item->url;?>"><?=$item->title;?></a>
                    </li>
                  <?php
                endif;
              endforeach;
            else:
              ?>
              <li class="nav-item active">
                <a class="nav-link" href="#">Menu has no content</a>
              </li>
              <?php
            endif;
            ?>
          </ul>
        </div>

        <div class="col-12 col-xs-12  col-sm-3">
          <ul class="navbar-nav">
          <?php
            $footerMenu = insaneWordpress::getMenu( "footer_menu_4" );
            if( $footerMenu->items ):
              ?>
                <li class="nav-item active title">
                  <a class="nav-link" href="#"><?=$footerMenu->menu->name?></a>
                </li>
              <?php
              foreach( $footerMenu->items as $key => $item ):
                if( $item->children ):
                  ?>
                  <li class="nav-item dropdown <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                    <a class="nav-link dropdown-toggle" id="<?=md5( $footerMenu->name . $item->ID );?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$item->title;?></a>
                      <div class="dropdown-menu" aria-labelledby="<?=md5( $footerMenu->name . $item->ID );?>">
                      <?php
                      foreach( $item->children as $childKey => $childItem ):
                        ?>
                          <a class="dropdown-item <?=insaneWordpress::urlMatch( $childItem ) ? 'active' : ''?>" href="<?=$childItem->url;?>"><?=$childItem->title;?></a>
                        <?php
                      endforeach;
                      ?>
                      </div>
                  </li>
                  <?php
                else:
                  ?>
                    <li class="nav-item <?=insaneWordpress::urlMatch( $item ) ? 'active' : ''?>">
                      <a class="nav-link" href="<?=$item->url;?>"><?=$item->title;?></a>
                    </li>
                  <?php
                endif;
              endforeach;
            else:
              ?>
              <li class="nav-item active">
                <a class="nav-link" href="#">Menu has no content</a>
              </li>
              <?php
            endif;
            ?>
          </ul>
        </div>

    </div>
  </div>
</div>

<?php
  insaneHeader::addCode(
    "
      #footer-widgets .container .row .nav-item.title {
        border-bottom: 2px solid rgba( 254,254,254,0.2 );
      }
    "
  );
?>

<div class="wrapper">
  <footer>
    <div class="container pt-2">
      <p>
        <?=__("Copyright YEAR COMPANY");?>
        <br />
        <?=__("All rights reserved");?>
        <br />
        <?=__("The services are licenced");?>
      </p>
    </div>
  </footer>
</div>
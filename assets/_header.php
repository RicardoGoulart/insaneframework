<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

// Now lets add a style we have in the assets folder
self::addStyle( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'bootstrap.min.css',
  // Is it external ? No!
  false,
  // We want more than just this file?
  false
);

// Now lets add a style we have in the assets folder
// Isto apenas serve de fallback, caso o user nao tenha suporte para SVG... // Chato mas tem que ser...
self::addStyle( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'fontawesome-all.min.css',
  // Is it external ? No!
  false,
  // We want more than just this file?
  false
);

// Now lets add a style we have in the assets folder
self::addStyle( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'styles.css',
  // Is it external ? No!
  false,
  // We want more than just this file?
  false
);

?>
<!DOCTYPE html>
<html lang="<?=self::$lang;?>">
  <head>
    <title><?=self::$title;?></title>
    <meta charset="utf-8"/>
    <?=self::getMetaTags();?>
    <?=self::getStyles( false, true );?>
  </head>
  <body>
    <?php
      require_once __DIR__ . DIRECTORY_SEPARATOR . '_top_content.inc.php';
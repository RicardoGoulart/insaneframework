<?php
// Vamos primeiro partir o URL
$pieces = explode( "/", trim( $_SERVER['REQUEST_URI'], "/" ) );
if( !empty( $pieces ) ):
  foreach( $pieces as $key => $piece ):
    $decode = insaneCSS::parseCSSRoute( $piece );
    insaneCSS::addStyle( $decode );
  endforeach;
  insaneCSS::parseStyles();
  echo insaneCSS::getStyles( true );
else:
  ?>
  /* No Content */
  <?php
endif;
<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-05 Happy new Year!
 * @version   0.2
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
    die( "Unauthorized Insane Access! GTFO!" );
endif;

// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig" ), __FILE__ );

class insaneLogger {
  /**
  * Holds the log messages
  * @author       Ricardo Goulart <ricardo@goulart.pt>
  * @version      0.2
  * @var          private $logs
  */
  private static $logs = array();

  /**
   * $folderName
   *
   * @var string
   */
  private static $folderName = 'logs';

  /**
   * $logFolder
   *
   * @var null
   */
  private static $logFolder = null;

  /**
   * __construct
   *
   * @return void
   */
  public function __construct(){
    self::log( "insaneLogger Class initiated! " );
    self::log( "Environment: " . ( insaneConfig::STAGE ? 'Production' : 'Development' ) );
    self::log( "Show errors: " . ( insaneConfig::ERRORS ? 'Yup' : 'Nope' ) );
    self::log( "Version: " . insaneConfig::VERSION );
    self::log( "Author: " . insaneConfig::AUTHOR );

    insaneLoader::registerDestruct( __CLASS__, "writeFile" );
  }

  /**
  * Logs a message to the error log stack
  * @author       Ricardo Goulart <ricardo@goulart.pt>
  * @version      0.2
  * @param        string message
  * @return       boolean
  */
  public static function log( $message = null ){
    if( !empty( $message ) ):
      $index                              = md5( rand( 0, 999 ) . microtime() . rand( 0, 999 ) );
      self::$logs[ $index ]                = new stdClass();
      self::$logs[ $index ]->timestamp     = date( "Y-m-d // H:i:s ::" ) . round( microtime( true ) * 10000 );
      self::$logs[ $index ]->message       = $message;
      self::$logs[ $index ]->readonly      = "[" . self::$logs[ $index ]->timestamp . "] ## " . self::$logs[ $index ]->message;
      return true;
    else:
      self::log( "Empty log entry was detected." );
      return false;
    endif;
  }

  /**
   * registerPath
   *
   * @param mixed $path
   * @return void
   */
  public static function registerPath( $path = null ){
    if( !empty( $path ) ):
      if( file_exists( $path ) ):
        self::$logFolder = $path . DIRECTORY_SEPARATOR . self::$folderName;
        return true;
      else:
        self::log( "Log folder dosent exist..." );
        return false;
      endif;
    else:
      self::log( "No log folder specified..." );
      return false;
    endif;
  }

  /**
  * Returns the messages stored in the $logs
  * @author       Ricardo Goulart <ricardo@goulart.pt>
  * @version      0.2
  * @param        string stringMode
  * @return       boolean
  */
  public static function getLogs( $stringMode = true ){
    if( $stringMode ):
      $buffer = PHP_EOL . "---------------------" . PHP_EOL;
      foreach( self::$logs as $key => $val ):
        $buffer .= $val->readonly . PHP_EOL;
      endforeach;
      return $buffer . "---------------------" . PHP_EOL;
    else:
      return self::$logs;
    endif;
  }

  /**
   * __destruct
   *
   * @return void
   */
  public static function writeFile( $force = false ){
    // If environment == dev, save a log file for each execution on a log folder.
    if( insaneConfig::STAGE === false || $force === true ):
      self::log( "Destruct function called for logs!" );
      if( !empty( self::$logFolder ) ): 
        // create a new folder based on date:
        self::$logFolder .= DIRECTORY_SEPARATOR . date( "Y" ) . DIRECTORY_SEPARATOR . date( "m" ) . DIRECTORY_SEPARATOR . date( "d" );       
        if( file_exists( self::$logFolder ) && is_dir( self::$logFolder ) ):
          self::log( "Log folder exists. Using it..." );
        else:
          self::log( "Log folder dosent exist. Attempting to create it..." );
          if( mkdir( self::$logFolder, 0775, true ) ):
            self::log( "Log folder created successfully." );
          else:
            self::log( "Failed to create log folder, check permissions or something wtf..!!! WAKE UP!!" );
            return false;
          endif;
        endif;
        
        $fileHandler = fopen( self::$logFolder . DIRECTORY_SEPARATOR . 'log_' . time() . ".log", "a+" );
        self::log( "Writting in log-file." );
        fputs( $fileHandler, self::getLogs( true ) );
        fclose( $fileHandler );
        self::log( "Logfile write was completed successfully." );
        return true;
      else:
        self::log( "Log folder was not specified. Use insaneLogger::registerLogger( directory ) to enable this functionality." );
        return false;
      endif;
    else:
      self::log( "When in production environment, logs wont be registered for performance reasons." );
      return false;
    endif;    
  }
}

new insaneLogger();
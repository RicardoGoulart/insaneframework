<?php
// Vamos primeiro partir o URL
$pieces = explode( "/", trim( $_SERVER['REQUEST_URI'], "/" ) );
if( !empty( $pieces ) ):
  foreach( $pieces as $key => $piece ):
    $decode = insaneJS::parseJSRoute( $piece );
    insaneJS::addScript( $decode );
  endforeach;
  insaneJS::parseScripts();
  echo insaneJS::getScripts( true );
else:
  ?>
  /* No Content */
  <?php
endif;
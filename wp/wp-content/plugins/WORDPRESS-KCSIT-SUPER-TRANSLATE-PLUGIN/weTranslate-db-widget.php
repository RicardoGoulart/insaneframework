<?php

/**
 *
 * Dashboard Widget for weTranslate
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 1.1
 * @package Wordpress
 * @subpackage weTranslate
 *
 */

class weTranslate_dashboard_widget {

    /**
     * Stores the $wpdb
     * @var $db null
     */
    private static $db = null;

    /**
     * Stores the WP table prefix
     * @var null
     */
    private static $table_prefix = null;

    /**
     * Constructor for this class
     * @author Ricardo Goulart
     * @param $db
     * @param $table_prefix
     */
    public function __construct( $db = null, $table_prefix = null ) {
        if( self::$db === null && !empty( $db ) ):
            self::$db = $db;
        endif;

        if( self::$table_prefix === null && !empty( $table_prefix ) ):
            self::$table_prefix = $table_prefix;
        endif;

        wp_add_dashboard_widget( 'dashboard_widget', '<div class="dashicons dashicons-editor-spellcheck"></div> weTranslate', array( __CLASS__, 'st_request_db_w_view' ) );
    }

    /**
     * Loads the widget view
     * @author Ricardo Goulart
     */
    public static function st_request_db_w_view() {
        require_once dirname( __FILE__ ) . '/views/db_widget.php';
    }

    /**
     * Queries the DB for metrics to be displayed in graph mode
     * @author Ricardo Goulart
     * @return Array
     */
    public static function st_get_graph_metrics() {

        $to_ret = array(
            'unique_records'    => 0,
            'active_records'    => 0,
            'inactive_records'  => 0
        );

        $sql = "SELECT COUNT( DISTINCT( slug ) ) AS total FROM " . self::$table_prefix . "weTranslate_languages";
        $res = self::$db->get_row( $sql, ARRAY_A );

        if( !empty( $res ) ):
            $to_ret['unique_records'] = (int) $res['total'];
        else:
            return false;
        endif;

        $sql = "SELECT COUNT( DISTINCT( slug ) ) AS total FROM " . self::$table_prefix . "weTranslate_languages WHERE active = 0";
        $res = self::$db->get_row( $sql, ARRAY_A );

        if( !empty( $res ) ):
            $to_ret['inactive_records'] = (int) $res['total'];
            $to_ret['active_records'] = (int) ( $to_ret['unique_records'] - $to_ret['inactive_records'] );
        endif;

        return $to_ret;
    }
}

new weTranslate_dashboard_widget( $wpdb, $table_prefix );
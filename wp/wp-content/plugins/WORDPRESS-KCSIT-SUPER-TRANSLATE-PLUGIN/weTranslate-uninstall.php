<?php

/**
 *
 * Uninstall weTranslate
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 1.1
 * @package Wordpress
 * @subpackage weTranslate
 */

class weTranslate_uninstall {
    /**
     * $db
     *
     * @var undefined
     */
    private $db;

    /**
     * __construct
     *
     * @param mixed $db
     * @return void
     */
    public function __construct( $db ){
        $this->db = $db;
        $this->check_db_tables();
    }

    /**
     * check_db_tables
     *
     * @return void
     */
    private function check_db_tables(){
        //TODO
        //Promp for delete translation database
    }

    /**
     * uninstall_db_tables
     *
     * @return void
     */
    private function uninstall_db_tables(){
        // Backup db, save file in plugin dir, and delete the db tables.
    }
}

new weTranslate_uninstall( $wpdb );
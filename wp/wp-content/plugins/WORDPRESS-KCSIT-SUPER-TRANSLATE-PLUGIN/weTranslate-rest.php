<?php

/**
 *
 * REST classes for weTranslate
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 1.1
 * @package Wordpress
 * @subpackage weTranslate
 */
class weTranslate_rest {
    /**
     * Stores the $wpdb engine
     * @var null
     */
    private static $db = null;

    /**
     * Stores the WP table prefix
     * @var null
     */
    private static $table_prefix = null;

    /**
     * Security key for the REST services.
     * @deprecated
     * @var string
     */
    private static $security_key = "ssDcvsdERG34WECsdjhbVKJHDFkjdfgSegERbfEGwefsadvfDFGggrFgvb";

    /**
     * Constructor for the class
     * @author Ricardo Goulart
     * @param $db
     * @param $table_prefix
     */
    public function __construct( $db = null, $table_prefix = null ){
        if( self::$db === null && !empty( $db ) ):
            self::$db = $db;
        endif;

        if( self::$table_prefix === null && !empty( $table_prefix ) ):
            self::$table_prefix = $table_prefix;
        endif;

        self::addActionsREST();
    }

    /**
     * Returns a security key when requested
     * @author Ricardo Goulart
     * @return string
     */
    public static function getSecurityKey(){
        return wp_create_nonce( self::$security_key . date( "Y-m-d" ) );
    }

    /**
     * Returns all the websites as an array
     * @author Ricardo Goulart
     * @return string (json encoded)|bool
     * @throws Exception
     */
    public static function getAllIdiomas(){ 
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        $qry = "SELECT option_value FROM " . self::$table_prefix . "options WHERE `option_name` = 'weTranslate_plugin_active_languages' LIMIT 1";
        $res = self::$db->get_row( $qry, ARRAY_A );

        if( !empty( $res ) ):
            echo ( $res['option_value'] );
        else:
            throw new Exception( "Could not find necessary key in the configuration table. :(" );
            return false;
        endif;

        die;
    }

    /**
     * Saves the configuration
     * @author Ricardo Goulart
     * @return bool
     * @throws Exception
     * @param st_wl_st_this_ws_idioma_prefix string
     * @param st_wl_st_this_ws_idioma_text string
     * @param st_wl_st_this_ws_idioma_URL string
     */
    public static function saveIdioma(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        if( !isset( $_POST['st_wl_st_this_ws_idioma_prefix'] ) || empty( $_POST['st_wl_st_this_ws_idioma_prefix'] ) || strlen( $_POST['st_wl_st_this_ws_idioma_prefix'] ) < 2 ):
            return false;
        endif;

        $qry = "SELECT option_value FROM " . self::$table_prefix . "options WHERE `option_name` = 'weTranslate_plugin_active_languages'";
        $res = self::$db->get_row( $qry, ARRAY_A ); 

        if( !empty( $res ) ):
            $myStack = (array) json_decode( $res['option_value'] );

            $myStack[$_POST['st_wl_st_this_ws_idioma_prefix']] = array(
                htmlentities( $_POST['st_wl_st_this_ws_idioma_text'] ),
                ( isset( $_POST['st_wl_st_this_ws_idioma_primary'] ) ? 'primary' : 'secondary' ),
                $_POST['st_wl_st_this_ws_idioma_URL']
            );

            $myStack = json_encode( $myStack );

            $qry = "UPDATE " . self::$table_prefix . "options SET `option_value` = '$myStack' WHERE `option_name` = 'weTranslate_plugin_active_languages' LIMIT 1";
            self::$db->query( $qry );
            die( "OK" );
        else:
            die( "Missing stuff" );
        endif;
    }

    /**
     * Removed the desired language code from the array stack.
     * @author Ricardo Goulart
     * @return bool
     * @throws Exception
     * @param langId string
     */
    public static function removeIdioma(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        if( isset( $_POST['langId'] ) ):
            $qry = "SELECT option_value FROM " . self::$table_prefix . "options WHERE `option_name` = 'weTranslate_plugin_active_languages' LIMIT 1";
            $res = self::$db->get_row( $qry, ARRAY_A );
            if( !empty( $res ) ):

                $res = json_decode( $res['option_value'] );
                $newRes = array();

                foreach( $res as $key => $val ):
                    if( $key !== $_POST['langId'] ):
                        $newRes[$key] = $val;
                    endif;
                endforeach;

                $newRes = json_encode( $newRes );

                //Save this to the DB now.
                $qry = "UPDATE " . self::$table_prefix . "options SET `option_value` = '$newRes' WHERE `option_name` = 'weTranslate_plugin_active_languages' LIMIT 1";
                self::$db->query( $qry );
                die( "OK" );
            else:
                throw new Exception( "No configuration found on this website, cannot delete what it is not there... :/" );
                die;
            endif;
        endif;
    }

    /**
     * Returns the data from the active languages stack stored in the db.
     * @author Ricardo Goulart
     * @return bool
     * @throws Exception
     * @param langId string
     */
    public static function getLanguageForEdit(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        if( isset( $_POST['langId'] ) ):
            $qry = "SELECT option_value FROM " . self::$table_prefix . "options WHERE `option_name` = 'weTranslate_plugin_active_languages'";
            $res = self::$db->get_row( $qry, ARRAY_A );
            if( !empty( $res ) ):
                $res = json_decode( $res['option_value'] );
                foreach( $res as $key => $val ):
                    if( $key == $_POST['langId'] ):
                        array_push( $val, $key );
                        $val[0] = urlencode( html_entity_decode( $val[0] ) );
                        echo( json_encode( $val ) );
                        die;
                    endif;
                endforeach;
            else:
                throw new Exception( "No configuration found on this website, cannot edit what it is not there... :/" );
                die;
            endif;
        endif;
    }

    /**
     * Checks the database for existing slugs matching the given parameter.
     * @author Ricardo Goulart
     * @return string "OK" or "KO" if false
     * @throws Exception
     * @param slug string
     */
    public static function checkSlugIsUnique(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        if( isset( $_GET['slug'] ) && !empty( $_GET['slug'] ) ) :
            $slug = $_GET['slug'];
            $qry = "SELECT COUNT(1) AS 'total' FROM " . self::$table_prefix . "weTranslate_languages WHERE `slug` = '$slug'";
            $res = self::$db->get_row( $qry, ARRAY_A );
            if( $res['total'] == 0 ):
                echo( "OK" );
            else:
                echo( "KO" );
            endif;
        else :
            throw new Exception( "You should not be here .. :(" );
            return false;
        endif;
        die;
    }

    /**
     * Initiates the REST functions and its routes.
     * @author Ricardo Goulart
     * @return void
     */
    public static function addActionsREST(){
        $myActions = array(
            'getAllIdiomas'         => 'getAllIdiomas',
            'saveIdioma'            => 'saveIdioma',
            'removeIdioma'          => 'removeIdioma',
            'getLanguageForEdit'    => 'getLanguageForEdit',
            'checkSlugIsUnique'     => 'checkSlugIsUnique',
            'saveTranslation'       => 'saveTranslation',
            'updateSyncOptions'     => 'updateSyncOptions',
            'st_rest_sync_service'  => 'st_rest_sync_service'
        );

        foreach( $myActions as $key => $val ):
            add_action( 'wp_ajax_' . $key, array( __CLASS__, $val ) );
        endforeach;
    }

    /**
     * Saves the translation to the database.
     * Deletes the duplicates on the process.
     * @author Ricardo Goulart
     * @return string "OK"
     * @throws Exception
     */
    public final static function saveTranslation(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        $st_old_slug        = (string) $_POST['oldSlug'];
        $st_new_slug        = (string) $_POST['st_unique_slug'];
        $st_arrayStack      = (array) $_POST['st_translation_for'];
        $st_will_be_visible = (int) ( ( $_POST['st_unique_active'] == 'on' ) ? 1 : 0 );

        if( !empty( $st_old_slug ) ):
            // UPDATE
            self::$db->query( "DELETE FROM " . self::$table_prefix . "weTranslate_languages WHERE slug = '$st_old_slug'" );
        endif;

        // Insert
        foreach( $st_arrayStack as $langId => $translationText ):
            $translationText = htmlentities( $translationText );
            $qry = "INSERT INTO " . self::$table_prefix . "weTranslate_languages ( slug, language, value, active ) VALUES ( '$st_new_slug', '$langId', '$translationText', '$st_will_be_visible' ) ";
            self::$db->query( $qry );
        endforeach;

        die( "OK" );
    }

    /**
     * Updates the sync options on the database
     * @author Ricardo Goulart
     * @return string "OK"
     * @throws Exception
     */
    public static function updateSyncOptions(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        $st_is_active = ( $_POST['isActive'] == "true" ? 1 : 0 );
        $st_main_url  = (string) $_POST['mainUrl'];

        $qry = "UPDATE " . self::$table_prefix . "options SET option_value = '$st_is_active' WHERE `option_name` = 'weTranslate_plugin_sync_active' LIMIT 1";
        self::$db->query( $qry );

        $qry = "UPDATE " . self::$table_prefix . "options SET option_value = '$st_main_url' WHERE `option_name` = 'weTranslate_plugin_sync_url' LIMIT 1";
        self::$db->query( $qry );

        die( "OK" );
    }

    /**
     * Returns the translated slug value for the given key and language
     * @author Ricardo Goulart
     * @param slug string (MD5)
     * @param lang string
     * @return string|0
     */
    public static function st_rest_sync_service(){
        $myStack = array();
        parse_str( $_SERVER[ 'QUERY_STRING' ], $myStack );
        $mySlug = (string) $myStack['slug'];
        $myLang = (string) md5( $myStack['lang'] );
        $sql = "SELECT value FROM " . self::$table_prefix . "weTranslate_languages WHERE MD5( slug ) = '$mySlug' AND MD5( language ) = '$myLang' AND active = 1";
        $res = self::$db->get_row( $sql, ARRAY_A );
        if( !empty( $res ) ):
            die( json_encode( $res ) );
        else:
            die(0);
        endif;
    }
}

new weTranslate_rest( $wpdb, $table_prefix );
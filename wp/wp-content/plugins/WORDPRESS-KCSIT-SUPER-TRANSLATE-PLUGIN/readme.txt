=== WETRANSLATE FOR WORDPRESS ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: http://goulart.pt
Tags: translate, localization, timwe, ricardo goulart
Requires at least: 4.9.1
Tested up to: 4.9.1
Stable tag: 4.9.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A Plugin to help developers code websites with multi-language using a DB with KEY => VAL structure and super easy implementation.

== Changelog ==

= 1.0 =
* Released to public FOR FR€€

= 0.2 =
* Migrated configurations from a dedicated table to the wordpress_options table
* Added more security to REST services
* Added a uninstall script
* Possible to download a backup of your translation records so you can take them always with you to other websites :)
* Added an import-backup function on the front end, so you dont have to go to the database like a boss.

= 0.1 =
* Initial version of the plugin. ( Internal use and working/dev copy ).

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

= 0.5 =
This version fixes a security related bug.  Upgrade immediately.
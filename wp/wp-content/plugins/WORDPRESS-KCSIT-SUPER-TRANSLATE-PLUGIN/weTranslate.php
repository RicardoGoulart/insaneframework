<?php
/*
    Plugin Name: WeTranslate
    Description: Ability to translate sentences and words, and get them afterwards. Syncs with all websites
    Version: 1.1
    Author: Ricardo Goulart <goulart.g@gmail.com>
    License: GPL2+
*/

error_reporting( E_ALL );
ini_set( "display_errors", 1 );

/**
 * @version number (Proudly) :D
 * @var version_record
 */
define( "VERSION_RECORD_weTRANSLATE", "1.1" );

/**
 * Install Hook
 * @author Ricardo Goulart
 */
function weTranslate_install(){
    global $wpdb, $table_prefix;
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranstate-install.php';
}
register_activation_hook( __FILE__, 'weTranslate_install' );

/**
 * Uninstall Hook
 * @author Ricardo Goulart
 */
function weTranslate_uninstall(){
    global $wpdb, $table_prefix;
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranslate-uninstall.php';
}
register_deactivation_hook( __FILE__, 'weTranslate_uninstall' );

/**
 * Bind more links to the administration menu.
 * @author Ricardo Goulart
 */
function weTranslate_menu_bind(){
    // Only show menu to administrators
    if( is_admin() && current_user_can( 'manage_options' ) ):
        add_menu_page( 'WeTranslate 4 WordPress', 'WeTranslate', 'manage_options', 'weTranslate-4-wordpress-welcome', 'weTranslate_init_controller', 'dashicons-editor-spellcheck', '25.5' );
        add_submenu_page( 'weTranslate-4-wordpress-welcome', 'Idiomas', 'Idiomas', 'manage_options', 'weTranslate-4-wordpress-options', 'weTranslate_init_controller' );
    endif;
}
add_action( 'admin_menu', 'weTranslate_menu_bind' );

/**
 * Ads a dashboard widget
 * @author Ricardo Goulart
 */
function weTranslate_dashboard_widget(){
    global $wpdb, $table_prefix;
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranslate-db-widget.php';
}
add_action( 'wp_dashboard_setup', 'weTranslate_dashboard_widget' );

/**
 * Initiates the core for the administration interface.
 * @author Ricardo Goulart
 */
function weTranslate_init_controller(){
    global $wpdb, $table_prefix;
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranslate-core.php';
}

/**
 * Returns the translation for the given string (key)..
 * @author Ricardo Goulart
 * @param string $slug
 * @return string
 */
function __st( $slug = "" ){
    if( empty( $slug ) ):
        return "No input variables for weTranslate";
    else:
        return weTranslate_reader::st_getLangBySlug( strtolower( $slug ) );
    endif;
}

/**
 * AutoLoads
 * @author Ricardo Goulart
 */
function weTranslate_plugin_autoload(){
    global $wpdb, $table_prefix;
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranslate-rest.php';
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranslate-reader.php';
}
spl_autoload_register( 'weTranslate_plugin_autoload' );
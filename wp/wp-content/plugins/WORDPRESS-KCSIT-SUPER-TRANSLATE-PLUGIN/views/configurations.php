<?php

/**
 *
 * View for weTranslate's options page
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 0.1
 * @package Wordpress
 * @subpackageweTranslate
 */

?>

<script type="text/javascript">

    var st_is_busy = false;

    jQuery( document ).ready(function() {
        st_updateTheLanguages();
    });

    var st_updateTheLanguages = function(){
        jQuery("#st_target_list_langs").slideUp( 500 );
        // This receives the languages and inserts them into the stage...
        jQuery("#st_target_list_langs").html("");

        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: { action : 'getAllIdiomas' },
            cache: false,
            dataType: "json",
            async: true,
            beforeSend: function(){
                st_is_busy = true;
            },
            error: function(){
                alert( "Verifique a sua ligação, o servidor não foi atingido." );
                st_is_busy = false;
                return false;
            },
            success: function( data ){
                st_is_busy = false;
            }
        }).done( function( data ) {
            jQuery.each( data, function( key, val ){
                var is_primary = false;

                if( val[1] == 'primary' )
                {
                    is_primary = '<i style="color: #777; font-size: 10px;">(Instala&ccedil;&atilde;o actual)</i>';
                }

                jQuery("#st_target_list_langs").append( '<div style="position: relative; float: left; width: 100%; clear: both;">' );
                jQuery("#st_target_list_langs").append( '<p class="regular-text">' );
                jQuery("#st_target_list_langs").append( '<input type="button" value="Eliminar" class="button button-small" onclick="javascript:st_removeLang(\''+key+'\');" /> ' );
                jQuery("#st_target_list_langs").append( '<input type="button" value="Editar" class="button button-primary button-small" onclick="javascript:st_editLang(\''+key+'\');" /> ' );
                jQuery("#st_target_list_langs").append( val[0] + ' ' + ( is_primary !== false ? is_primary : '' ) );
                jQuery("#st_target_list_langs").append( '</p></div>' );
            });

            jQuery("#st_target_list_langs").slideDown( 500 );
        });
    }

    var st_removeLang = function( key ){
        if( st_is_busy == true ){
            return;
        }

        if( confirm( "Está prestes a remover definitivamente esta linguagem.\nConfirma esta acção?" ) ){
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: { action : 'removeIdioma', langId : key },
                cache: false,
                dataType: "html",
                async: true,
                beforeSend: function(){
                    st_is_busy = true;
                },
                error: function(){
                    alert( "Verifique a sua ligação, o servidor não foi atingido." );
                    st_is_busy = false;
                    return false;
                },
                success: function( data ){
                    st_is_busy = false;
                }
            }).done( function( data ) {
                st_updateTheLanguages();
            });
        }
    };

    var st_editLang = function( key ){
        if( st_is_busy == true ){
            return;
        }

        if( !key ){
            jQuery("#st_new_edit_form")[0].reset();
            st_open_the_lang();
        }
        else {
            jQuery("#st_new_edit_form")[0].reset();

            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: { action : 'getLanguageForEdit', langId : key },
                cache: false,
                dataType: "json",
                async: true,
                beforeSend: function(){
                    st_is_busy = true;
                },
                error: function(){
                    alert( "Verifique a sua ligação, o servidor não foi atingido." );
                    st_is_busy = false;
                    return false;
                },
                success: function( data ){
                    st_is_busy = false;
                }
            }).done( function( data ) {
                st_open_the_lang();
                jQuery("#st_wl_st_this_ws_idioma_prefix").val( data[3] );
                jQuery("#st_wl_st_this_ws_idioma_text").val( decodeURIComponent( data[0].replace(/\+/g, ' ') ) );
                jQuery("#st_wl_st_this_ws_idioma_URL").val( data[2] );
                if( data[1] == 'primary' ) {
                    jQuery("#st_wl_st_this_ws_idioma_primary").prop('checked', true);
                }
            });
        }
    };

    var st_save_lang = function(){
        // Handles the ajax submit

        if( st_is_busy == true ){
            return;
        }

        var data = jQuery("#st_new_edit_form").serialize();
        var target = jQuery("#st_new_edit_form").attr( "action" );
        var method = jQuery("#st_new_edit_form").attr( "method" );

        jQuery.ajax({
            url: target,
            type: method,
            data: data,
            async: true,
            cache: false,
            dataType: "json",
            beforeSend: function(){
                // Add the loader.
                st_is_busy = true;
                jQuery("#st_edit_Lang").fadeTo( 500, 0.5 );
                jQuery(".st_submitBtn").prop('disabled', true);
            },
            error: function(){
                // Remove the loader.
                jQuery("#st_edit_Lang").fadeTo( 500, 1 );
                jQuery(".st_submitBtn").prop('disabled', false);
                st_is_busy = false;
            }
        }).done(function(data){
            // Remove the loader.
            jQuery("#st_edit_Lang").fadeOut( 500 );
            jQuery(".st_submitBtn").prop('disabled', false);
            st_is_busy = false;
            st_updateTheLanguages();
            st_open_the_lang(1);
            jQuery("#st_new_edit_form")[0].reset();
        });

        return false;
    };

    var st_open_the_lang = function( forceClose ){
        if( forceClose == 1 ){
            if( document.getElementById('st_edit_Lang').style.display != 'none' )
            {
                jQuery("#st_edit_Lang").slideToggle( 500 );
            }
        }else if( forceClose == 2 ){
            if( document.getElementById('st_edit_Lang').style.display == 'none' )
            {
                jQuery("#st_edit_Lang").slideToggle( 500 );
            }
        }else if( document.getElementById('st_edit_Lang').style.display == 'none' )
        {
            jQuery("#st_edit_Lang").slideToggle( 500);
        }
        else
        {
            jQuery("#st_edit_Lang").slideToggle( 500, function(){ jQuery("#st_edit_Lang").slideToggle( 500 ); } );
        }
    }
</script>

<div class="wrap">
    <h2>Opções</h2>
    <div class="wrap welcome-panel">
        <div style="position: relative; float: left; width: 100%;">

            <div class="welcome-panel-content">
                <div class="welcome-panel-column-container" style="padding-bottom: 20px;">
                    <div class="welcome-panel-column">
                        <h4><div class="dashicons dashicons-editor-spellcheck"></div> Idiomas disponíveis</h4>
                        <div id="st_target_list_langs"></div>
                        <input type="button" value="Adicionar outro" class="button button-primary button-hero" onclick="javascript:st_editLang();" />
                    </div>
                    <div id="st_edit_Lang" class="welcome-panel-column" style="display: none;">
                        <h4><div class="dashicons dashicons-welcome-write-blog"></div> Editar Idioma</h4>
                        <br />
                        <form name="st_new_edit_form" id="st_new_edit_form" action="/wp-admin/admin-ajax.php" method="POST" enctype="multipart/form-data" onsubmit="javascript: return st_save_lang();">
                            <input type="hidden" name="action" value="saveIdioma" />
                            <label for="st_wl_st_this_ws_idioma_prefix">
                                <div class="dashicons dashicons-translation"></div> Siglas do Idioma: <br />
                                <input type="text" name="st_wl_st_this_ws_idioma_prefix" value="" id="st_wl_st_this_ws_idioma_prefix" class="regular-text" placeholder="PT  (ex.)" />
                            </label>
                            <br /><br />
                            <label for="st_wl_st_this_ws_idioma_text">
                                <div class="dashicons dashicons-tag"></div> Idioma por extenso: <br />
                                <input type="text" name="st_wl_st_this_ws_idioma_text" value="" id="st_wl_st_this_ws_idioma_text" class="regular-text" placeholder="Português (ex.)" />
                            </label>
                            <br /><br />
                            <label for="st_wl_st_this_ws_idioma_URL">
                                <div class="dashicons dashicons-admin-links"></div> URL de acesso: <br />
                                <input type="text" name="st_wl_st_this_ws_idioma_URL" value="" id="st_wl_st_this_ws_idioma_URL" class="regular-text" placeholder="http://en.my-website.com/" />
                            </label>
                            <br /><br />
                            <label for="st_wl_st_this_ws_idioma_primary">
                                <input type="checkbox" name="st_wl_st_this_ws_idioma_primary" value="sim" id="st_wl_st_this_ws_idioma_primary" /> <div class="dashicons dashicons-networking"></div> Este idioma pertence a esta instalação <i>(Instala&ccedil;&atilde;o actual)</i>
                            </label>
                            <br />
                            <input type="submit" value="Guardar" class="regular-text button button-primary button-hero st_submitBtn" />
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
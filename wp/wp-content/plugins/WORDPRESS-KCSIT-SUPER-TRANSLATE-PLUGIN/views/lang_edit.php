<?php

/**
 *
 * View for weTranslate's Edit translation page
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 0.1
 * @package Wordpress
 * @subpackage weTranslate
 */

?>

<script type="text/javascript">

    jQuery( document ).ready(function() {
        // Do nothing
    });

    var is_acceptable_unique_slug = true;

    var st_submit_edited = function(){
        if( is_acceptable_unique_slug == false ){
            alert( "Por favor, insira um slug único!" );
            return false;
        } else {
            jQuery("#st_edit_form_language").fadeTo( 500, 0.5 );
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: jQuery("#st_edit_form_language").serialize(),
                cache: true,
                dataType: "html",
                async: true
            }).done( function( data ) {
                if( data == "OK" ) {
                    if( confirm( "Registo salvo com sucesso!\nDeseja inserir um novo?" ) ){
                        window.location.assign( "?page=weTranslate-4-wordpress-welcome&action=add" );
                        return false;
                    } else {
                        window.location.assign( "?page=weTranslate-4-wordpress-welcome" );
                        return false;
                    }
                } else {
                    alert( "Ocurreu um erro :/" );
                }
                jQuery("#st_edit_form_language").fadeTo( 500, 1 );
            });
        }
        return false;
    };

    var st_check_slug_is_unique = function( newVal, oldVal ){
        if( newVal == oldVal ){
            // Hide Everything
            jQuery("#st_slug_is_unique_y").hide();
            jQuery("#st_slug_is_unique_n").hide();
            return;
        } else {
            //Request to database
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'GET',
                data: { action : 'checkSlugIsUnique', slug : newVal },
                cache: true,
                dataType: "html",
                async: true
            }).done( function( data ) {
                if( data == "OK" ) {
                    jQuery("#st_slug_is_unique_n").hide();
                    jQuery("#st_slug_is_unique_y").show();
                    is_acceptable_unique_slug = true;
                } else {
                    jQuery("#st_slug_is_unique_n").show();
                    jQuery("#st_slug_is_unique_y").hide();
                    is_acceptable_unique_slug = false;
                }
            });
            return false;
        }
    };

</script>
<div class="wrap">
    <h2>Editar <a href="javascript: window.history.back(-1);" class="add-new-h2"><div style="line-height: 37px;" class="dashicons dashicons-arrow-left"></div> Voltar</a></h2>
    <div class="wrap welcome-panel" style="padding-bottom: 35px;">
        <form name="st_edit_form_language" id="st_edit_form_language" action="/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data" onsubmit="javascript: return st_submit_edited();">
            <?php
                $stack = self::getLanguageForEdit( $_GET['langId'] );
            ?>
            <input type="hidden" name="oldSlug" value="<?=$stack[0]['slug']?>" />
            <input type="hidden" name="action" value="saveTranslation" />
            <label for="st_unique_active">
                <div class="dashicons dashicons-visibility"></div> Activo? &nbsp;&nbsp;
            </label>
            <input id="st_unique_active" type="checkbox" <?=( $stack[0]['active'] == 1 ? 'checked="checked"' : '' )?> name="st_unique_active" />
            <br />
            <br />

            <label for="st_unique_slug">
                <div class="dashicons dashicons-admin-network"></div> Apontador &uacute;nico: (Sem caracteres especiais sff)
            </label>
            <br />
            <input class="regular-text" id="st_unique_slug" type="text" placeholder="<?=$stack[0]['slug']?>" onchange="javascript: st_check_slug_is_unique( this.value, '<?=$stack[0]['slug']?>' );" value="<?=$stack[0]['slug']?>" name="st_unique_slug" />
            <div id="st_slug_is_unique_y" style="display: none; color: green;" class="dashicons dashicons-yes"></div>
            <div id="st_slug_is_unique_n" style="display: none; color: red;" class="dashicons dashicons-no-alt"></div>
            <br />
            <br />
            <hr />
            <br />
            <?php
                foreach( self::getAllIdiomas() as $key => $val ):
                    $was_printed = false;
                    foreach( $stack as $k => $v ):
                        if( $key == $v['language'] ):
                            ?>
                            <label for="st_translation_for_<?=$key?>">
                                <div style="color: green;" class="dashicons dashicons-editor-spellcheck"></div> Tradu&ccedil;&atilde;o para: <strong><?=$val?></strong>
                            </label>
                            <br />
                            <input style="width: 98%;" class="regular-text" id="st_translation_for_<?=$key?>" type="text" placeholder="<?=html_entity_decode( $v['value'] )?>" value="<?=html_entity_decode( $v['value'] )?>" name="st_translation_for[<?=$key?>]" />
                            <br />
                            <br />
                            <?php
                            $was_printed = true;
                        endif;
                    endforeach;
                    if( $was_printed === false ):
                        ?>
                        <label for="st_translation_for_<?=$key?>">
                            <div style="color: red;" class="dashicons dashicons-edit"></div> Tradu&ccedil;&atilde;o para: <strong><?=$val?></strong>
                        </label>
                        <br />
                        <input style="width: 98%;" class="regular-text" id="st_translation_for_<?=$key?>" type="text" placeholder="Sem tradu&ccedil;&atilde;o !!" value="" name="st_translation_for[<?=$key?>]" />
                        <br />
                        <br />
                    <?php
                    endif;
                endforeach;
            ?>
            <div style="position: relative; float: left; width: 100%; text-align: center;">
                <input type="submit" value="Guardar dados" class="button button-primary button-hero" />
                <br />
                <input type="reset" value="Anular altera&ccedil;&otilde;es" class="button button-cancel" />
            </div>
        </form>
    </div>
</div>
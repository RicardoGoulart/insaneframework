<?php
/**
 *
 * Dashboard widget view for  weTranslate
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 0.1
 * @package Wordpress
 * @subpackage weTranslate
 */
?>


<div id="st_db_w_container"></div>
<div style="text-align: center;">
    <a href="/wp-admin/admin.php?page=weTranslate-4-wordpress-welcome" class="button button-primary button-hero" title="Rever tradu&ccedil;&otilde;es"><div style="line-height: 40px;" class="dashicons dashicons-pressthis"></div> Rever tradu&ccedil;&otilde;es</a>
</div>
<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
    jQuery(function () {
        jQuery('#st_db_w_container').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [''],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Quantidade',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
               enabled: false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                x: 0,
                y: -15,
                floating: true,
                borderWidth: 0,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
                shadow: false
            },
            credits: {
                enabled: false
            },
            <?php
            $my_st_Data = self::st_get_graph_metrics();
            if( !empty( $my_st_Data ) ):
            ?>
            series: [
                {
                    name: 'Inactivas',
                    data: [<?=$my_st_Data['inactive_records']?>],
                    color: "red"
                },
                {
                    name: 'Activas',
                    data: [<?=$my_st_Data['active_records']?>],
                    color: "green"
                },
                {
                    name: 'Traduções',
                    data: [<?=$my_st_Data['unique_records']?>],
                    color: "#000"
                }
            ]
            <?php
            else:
            ?>
            series: [
                {
                    name: 'Sem dados para amostra.',
                    data: [0],
                    color: "red"
                }
            ]
            <?php
            endif;
            ?>

        });
    });
</script>
<?php

/**
 *
 * View for weTranslate's welcome page
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 0.1
 * @package Wordpress
 * @subpackage weTranslate
 */

?>

<script type="text/javascript">

    var st_default_cookie_name_welcome = "st_welcome_displayed";

    var st_createCookie = function ( name, value, days ) {
        if( days )
        {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    };

    var st_readCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    };

    var st_eraseCookie = function (name) {
        st_createCookie( name, "", -1 );
    };

    var st_wconfig = function(){

        return false; //No page refresh...
    };

    var st_fade_welcome_panel = function( hide ){
        if( hide === 1 ){
            jQuery('#welcome-panel').slideToggle( 500 );
            st_createCookie( st_default_cookie_name_welcome, 1, 5 );
        }
        else
        {
            if( st_readCookie( st_default_cookie_name_welcome ) ){
                // Do nothing
            }
            else {
                jQuery('#welcome-panel').slideToggle( 500 );
            }
        }
    };

    jQuery( document ).ready(function() {
        st_fade_welcome_panel(0);
    });


</script>

<div class="wrap">
    <h2>weTranslate <a href="?page=<?=$_GET['page']?>&action=add" class="add-new-h2"><div style="line-height: 37px;" class="dashicons dashicons-edit"></div> Adicionar</a></h2>

    <div id="welcome-panel" class="welcome-panel" style="display: none;">
        <a class="welcome-panel-close" href="javascript:st_fade_welcome_panel(1);">Minimizar</a>
        <div class="welcome-panel-content">
            <h3>Obrigado!</h3>
            <p class="about-description">Muito obrigado por teres instalado este plugin.</p>
            <div class="welcome-panel-column-container" style="padding: 20px 0;">
                <div class="welcome-panel-column">
                    <h4>Início Rápido</h4>
                    <div class="dashicons dashicons-smiley" style="font-size: 128px;">&nbsp;</div>
                </div>
                <div class="welcome-panel-column">
                    <h4><div class="dashicons dashicons-megaphone"></div> Feedback</h4>
                    <div style="position: relative; float: left; width: 94%; padding: 0 3%;">
                        <p style="position: relative; float: left; clear: both;" class="description">
                            "Tenho liberdade para gerir as línguas e as traduções. É tudo o que eu quero..."
                            <br />
                            - Ricardo Goulart
                        </p>
                        <hr style="width: 70%; text-align: left; float: left; position: relative; clear: both;" />

                        <p style="position: relative; float: left; clear: both;" class="description">
                            "Depois de fazer uma vez, posso linkar todos os meus websites com este, e buscar as traduções quando quiser, brutal!"
                            <br />
                            - Anonymous
                        </p>
                        <a style="position: relative; float: left; text-align: center;" href="mailto:goulart.g@gmail.com&subject=<?=urlencode("(weTRANSLATE :: FEEDBACK) Adorei o plugin, e gostaria de contribuir construtivamente para o seu desenvolvimento.")?>" class="button button-secondary">Deixa o teu! <div style="line-height: 25px;" class="dashicons dashicons-yes"></div></a>
                    </div>
                </div>
                <div class="welcome-panel-column">
                    <h4><div class="dashicons dashicons-editor-help"></div> Dúvidas?</h4>
                    <div style="position: relative; float: left; width: 94%; padding: 0 3%;">
                        <p>Contacte-me para esclarecimentos, terei o maior prazer.</p>
                        <a href="mailto:goulart.g@gmail.com&subject=<?=urlencode("(weTRANSLATE :: SUPPORT) Preciso que me esclareças aqui uma dúvida sff..")?>" class="button button-primary button-hero"><div style="line-height: 45px;" class="dashicons dashicons-awards"></div> Fala comigo!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table class="wp-list-table widefat fixed pages" style="margin-top: 10px;">
        <thead>
        <tr>
            <th scope="col" id="id" class="manage-column column-cb check-column">
                <span>ID</span>
            </th>
            <th scope="col" id="slug" class="manage-column column-slug">
                <span>Slug</span>
            </th>
            <th scope="col" id="texto" class="manage-column column-title">
                <span>Texto</span>
            </th>
            <th scope="col" id="date" class="manage-column column-date">
                <span>Data</span>
            </th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="col" id="f_id" class="manage-column column-cb check-column">
                <span>ID</span>
            </th>
            <th scope="col" id="f_slug" class="manage-column column-slug">
                <span>Slug</span>
            </th>
            <th scope="col" id="f_texto" class="manage-column column-title">
                <span>Texto</span>
            </th>
            <th scope="col" id="f_date" class="manage-column column-date">
                <span>Data</span>
            </th>
        </tr>
        </tfoot>

        <tbody>
        <?php
        $st_get_LangsList = self::getLanguagesForList();
        if( !empty( $st_get_LangsList ) ):
            foreach( $st_get_LangsList as $key => $val ):
                $st_edit_link   = "?page=".$_GET['page']."&action=edit&langId=" . ( $val['id'] );
                $st_delete_link = "javascript: if( confirm('Deseja eliminar este item?') ){ window.location.assign( '?page=".$_GET['page']."&action=delete&langId=" . ( $val['id'] ) . "' ); }";
                ?>
                <tr id="st_lang_id_<?=$key?>" class="post_<?=$key?> type-page status-publish hentry alternate iedit author-self level-0">
                    <th scope="row" class="check-column">
                        <label class="screen-reader-text" for="cb-select-2">
                            Seleccionar o ID <?=$key?>
                        </label>
                        <input id="cb-select-<?=$key?>" type="checkbox" name="post[]" value="<?=$val['id']?>" />
                        <div class="locked-indicator"></div>
                    </th>
                    <td class="post-title page-title column-title">
                        <strong>
                            <a class="row-title" href="<?=$st_edit_link?>" title="Editar <?=$val['slug']?>">
                                <?=$val['slug']?>
                            </a>
                        </strong>

                        <div class="locked-info">
                            <span class="locked-avatar"></span> <span class="locked-text"></span>
                        </div>

                        <div class="row-actions">
                            <span class="edit">
                                <a href="<?=$st_edit_link?>" title="Editar este item">Editar</a> |
                            </span>

                            <!-- TODO --
                            <span class="inline hide-if-no-js">
                                <a href="#" class="editinline" title="Editar este item em linha">Edição&nbsp;Rápida</a> |
                            </span>
                            /-->

                            <span class="trash">
                                <a class="submitdelete" title="Mover este item para o lixo" href="<?=$st_delete_link?>">Eliminar</a> |
                            </span>

                            <!-- TODO --
                            <span class="view">
                                <a href="#" title="Visualizar" rel="permalink">Ver</a>
                            </span>
                            /-->
                        </div>

                        <div class="hidden" id="inline_<?=$key?>">
                            <div class="post_title"><?=$val['slug']?></div>
                            <div class="post_name"><?=$val['value']?></div>
                            <div class="jj"><?=date( "d", strtotime( $val['updatedt'] ) )?></div>
                            <div class="mm"><?=date( "m", strtotime( $val['updatedt'] ) )?></div>
                            <div class="aa"><?=date( "Y", strtotime( $val['updatedt'] ) )?></div>
                            <div class="hh"><?=date( "h", strtotime( $val['updatedt'] ) )?></div>
                            <div class="mn"><?=date( "i", strtotime( $val['updatedt'] ) )?></div>
                            <div class="ss"><?=date( "s", strtotime( $val['updatedt'] ) )?></div>
                            <div class="post_password"></div>
                            <div class="post_parent">0</div>
                            <div class="page_template">default</div>
                            <div class="menu_order">0</div>
                        </div>
                    </td>

                    <td class="author column-author">
                        <a href="<?=$st_edit_link?>"><?=$val['value']?></a>
                    </td>

                    <td class="date column-date">
                        <abbr title="<?=date( "Y/m/d H:i:s", strtotime( $val['updatedt'] ) )?>">
                            <?=date( "Y/m/d", strtotime( $val['updatedt'] ) )?>
                        </abbr>
                        <br />
                        <?=( $val['active'] == 1 ? 'Publicado' : 'Rascunho' );?>
                    </td>
                </tr>
            <?php
            endforeach;
        else:
            ?>
            <tr id="st_lang_id_<?=$key?>" class="post_<?=$key?> type-page status-publish hentry alternate iedit author-self level-0">
            <th colspan="4" scope="row" class="check-column" align="center">
                <span style="text-align: center; position: relative; float: left; width: 100%; padding-bottom: 10px;">Sem tradu&ccedil;&otilde;es para amostra. Crie um <a href="?page=<?=$_GET['page']?>&action=add">agora!</a></span>
            </th>
            </tr>
            <?php
        endif;
        ?>
        </tbody>
    </table>
</div>
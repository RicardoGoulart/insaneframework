<?php

/**
 *
 * View for weTranslate's multi-site page
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 0.1
 * @package Wordpress
 * @subpackage weTranslate
 */

$current_sync_url    = self::getCurrentSyncUrl();
$current_sync_status = self::getCurrentSyncStatus();
$all_my_websites     = self::getAllMyWebsitesSync();
if( empty( $all_my_websites ) ):
    ?>
    <div class="wrap">
        <h2><div class="dashicons dashicons-admin-site" style="line-height: 30px;"></div> Multi Site <a href="javascript: window.history.back(-1);" class="add-new-h2"><div style="line-height: 37px;" class="dashicons dashicons-arrow-left"></div> Voltar</a></h2>
        <div class="wrap welcome-panel" style="padding-bottom: 10px;">
            <p class="description">Para ter acesso a esta "feature", necessita de ter no m&iacute;nimo 2 URL's definidos no menu de "Idiomas"</p>
        </div>
    </div>
    <?php
else:
    // All ok
    $all_my_websites = json_decode( $all_my_websites );
    ?>
    <script type="text/javascript">

        var st_save_the_sync = function (){
            jQuery("#st_loader").fadeTo( 500, 0.5 );
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: { action : 'updateSyncOptions', isActive : jQuery("#st_activate_sync").is(':checked'), mainUrl : jQuery("#st_main_sync_url").val() },
                cache: false,
                dataType: "html",
                async: true
            }).done( function( data ) {
                if( data == "OK" ) {
                    alert( "Informação actualizada com sucesso!" );
                } else {
                    alert( "Ocurreu um erro ao gravar. :/" );
                }
                jQuery("#st_loader").fadeTo( 500, 1 );
            });
        };

    </script>


    <div class="wrap">
        <h2><div class="dashicons dashicons-admin-site" style="line-height: 30px;"></div> Multi Site <a href="javascript: window.history.back(-1);" class="add-new-h2"><div style="line-height: 37px;" class="dashicons dashicons-arrow-left"></div> Voltar</a></h2>
        <div class="wrap welcome-panel" style="padding-bottom: 10px;" id="st_loader">
            <label for="st_activate_sync">
                <input type="checkbox" value="yes" <?=( $current_sync_status == 1 ? 'checked="checked"' : '' )?> name="st_activate_sync" id="st_activate_sync" /> Quero que os meus websites comuniquem entre s&iacute;.
            </label>
            <br />
            <hr />
            <label for="st_main_sync_url">
                <div class="dashicons dashicons-dashboard"></div> URL de sincronismo
                <br />
                <select name="st_main_sync_url" id="st_main_sync_url" class="regular-text">
                    <?php
                    foreach( $all_my_websites as $key => $val ):
                        ?>
                        <option title="<?=$val[0]?>" <?=( $val[2] == $current_sync_url ? 'selected="selected"' : '' )?> value="<?=$val[2]?>"><?=$val[0]?></option>
                        <?php
                    endforeach;
                    ?>
                </select>
            </label>
            <br />
            <br />
            <input type="button" onclick="javascript:st_save_the_sync();" value="Guardar" class="button button-primary" />
            <p class="description">Ao activar o sync, est&aacute; a permitir que todos acedam ao seu website para buscar informa&ccedil;&otilde;es de tradu&ccedil;&atilde;o em falta, assim como o inverso.<br /><strong>Utilize com precau&ccedil;&atilde;o!</strong></p>
        </div>
    </div>
    <?php
endif;
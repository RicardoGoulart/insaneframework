<?php

/**
 *
 * Reader classes for weTranslate
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 1.1
 * @package Wordpress
 * @subpackage weTranslate
 */
class weTranslate_reader {

    /**
     * @var $db ==> $wpdb
     */
    private static $db              = null;

    /**
     * @var $table_prefix ==> $table_prefix
     */
    private static $table_prefix    = null;

    /**
     * Stores the language code for this website (Eg.: PT, EN, FR)
     * @var string
     */
    private static $langCode        = null;

    /**
     * Stores the translations to prevent multiple requests to the database for the same thing.
     * @var array|null
     */
    private static $internalStack   = null;

    /**
     * Stores the Sync URL if any.
     * @var string|null
     */
    private static $syncUrl         = null;

    /**
     * Constructor for this class
     * @author Ricardo Goulart
     * @param $db ==> $wpdb
     * @param $table_prefix ==> $table_prefix
     */
    public function __construct( $db = null, $table_prefix = null ){
        if( self::$db === null && !empty( $db ) ):
            self::$db = $db;
        endif;

        if( self::$table_prefix === null && !empty( $table_prefix ) ):
            self::$table_prefix = $table_prefix;
        endif;

        if( self::$langCode === null ):
            self::getLangCodeOptions();
        endif;

        if( self::$internalStack === null ):
            self::$internalStack = array();
        endif;
    }

    /**
     * Queries the database for the translation to the requested slug.
     * If it does not exists, checks for sync options and tries to get it anyway
     * @author Ricardo Goulart
     * @param string $slug
     * @return string
     */
    public static function st_getLangBySlug( $slug = "" ){
        $slugtastic = md5($slug);
        if( array_key_exists( $slugtastic, self::$internalStack ) ):
            return self::$internalStack[$slugtastic];
        else:
            // The queries are MD5'ed because I know how to mySql inject if they aren't... Safety first, performance second...
            $sql = "SELECT value FROM " . self::$table_prefix . "weTranslate_languages WHERE MD5( slug ) = '$slugtastic' AND MD5( language ) = '".md5( self::$langCode )."' AND active = 1 LIMIT 1";
            $res = self::$db->get_row( $sql, ARRAY_A );
            if( !empty( $res ) ) :
                // Add to the internal stack
                self::$internalStack[$slugtastic] = $res['value'];
                return $res['value'];
            else:
                // The key does not exists.
                if( self::st_checkSyncActive() ) :
                    return self::st_returnSyncText( $slugtastic );
                else:
                    $loled_text = "This key does not exist in the DB :(";
                    self::$internalStack[$slugtastic] = $loled_text;
                    return $loled_text;
                endif;
            endif;
        endif;
    }

    /**
     * Returns the sync state
     * @author Ricardo Goulart
     * @return bool
     */
    public static function st_checkSyncActive(){
        $qry = "SELECT option_value FROM ".self::$table_prefix."options WHERE `option_name` = 'weTranslate_plugin_sync_active'";
        $res = self::$db->get_row( $qry, ARRAY_A );
        if( !empty( $res ) ) :
            if( intval( $res['option_value'] ) === 1 ):
                return true;
            else:
                return false;
            endif;
        else:
            return false;
        endif;
    }

    /**
     * Requests to the other server the translation needed for the text.
     * @author Ricardo Goulart
     * @param $md5ed_slug string (MD5)
     * @return string
     */
    public static function st_returnSyncText( $md5ed_slug = "" ){
        if( self::$syncUrl === null ):
            $qry = "SELECT option_value FROM ". self::$table_prefix ."options WHERE `option_name` = 'weTranslate_plugin_sync_url'";
            $res = self::$db->get_row( $qry, ARRAY_A );
            if( !empty( $res ) ) :
                self::$syncUrl = rtrim( $res['option_value'] , "/" );
            else:
                throw new Exception( "No sync url defined ...." );
                return false;
            endif;
        endif;

        $url  = self::$syncUrl . "/wp-admin/admin-ajax.php?action=st_rest_sync_service&slug=$md5ed_slug&lang=".self::$langCode;

        $curl = curl_init();

        curl_setopt( $curl, CURLOPT_URL, $url );
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array(
            'Access-Control-Allow-Origin: *',
            'Connection: Close'
        ));
        curl_setopt( $curl, CURLOPT_FAILONERROR, false );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $curl, CURLOPT_USERAGENT, 'weTranslate for Wordpress' );
        curl_setopt( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt( $curl, CURLOPT_TIMEOUT, 10 );
        curl_setopt( $curl, CURLOPT_COOKIESESSION, 1 );
        curl_setopt( $curl, CURLOPT_AUTOREFERER, 1 );
        curl_setopt( $curl, CURLOPT_HEADER, 1 );
        curl_setopt( $curl, CURLINFO_HEADER_OUT, 1 );
        curl_setopt( $curl, CURLOPT_HTTPGET, true );
        curl_setopt( $curl, CURLOPT_POST, false );
        curl_setopt( $curl, CURLOPT_CERTINFO, true );
        curl_setopt( $curl, CURLOPT_VERBOSE, true );
        curl_setopt( $curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY );

        $result = curl_exec( $curl );

        curl_close( $curl );

        if( $result ):
            $result = json_decode( $result );
            self::$internalStack[$md5ed_slug] = $result['value'];
        else:
            $result = "Key does not exist, not even in sync..";
            self::$internalStack[$md5ed_slug] = $result;
        endif;

        return self::$internalStack[$md5ed_slug];
    }

    /**
     * Stores the default language for this website on a private static variable within this class
     * @author Ricardo Goulart
     * @return bool
     */
    public static function getLangCodeOptions(){
        $sql = "SELECT option_value FROM " . self::$table_prefix . "options WHERE `option_name` = 'weTranslate_plugin_active_languages' LIMIT 1";
        $res = self::$db->get_row( $sql, ARRAY_A );
        if( !empty( $res ) ) :
            $res = (array) json_decode( $res['option_value'] );
            foreach( $res as $key => $val ):
                if( $val[1] == 'primary' ):
                    self::$langCode = $key;
                    return true;
                endif;
            endforeach;
            if( self::$langCode === null ):
                die( "You have a corrupted database, plz uninstall the weTRANSLATE plugin, and re-install it again." );
            endif;
        else:
            die( "You have no default language for your website. Plz proceed to the administration panel to configure the stuff...." );
        endif;
    }
}

new weTranslate_reader( $wpdb, $table_prefix );
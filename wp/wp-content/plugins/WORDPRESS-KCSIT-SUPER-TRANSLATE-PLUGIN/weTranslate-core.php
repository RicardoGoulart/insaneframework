<?php

/**
 *
 * REST classes for weTranslate
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 1.1
 * @package Wordpress
 * @subpackage weTranslate
 *
 * TODO: Only throw exceptions when debug mode is enabled.
 */
class weTranslate_core {
    /**
     * Stores the $wpdb
     * @var $db null
     */
    private static $db = null;

    /**
     * Stores the WP table prefix
     * @var null
     */
    private static $table_prefix = null;

    /**
     * Stores my routes
     * @var array
     */
    private static $myRoutes = array();

    /**
     * Stores the instance
     * @var mixed
     */
    private static $instance_st;

    /**
     * Constructor for this class
     * @author Ricardo Goulart
     * @param $db
     * @param $table_prefix
     */
    public function __construct( $db = null, $table_prefix = null ){
        if( self::$db === null && !empty( $db ) ):
            self::$db = $db;
        endif;

        if( self::$table_prefix === null && !empty( $table_prefix ) ):
            self::$table_prefix = $table_prefix;
        endif;

        self::start_routing();
    }

    /**
     * Returns the languages grouped by slug to the list which appears on the admin panel for the weTranslate plugin.
     * @author Ricardo Goulart
     * @return string|bool
     */
    public static function getLanguagesForList(){
        $qry = "SELECT * FROM " . self::$table_prefix . "weTranslate_languages GROUP BY slug ORDER BY active DESC, id DESC ";
        $res = self::$db->get_results( $qry, ARRAY_A );
        if( !empty( $res ) ):
            return $res;
        else:
            //throw new Exception( "Result set from database returned empty, maby you should check some tables huh???" );
            return false;
        endif;
    }

    /**
     * Returns all languages for this SLUG for edit purposes
     * @author Ricardo Goulart
     * @param int $slug
     * @return json_encoded( $string ) | bool
     * @throws Exception
     */
    public static function getLanguageForEdit( $slug = 0 ){
        if( !empty( $slug ) ):
            $slug = intval( $slug );
            $qry = "SELECT slug FROM " . self::$table_prefix . "weTranslate_languages WHERE id = '$slug'";
            $res = self::$db->get_row( $qry, ARRAY_A );
            if( !empty( $res ) ):
                $slug = $res['slug'];
                $qry = "SELECT * FROM " . self::$table_prefix . "weTranslate_languages WHERE slug = '$slug' ORDER BY language ASC";
                $res = self::$db->get_results( $qry, ARRAY_A );
                return $res;
            else:
                throw new Exception( "Something when wrong :( Contact support plz..." );
                return false;
            endif;
        endif;
    }

    /**
     * Returns all active languages from the configuration table.
     * @author Ricardo Goulart
     * @return array|bool
     * @throws Exception
     */
    public static function getAllIdiomas(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        $qry = "SELECT option_value FROM " . self::$table_prefix . "options WHERE `option_name` = 'weTranslate_plugin_active_languages' LIMIT 1";
        $res = self::$db->get_row( $qry, ARRAY_A );

        if( !empty( $res ) ):
            $stack = (array) json_decode( $res['option_value'] );
            $toRet = array();
            foreach( $stack as $key => $val ):
                $toRet[$key] = $val[0];
            endforeach;
            return $toRet;
        else:
            throw new Exception( "Could not find necessary key in the configuration table. :(" );
            return false;
        endif;
    }

    /**
     * Handles the routes for the admin panel administration
     * @author Ricardo Goulart
     * @throws Exception
     */
    public static function start_routing(){
        if( !empty( $_SERVER[ 'QUERY_STRING' ] ) ):
            parse_str( $_SERVER[ 'QUERY_STRING' ], self::$myRoutes );
        endif;

        if( isset( self::$myRoutes['page'] ) ):
            switch( self::$myRoutes['page'] ):
                default:
                    throw new Exception( "weTranslate cannot follow these instructions.." );
                    break;
                case 'weTranslate-4-wordpress-options':
                    require_once dirname( __FILE__ ) . '/views/configurations.php';
                    break;
                case 'weTranslate-4-wordpress-welcome':
                    if( empty( self::$myRoutes['action'] ) ):
                        self::$myRoutes['action'] = null;
                    endif;
                    switch( self::$myRoutes['action'] ) :
                        case 'edit':
                            require_once dirname( __FILE__ ) . '/views/lang_edit.php';
                            break;
                        case 'add':
                            require_once dirname( __FILE__ ) . '/views/lang_add.php';
                            break;
                        case 'delete':
                            self::st_deleteBySlug();
                        default:
                            require_once dirname( __FILE__ ) . '/views/welcome.php';
                            break;
                    endswitch;
                    break;
                case 'weTranslate-4-wordpress-multi-site':
                    require_once dirname( __FILE__ ) . '/views/multi-site.php';
                    break;
            endswitch;
        else:
            // No params where given, display "home"
            require_once dirname( __FILE__ ) . '/views/welcome.php';
        endif;
    }

    /**
     * Deletes all translations with the same slug as the given key (ID)
     * @author Ricardo Goulart
     * @return bool
     * @param int $_GET['langId']
     * @throws Exception
     */
    public static function st_deleteBySlug(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        $slug = intval( $_GET['langId'] );

        if( !empty( $slug ) ):
            $sql = "SELECT slug FROM " . self::$table_prefix . "weTranslate_languages WHERE id = $slug";
            $res = self::$db->get_row( $sql, ARRAY_A );
            if( !empty( $res ) ):
                // DELETE
                $slug = $res['slug'];
                $sql = "DELETE FROM " . self::$table_prefix . "weTranslate_languages WHERE slug = '$slug'";
                self::$db->query( $sql );
                // FIXME: Move the delete to the REST service ???
            else:
                // ERROR
                throw new Exception( "Some parameters are not matching properly. Plz contact support or read the non existing documentation! xD" );
                return false;
            endif;
        endif;
    }

    /**
     * Query's the database and returns all content stored in the active_languages
     * @author Ricardo Goulart
     * @return string_json_encoded|bool
     * @throws Exception
     */
    public static function getAllMyWebsitesSync(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        $qry = "SELECT option_value FROM ". self::$table_prefix ."options WHERE `option_name` = 'weTranslate_plugin_active_languages' LIMIT 1";
        $res = self::$db->get_row( $qry, ARRAY_A );
        if( !empty( $res  ) ) :
            return $res['option_value'];
        else:
            return false;
        endif;
    }

    /**
     * Query's the database for the value stored in the "sync_active" field
     * @author Ricardo Goulart
     * @return bool
     * @throws Exception
     */
    public static function getCurrentSyncStatus(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        //Returns an url which can be compared after..
        $qry = "SELECT option_value FROM ". self::$table_prefix ."options WHERE `option_name` = 'weTranslate_plugin_sync_active' LIMIT 1";
        $res = self::$db->get_row( $qry, ARRAY_A );
        if( !empty( $res  ) ) :
            return $res['option_value'];
        else:
            return false;
        endif;
    }

    /**
     * Query's the database for the string stored in the "sync_url" field
     * @author Ricardo Goulart
     * @return bool
     * @throws Exception
     */
    public static function getCurrentSyncUrl(){
        if( !is_admin() || !current_user_can( 'manage_options' ) ):
            throw new Exception( "Only admins can do this..." );
            return false;
        endif;

        //Returns an url which can be compared after..
        $qry = "SELECT option_value FROM ". self::$table_prefix ."weTranslate_config WHERE `option_name` = 'weTranslate_plugin_sync_url' LIMIT 1";
        $res = self::$db->get_row( $qry, ARRAY_A );
        if( !empty( $res  ) ) :
            return $res['option_value'];
        else:
            return false;
        endif;
    }
}

if( !class_exists( "weTranslate_rest" ) ):
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranslate-rest.php';
    require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'weTranslate-reader.php';
endif;

new weTranslate_core( $wpdb, $table_prefix );
<?php

/**
 *
 * Install weTranstate
 *
 * @author Ricardo Goulart <goulart.g@gmail.com>
 * @since 0.1
 * @version 1.1
 * @package Wordpress
 * @subpackage weTranstate
 */
class weTranstate_install {
    /**
     * $db
     *
     * @var null
     */
    private static $db = null;
    /**
     * $table_prefix
     *
     * @var null
     */
    private static $table_prefix = null;
    /**
     * $version_record
     *
     * @var null
     */
    private static $version_record = null;

    /**
     * __construct
     *
     * @param mixed $db
     * @param mixed $table_prefix
     * @param mixed $version_record
     * @return void
     */
    public function __construct( $db = null, $table_prefix = null, $version_record = null ){
        if( !empty( $db ) ):
            self::$db = $db;
        else:
            throw new Exception( "Plugin needs database connection and none was provided." );
        endif;

        if( !empty( $table_prefix ) ):
            self::$table_prefix = $table_prefix;
        else:
            throw new Exception( "Plugin needs the table prefix string and none was provided." );
        endif;

        if( !empty( $version_record ) ):
            self::$version_record = $version_record;
        else:
            throw new Exception( "Plugin needs the plugin version and none was provided." );
        endif;
        
        self::check_db_tables();
    }

    /**
     * check_db_tables
     *
     * @return void
     */
    private static function check_db_tables(){
        $theTablesExist = self::$db->get_results( "SELECT * FROM " . self::$table_prefix . "options WHERE option_name = 'weTranslate_plugin_version'", ARRAY_A );
        if( empty( $theTablesExist ) ):
            self::install_db_tables();
        else:
            //TODO : Maby backup the entire thing and prepare the stuff for migration and what not. ?
            self::doNothing();
        endif;
    }

    /**
     * install_db_tables
     *
     * @return void
     */
    private static function install_db_tables(){
        $sql = "INSERT INTO " . self::$table_prefix . "options (`option_name`, `option_value`) VALUES( 'weTranslate_plugin_version', '".self::$version_record."' )";
        self::$db->query( $sql );

        $sql = "INSERT INTO " . self::$table_prefix . "options (`option_name`, `option_value`) VALUES( 'weTranslate_plugin_sync_url', '".$_SERVER['HTTP_HOST']."' )";
        self::$db->query( $sql );

        $sql = "INSERT INTO " . self::$table_prefix . "options (`option_name`, `option_value`) VALUES( 'weTranslate_plugin_sync_active', '0' )";
        self::$db->query( $sql );

        $sql = "INSERT INTO " . self::$table_prefix . "options (`option_name`, `option_value`) VALUES( 'weTranslate_plugin_active_languages', '".json_encode( array( 'PT' => array( 'Portugu&ecirc;s', 'primary', $_SERVER['HTTP_HOST'] ) ) )."' )";
        self::$db->query( $sql );

        // TODO Create the language table.
        $create_st_languages_schema = "CREATE TABLE `".self::$table_prefix."weTranslate_languages` (
            `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `slug` VARCHAR(128) NULL DEFAULT NULL,
            `language` VARCHAR(5) NULL DEFAULT NULL,
            `value` VARCHAR(512) NULL DEFAULT NULL,
            `active` TINYINT(2) UNSIGNED NULL DEFAULT '1',
            `updatedt` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            INDEX `id` (`id`),
            INDEX `language` (`language`),
            INDEX `slug` (`slug`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        AUTO_INCREMENT=1;";
        self::$db->query( $create_st_languages_schema );

        $sql = "INSERT INTO " . self::$table_prefix . "weTranslate_languages (`slug`, `language`, `value`) VALUES( 'welcome', 'PT', 'Bem Vindo!!!!!' )";
        self::$db->query( $sql );
        $sql = "INSERT INTO " . self::$table_prefix . "weTranslate_languages (`slug`, `language`, `value`) VALUES( 'welcome', 'EN', 'Welcome!!!!!' )";
        self::$db->query( $sql );
    }

    /**
     * doNothing
     *
     * @return void
     */
    private static function doNothing(){
        return true;
    }
}

new weTranstate_install( $wpdb, $table_prefix, VERSION_RECORD_weTRANSLATE );
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'insane_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Cx_Q}/`8x4A>2/bl<a3l|`8V0#kk PWQO u#v$x?6kY7{)Qppb3W[,y~G5@BR~o*');
define('SECURE_AUTH_KEY',  'Ik#h_p>K9Akkd%|I9jzKUJ^$A7N7a~0GlY0rCi%M^[yqR{A;HXwvry(jok3,!dvR');
define('LOGGED_IN_KEY',    'o%>[Xrk[V7>mCYg{FlcxUqYl+zi$%aP]1 &AVL(JD[aL-D5v+uw09h#Lj}U9=Q5J');
define('NONCE_KEY',        '04c+]B^~6F`+Ax83SU?n{z>H7j$+UJM$s@yJj/~I|SbzxS*pFcM,,9xW`@A#F]N%');
define('AUTH_SALT',        'Jph%O&88ElL{0X8olwrqlAp=[1coUXm=G-yL<Z;~Wjw|_+re>Jn*?g]vEr#?-cC*');
define('SECURE_AUTH_SALT', '68WcMj0*$Xl.UTmaZ:EJN}y:4@evNlK/!4ka^sPVf J9$ge%&0j=GhTnX;~})42!');
define('LOGGED_IN_SALT',   '-1BW[8;eE{2Z5/6 ]MdCmB,XE:N3 #=AJdx:;M~$65.`iO%-IRW10|qUW##<$) e');
define('NONCE_SALT',       '+#oy%iKxfw`pwtR!*uzfJCYtQ@sh41tM$TI3?aE[5h,/LZF{M2<KJI3wn7sWkK9A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'insane_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

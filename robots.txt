Sitemap: insane.fw/sitemap.xml
User-Agent: *
Disallow: /core/
Disallow: /logs/
Disallow: /routes/
Disallow: /cache/
Disallow: /assets/

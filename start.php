<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-05 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

insaneLoader::registerDependency( array( 
  "insaneConfig", 
  "insaneLogger", 
  "insaneCache", 
  "insaneRouter", 
  "insaneRobots", 
  "insaneDatabase",
  "insaneWordpress"
 ), __FILE__ );

insaneWordpress::registerPath( __DIR__ . DIRECTORY_SEPARATOR . 'wp' . DIRECTORY_SEPARATOR . 'wp-load.php' );

require_once __DIR__ . DIRECTORY_SEPARATOR . 'languages.php';

insaneHeader::registerPath( __DIR__ . DIRECTORY_SEPARATOR . 'assets' );
insaneFooter::registerPath( __DIR__ . DIRECTORY_SEPARATOR . 'assets' );

// Register some routes
require_once __DIR__ . DIRECTORY_SEPARATOR . 'routers.php';

if( !insaneRouter::filter() ):
  // Search in wordpress in case exists...
  if( class_exists( "insaneWordpress" ) ):
    if( insaneWordpress::routeExists() ):
      insaneRouter::register( insaneWordpress::$route, "wordpress.router.inc.php", 50 );
      if( !insaneRouter::filter() ):
        insaneRouter::show404();
      endif;
    else:
      // Nao existe no wordpress
      insaneRouter::show404();
    endif;
  else:
    // Show 404 at this point
    insaneRouter::show404();
    // It dies after this...
  endif;
endif;

// Set up cache path and Time.
insaneCache::configure( __DIR__, insaneRouter::$route['cache'] );

if( !insaneCache::showCacheFile() ):
  // Save the cache with the contents
  insaneRouter::parseRoute( __DIR__ . DIRECTORY_SEPARATOR . 'routes' );
  if( !insaneCache::saveCacheFile( insaneRouter::getContents() ) ):
    echo( "Error creating cache file!" );
  else:
    insaneCache::showCacheFile( true );
  endif;
endif;

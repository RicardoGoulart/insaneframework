<?php

// This FW is awesome!
// Why?

// Lets define the page title for this link
insaneHeader::setTitle( "Welcome Page!" );

// Lets define an author
insaneHeader::setAuthor( "John Doe" );

// Lets set the language also.
insaneHeader::setLanguage( "pt", "pt-PT" );

// Lets define some keywords and a description
insaneHeader::setKeywords( "Keyword1, Keyword2, Keyword3" );
insaneHeader::setDescription( "Just a small description" );

// Lets pretend we need some style sheet that does not belong on the project... 
insaneHeader::addStyle( 
  // Define the URL
  "//maxdesign.com.au/jobs/css-layouts/10-example-layout-one-fixed/assets/css/styles.css",
  // Is it external ? Yes!
  true
);

// Lets add another one just because!
insaneHeader::addStyle( 
  // Define the URL
  "//a.amz.mshcdn.com/assets/posts-5566b7abfaeb93a59435209a1859b4dadda739890e225166ecef62390f985a79.css",
  // Is it external ? Yes!
  true
);

// Now lets add a style we have in the assets folder
insaneHeader::addStyle( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'testfile.css',
  // Is it external ? No!
  false,
  // We want more than just this file? No!
  false
);

// Lets add another one!
insaneHeader::addStyle( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'testfile2.css',
  // Is it external ? No!
  false,
  // We want more than just this file? No!
  false
);

// Now lets add both of them at the same time!
insaneHeader::addStyle( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'testfile.css',
  // Is it external ? No!
  false,
  // We want more than just this file? Yes!
  true
);
insaneHeader::addStyle( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'testfile2.css',
  // Is it external ? No!
  false,
  // We want more than just this file? Yes!
  true
);
// Both of these styles will came out as ONE.... (One requests loads both files)

// Lets also add some style of our own!
insaneHeader::addCode(
  "
    .someClassImadeUpJustNow {
      position: relative;
      float: left;
      width: 100%;
    }

    .anotherClass {
      display: none;
    }
  "
);
// This code will get minified automatically! Enjoy!

// Lets add another one, but with a twist!
insaneHeader::addCode(
  "/*@*/   /*  <------ Notice the first 5 chars! Very important! */

    /* Quick note also: All comments dissapear */
    .anotherMadeUpClass {
      position: relative;
      float: left;
      width: auto;

      /*!@ web */
        width: 100%;
      /*!@ end */

      /*!@ tablet */
        width: 50%;
      /*!@ end */

      /*!@ mobile */
        width: 33%;
      /*!@ end */

      /*!@ print */
        width: 100%;
      /*!@ end */
    }

    .lastOneIPromisse {
      display: none;

      /*!@ print */
        display: inline;
      /*!@ end */
    }
  "
);
// Can you get the point?
// Use it wiselly!
// And never forget to add the parser param on top of the css code /*@*/
// It *must* be on the VERY BEGINNING!!!!

// Lets set the current URL on the header... (Cannonical)
insaneHeader::setUrl( $_SERVER['REQUEST_URI'] );

// And last, we can print the header!
// Important: We can't addStyle after printing the header, but we can addCode! (Because it goes into the footer!)
insaneHeader::getHeader( true ); // "True", means: print... "false" just returns as a string.
?>

<h1>Hello World</h1>
<h3>Howdy Friends</h3>
<p><b>This is a WP translation:</b><br /><?=__( "My Key is awesome" );?></p>

<p>
  <?php
  
    $args = array(
      'post_name'   => 'pt-page-example',
      'post_type'   => 'post',
      'post_status' => 'publish',
      'numberposts' => 5
    );
    $my_posts = get_posts($args);
    var_dump( $my_posts );
    var_dump( get_page_by_path( 'pt-page-example' )  );
    var_dump( url_to_postid( site_url('page-example') ) );
    var_dump( get_posts(array
      (
          'post_name'   => 'pt-page-example',
          'post_type'   => 'page',
          'numberposts' => 5,
          'fields' => 'ids'
      ))
    );
    var_dump( get_post(  array_pop( get_posts(array
    (
        'post_name'   => 'pt-page-example',
        'post_type'   => 'page',
        'numberposts' => 5,
        'fields' => 'ids'
    )) ) ) );
    var_dump( get_permalink( get_page_by_path( 'pt-page-example' ) ) );
    
    $menu_name = 'top';
    $locations = get_nav_menu_locations();
    $menu_id = $locations[ $menu_name ] ;
    
    var_dump( 
      wp_get_nav_menu_object($menu_id),
      wp_get_nav_menu_items( wp_get_nav_menu_object($menu_id) )
    );

    var_dump( insaneRouter::$route );

  ?>
</p>

<?php

// We need jquery on this page.... Lets add it now... (If you want it in all pages, consider adding it inside assets/footer.php file, but remember you are in context inside the footer file, so when calling insaneFooter, you need to do: self:: instead!)
insaneFooter::addScript( "//code.jquery.com/jquery-3.2.1.min.js", true );

// Now lets add a style we have in the assets folder
insaneFooter::addScript( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'testfile.js',
  // Is it external ? No!
  false,
  // We want more than just this file? No!
  false
);

// Lets add another one!
insaneFooter::addScript( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'testfile2.js',
  // Is it external ? No!
  false,
  // We want more than just this file? No!
  false
);

// Now lets add both of them at the same time!
insaneFooter::addScript( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'testfile.js',
  // Is it external ? No!
  false,
  // We want more than just this file? Yes!
  true
);
insaneFooter::addScript( 
  // Define the URL
  dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'testfile2.js',
  // Is it external ? No!
  false,
  // We want more than just this file? Yes!
  true
);
// Both of these styles will came out as ONE.... (One requests loads both files)

// And Last but not least:
// Lets add some code... javascript code...
insaneFooter::addCode(
  "var rmg = 1, rr = 2;
  /* this is a small comment */
  function hellow(){
    console.log( 'best fw ever!' );
  }
  /*anotherComment*/
  hellow();"
);

insaneFooter::getFooter( true ); // Same here

<?php

insaneHeader::addStyle( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'testfile.css', false, true ); // Its internal
insaneHeader::addStyle( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'testfile2.css', false, true ); // Its internal
insaneHeader::addCode( "/*@*/
  .insaneBody { 
    background-color: blue; 
    /*!@ tablet */
      background-color: red;
    /*!@ end */
  }
" );

insaneHeader::setTitle( "Test page called CSS" );
insaneHeader::setAuthor( "TimWe" );
#insaneHeader::setEmail( "support.timwe@timwe.com" );
insaneHeader::setDescription( "This is a small page description... Can be the page excerpt" );
#insaneHeader::setKeywords( "CSS, Testing, Page" );
insaneHeader::setUrl( $_SERVER['REQUEST_URI'] );

insaneHeader::getHeader( true );
?>

<h1>CSS</h1>
<h1>JS</h1>



<?php

insaneFooter::addScript( "//code.jquery.com/jquery-3.2.1.min.js", true );
insaneFooter::addScript( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'testfile.js', false, true ); // Its internal
insaneFooter::addScript( dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'testfile2.js', false, true ); // Its internal
insaneFooter::addCode( "
var x = 123;
/***    ** * * * * */

" );
insaneFooter::getFooter( true );

#Falta o footer. Javascript.
#Depois posso fazer o subdominio CSS e JS.
#Depois, imagens. E JS para as imagens.
#Depois CURL requests insaneRequests::getUrlAsJson()
#depois paginas no WP
#depois artigos do WP dentro de categorias
#Depois Language (WP plugin)
#Depois User Login
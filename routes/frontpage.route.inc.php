<?php

// This FW is awesome!
// Why?

// Lets define the page title for this link
insaneHeader::setTitle( "Welcome Page!" );

// Lets define an author
insaneHeader::setAuthor( "John Doe" );

// Lets set the language also.
insaneHeader::setLanguage( "pt", "pt-PT" );

// Lets define some keywords and a description
insaneHeader::setKeywords( "Keyword1, Keyword2, Keyword3" );
insaneHeader::setDescription( "Just a small description" );

// Lets set the current URL on the header... (Cannonical)
insaneHeader::setUrl( $_SERVER['REQUEST_URI'] );

// And last, we can print the header!
// Important: We can't addStyle after printing the header, but we can addCode! (Because it goes into the footer!)
insaneHeader::getHeader( true ); // "True", means: print... "false" just returns as a string.
?>

<div class="wrapper">
  <header>

    <div id="header-carousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <img class="d-block img-fluid insaneImage" data-ii-url="//img.insane.fw/<?=strrev( base64_encode( '//placehold.it/2000x1000' ) );?>" data-ii-method="width" data-ii-quality="50" src="//img.insane.fw/<?=strrev( base64_encode( '//placehold.it/1999x999' ) );?>/<?=strrev( base64_encode( json_encode( array( 'calcWidth' => 100, 'method' => 'width', 'quality' => 10 ) ) ) )?>" alt="Card image cap" />
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid insaneImage" data-ii-url="//img.insane.fw/<?=strrev( base64_encode( '//placehold.it/2000x1000' ) );?>" data-ii-method="width" data-ii-quality="40" src="//img.insane.fw/<?=strrev( base64_encode( '//placehold.it/1999x999' ) );?>/<?=strrev( base64_encode( json_encode( array( 'calcWidth' => 100, 'method' => 'width', 'quality' => 10 ) ) ) )?>" alt="Card image cap" />
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid insaneImage" data-ii-url="//img.insane.fw/<?=strrev( base64_encode( '//placehold.it/2000x1000' ) );?>" data-ii-method="width" data-ii-quality="90" src="//img.insane.fw/<?=strrev( base64_encode( '//placehold.it/1999x999' ) );?>/<?=strrev( base64_encode( json_encode( array( 'calcWidth' => 100, 'method' => 'width', 'quality' => 10 ) ) ) )?>" alt="Card image cap" />
        </div>
      </div>

      <a title="<?=__( "&laquo; Previous" );?>" class="carousel-control-prev" href="#header-carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"><?=__( "&laquo; Previous" );?></span>
      </a>

      <a title="<?=__( "Next &raquo;" );?>" class="carousel-control-next" href="#header-carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"><?=__( "Next &raquo;" );?></span>
      </a>

    </div>

  </header>
</div>

<div class="concreteBackground">

  <div class="container pt-3">
    <div id="front-page-carousel-1" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">

        <div class="carousel-item active">
          <div class="card-group">
            <?php
              foreach( array( '', '' ,'' ) as $index => $serviceResponse ):
              ?>
                <div class="card">
                  <img class="card-img-top img-fluid insaneImage" data-ii-url="//img.insane.fw/<?=strrev( base64_encode( 'http://placehold.it/2000x1090' ) );?>" data-ii-method="width" data-ii-quality="50" src="//img.insane.fw/<?=strrev( base64_encode( 'http://placehold.it/1999x1089' ) );?>/<?=strrev( base64_encode( json_encode( array( 'calcWidth' => 50, 'method' => 'width', 'quality' => 1 ) ) ) )?>" alt="Card image cap" />
                  <div class="card-body">
                    <h5 class="card-title text-primary text-center">Card title</h5>
                    <p class="card-text text-center">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                  <!-- <div class="card-footer"> /-->
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  <!-- </div> /-->
                </div>
              <?php
              endforeach;
            ?>
          </div>
        </div>
        
      </div>

      <a title="<?=__( "&laquo; Previous" );?>" class="carousel-control-prev bg-primary" href="#front-page-carousel-1" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"><?=__( "&laquo; Previous" );?></span>
      </a>

      <a title="<?=__( "Next &raquo;" );?>" class="carousel-control-next bg-primary" href="#front-page-carousel-1" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"><?=__( "Next &raquo;" );?></span>
      </a>
      
    </div>
  </div>

  <?php
  insaneHeader::addCode(
    "
    #front-page-carousel-1 .carousel-control-prev, 
    #front-page-carousel-1 .carousel-control-next {
      width: 3%;
      min-width: 15px;
      opacity: 1;
      top: 20%;
      bottom: auto;
      padding-top: 1%;
      padding-bottom: 1%;
      font-weight: light;
    }

    #front-page-carousel-1 .carousel-control-prev {
      left: -1.5%;
    }

    #front-page-carousel-1 .carousel-control-next {
      right: -1.5%;
    }

    #front-page-carousel-1 .card {
      background-color: #EEE;
    }

    #front-page-carousel-1 .card .progress-bar {
      width: 0%;
    }

    #front-page-carousel-1 .card:hover .progress-bar {
      width: 100%;
    }

    #front-page-carousel-1 .card:hover {
      cursor: pointer;
      background-color: #FFF;

      -webkit-transition: all 550ms ease-in-out;
      -moz-transition: all 550ms ease-in-out;
      -ms-transition: all 550ms ease-in-out;
      -o-transition: all 550ms ease-in-out;
      transition: all 550ms ease-in-out;
    } 
    "
  );
  ?>

</div>

<?php
  insaneFooter::getFooter( true ); // Same here

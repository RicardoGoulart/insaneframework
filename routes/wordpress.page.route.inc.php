<?php

// This FW is awesome!
// Why?

// Lets define the page title for this link
insaneHeader::setTitle( "Welcome Page!" );

// Lets define an author
insaneHeader::setAuthor( "John Doe" );

// Lets set the language also.
insaneHeader::setLanguage( "pt", "pt-PT" );

// Lets define some keywords and a description
insaneHeader::setKeywords( "Keyword1, Keyword2, Keyword3" );
insaneHeader::setDescription( "Just a small description" );

// Lets set the current URL on the header... (Cannonical)
insaneHeader::setUrl( $_SERVER['REQUEST_URI'] );

// And last, we can print the header!
// Important: We can't addStyle after printing the header, but we can addCode! (Because it goes into the footer!)
insaneHeader::getHeader( true ); // "True", means: print... "false" just returns as a string.

$thisPost = insaneWordpress::getContent();
?>

<div class="container">
  <div class="wrapper p-3">
    <div class="row">
      <?php
        if( 1 || !empty( $thisPost->mainImage ) ):
          ?>
            <div class="col-12 col-sm-4">
              <img class="img-fluid insaneImage" 
                data-ii-url="//img.insane.fw/<?=strrev( base64_encode( 'http://placehold.it/2000x1090' ) );?>" 
                data-ii-method="width" 
                data-ii-quality="90" 
                src="//img.insane.fw/<?=strrev( base64_encode( 'http://placehold.it/2000x1090' ) );?>/<?=strrev( base64_encode( json_encode( array( 'calcWidth' => 100, 'method' => 'width', 'quality' => 40 ) ) ) )?>" 
                alt="PAGE IMAGE" />
            </div>
          <?php
        endif;
      ?>
      <div class="<?=( 1 || !empty( $thisPost->mainImage ) ? 'col-12 col-sm-8' : 'col-12')?>">
        <h1><?=$thisPost->post_title;?></h1>

        <p>
          <?=insaneWPContent::wpautop( $thisPost->post_content );?>
        </p>
      </div>
    </div>
  </div>
</div>
<?php

insaneFooter::getFooter( true ); // Same here

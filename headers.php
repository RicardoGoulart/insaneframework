<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-05 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneCache" ), __FILE__ );

// Set keep-alive header
header( "Connection: Keep-Alive", true );

header( "Accept-Ranges: bytes", true );

// Allow from all
header( "Access-Control-Allow-Origin: *", true );

// Set indexation
header( "Pragma: public" );

// Set default content type
header( "Content-Type: " . insaneConfig::CONTENT_TYPE . "; charset=utf-8", true );

// Set content compression
if( insaneCache::$gZip === true && insaneConfig::STAGE === true ): 
  // Here we say we are accepting encoding on the response
  header( "Vary: Accept-Encoding", true );
  // Here we say the type of encoding we send on the response
  header( "Content-Encoding: GZip", true );
endif;
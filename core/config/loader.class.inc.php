<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   0.1
 * @since     4.0.1
 */

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

class insaneLoader {
  /**
   * $destructSequence
   *
   * @var array
  */
  private static $destructSequence = array();
  
  /**
   * registerDependency
   *
   * @param mixed $classes
   * @return void
   */
  public static function registerDependency( $classes = array(), $provider = "" ){
    if( empty( $provider ) ):
      die( "You have an error in your code. You are trying to registerDependencies with no reference of the caller. You need to send the __FILE__ as the second parameter. Shame on you!" );
    endif;

    if( !empty( $classes ) ):
      foreach( $classes as $key => $val ):
        insaneLogger::log( "Cheking dependency class: " . $val . " for file: " . $provider );
        if( !class_exists( $val ) ):
          die( "Class was not found and is part of the requirements: " . $val . " on file: " . __FILE__ );
        endif;
      endforeach;
      return true;
    else:
      insaneLogger::log( "Dependencies registered were EMPTY!!!" );
      return false;
    endif;
  }

  /**
   * registerDestruct
   *
   * @param mixed $className
   * @param mixed $classFunction
   * @return void
   */
  public static function registerDestruct( $className = null, $classFunction = null ){
    if( !empty( $className ) && !empty( $classFunction ) ):
      array_push( self::$destructSequence, array( $className, $classFunction ) );
      return true;
    else:
      insaneLogger::log( "Failed to register a destructor. ClassName : " . $className . " and Function: " . $classFunction );
      return false;
    endif;
  }

  /**
   * terminate
   *
   * @return void
   */
  public static function terminate(){
    foreach( self::$destructSequence as $index => $function ):
      insaneLogger::log( "Terminating with script execution: " . $function[0] . "::" . $function[1] );
      call_user_func( $function );
    endforeach;
  }
}
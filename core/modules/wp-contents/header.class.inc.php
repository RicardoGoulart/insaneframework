<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;
  
// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneRouter", "insaneCSS", "insaneLoader" ), __FILE__ );

class insaneHeader {
  /**
   * $title
   *
   * @var string
   */
  public static $title = null;

  /**
   * $author
   *
   * @var string
   */
  public static $author = null;

  /**
   * $email
   *
   * @var string
   */
  public static $email = null;
  
  /**
   * $description
   *
   * @var string
   */
  public static $description = null;
  
  /**
   * $keywords
   *
   * @var string
   */
  public static $keywords = null;
  
  /**
   * $url
   *
   * @var string
   */
  public static $url = null;

  /**
   * $lang
   *
   * @var null
   */
  public static $lang = null;

  /**
   * $langEquiv
   *
   * @var null
   */
  public static $langEquiv = null;
  
  /**
   * $metaDefaults
   *
   * @var array
   */
  public static $metaDefaults = array();

  /**
   * $headerPath
   *
   * @var string
   */
  private static $headerPath = null;

  /**
   * $codes
   *
   * @var array
   */
  private static $codes = array();

  /**
   * $styles
   *
   * @var array
   */
  private static $styles = array();

  /**
   * registerPath
   *
   * @param mixed $path
   * @return void
   */
  public static function registerPath( $path = null ){
    if( !empty( $path ) ):
      self::$headerPath = $path . DIRECTORY_SEPARATOR;
      return true;
    else:
      insaneLogger::log( "No path was provided for insaneHeader context." );
      return false;
    endif;
  }

  /**
   * setLanguage
   *
   * @param mixed $code
   * @param mixed $equiv
   * @return void
   */
  public static function setLanguage( $code = null, $equiv = null ){
    self::$lang = $code;
    self::$langEquiv = $equiv;
  }

  /**
   * buildMetaDefaults
   *
   * @return void
   */
  private static function buildMetaDefaults(){
    self::$metaDefaults = array(
      array(
        'type' => 'http-equiv',
        'name' => 'Content-Type',
        'value' => 'text/html'
      ),
      array(
        'type' => 'http-equiv',
        'name' => 'Content-script-type',
        'value' => 'text/javascript'
      ),
      array(
        'type' => 'http-equiv',
        'name' => 'Content-style-type',
        'value' => 'text/css'
      ),
      array(
        'type' => 'http-equiv',
        'name' => 'Imagetoolbar',
        'value' => 'no'
      ),
      array(
        'type' => 'http-equiv',
        'name' => 'X-UA-Compatible',
        'value' => 'chrome=1;IE=edge'
      ),
      array(
        'type' => 'http-equiv',
        'name' => 'Content-Language',
        'value' => self::$langEquiv
      ),
      array(
        'type' => 'name',
        'name' => 'Apple-mobile-web-app-title',
        'value' => self::$title
      ),
      array(
        'type' => 'name',
        'name' => 'Title',
        'value' => self::$title
      ),
      array(
        'type' => 'name',
        'name' => 'DC.title',
        'value' => self::$title
      ),
      array(
        'type' => 'name',
        'name' => 'Custodian',
        'value' => self::$author
      ),
      array(
        'type' => 'name',
        'name' => 'Author',
        'value' => self::$author
      ),
      array(
        'type' => 'name',
        'name' => 'DC.creator',
        'value' => self::$author
      ),
      array(
        'type' => 'name',
        'name' => 'Copyright',
        'value' => self::$author . " &copy; " . date( "Y" )
      ),array(
        'type' => 'name',
        'name' => 'DC.publisher',
        'value' => self::$author
      ),
      array(
        'type' => 'name',
        'name' => 'DC.creator.address',
        'value' => self::$email
      ),
      array(
        'type' => 'name',
        'name' => 'DC.subject',
        'value' => self::$keywords
      ),
      array(
        'type' => 'name',
        'name' => 'Keywords',
        'value' => self::$keywords
      ),
      array(
        'type' => 'name',
        'name' => 'Description',
        'value' => self::$description
      ),
      array(
        'type' => 'name',
        'name' => 'DC.description',
        'value' => self::$description
      ),
      array(
        'type' => 'name',
        'name' => 'DC.Identifier',
        'value' => self::$url
      ),
      array(
        'type' => 'name',
        'name' => 'Robots',
        'value' => 'Index, Follow'
      ),
      array(
        'type' => 'name',
        'name' => 'Revisit-after',
        'value' => '1 Month'
      ),
      array(
        'type' => 'name',
        'name' => 'Rating',
        'value' => 'General'
      ),
      array(
        'type' => 'name',
        'name' => 'Distribution',
        'value' => 'Global'
      ),
      array(
        'type' => 'name',
        'name' => 'DC.date.created',
        'value' => '2018-01-01'
      ),
      array(
        'type' => 'name',
        'name' => 'DC.date.modified',
        'value' => date( "Y-m-d H:i:s" )
      ),
      array(
        'type' => 'name',
        'name' => 'DC.format',
        'value' => 'text/xhtml'
      ),
      array(
        'type' => 'name',
        'name' => 'DC.type',
        'value' => 'text.homepage.software'
      ),
      array(
        'type' => 'name',
        'name' => 'Viewport',
        'value' => 'width=device-width, minimum-scale=1, maximum-scale=3, user-scalable=yes, initial-scale=1'
      ),
      array(
        'type' => 'name',
        'name' => 'Apple-mobile-web-app-capable',
        'value' => 'yes'
      ),
      array(
        'type' => 'name',
        'name' => 'google-site-verification',
        'value' => ''
      ),
      
      array(
        'type' => 'property',
        'name' => 'og:title',
        'value' => self::$title
      ),
      array(
        'type' => 'property',
        'name' => 'og:description',
        'value' => self::$description
      ),
      //array(
      //    'type' => 'property',
      //    'name' => 'og:type',
      //    'value' => 'FefYMwhnJ0cFPp6ew3duQrcKWo2Xyg3YKbZYJMuLKsQ'
      //),
      array(
        'type' => 'property',
        'name' => 'og:url',
        'value' => ''
      ),
      array(
        'type' => 'property',
        'name' => 'og:image',
        'value' => ''
      )
    );
  }
  
  /**
   * setSubdomain
   *
   * @param mixed $param
   * @return void
   */
  public static function setSubdomain( $param = "" ){
      self::$subdomain = $param;
  }
  
  /**
   * setServerName
   *
   * @param mixed $param
   * @return void
   */
  public static function setServerName( $param = "" ){
      self::$servername = $param;
  }
  
  /**
   * setTitle
   *
   * @param mixed $param
   * @return void
   */
  public static function setTitle( $param = "" ){
      self::$title = $param;
  }
  
  /**
   * setAuthor
   *
   * @param mixed $param
   * @return void
   */
  public static function setAuthor( $param = "" ){
      self::$author = $param;
  }
  
  /**
   * setEmail
   *
   * @param mixed $param
   * @return void
   */
  public static function setEmail( $param = "" ){
      self::$email = $param;
  }
  
  /**
   * setKeywords
   *
   * @param mixed $param
   * @return void
   */
  public static function setKeywords( $param = "" ){
      self::$description = $param;
  }
  
  /**
   * setDescription
   *
   * @param mixed $param
   * @return void
   */
  public static function setDescription( $param = "" ){
      self::$keywords = $param;
  }
  
  /**
   * setUrl
   *
   * @param mixed $param
   * @return void
   */
  public static function setUrl( $param = "" ){
      self::$url = $param;
  }

  /**
   * addMetaParams
   *
   * @param mixed $params
   * @return void
   */
  public static function addMetaParams( $params = array() ){
    if( empty( self::$metaDefaults ) ):
        self::buildMetaDefaults();
    endif;
    $updated = false;
    foreach( self::$metaDefaults as $key => $val ):
        if( $val['name'] == $params['name'] ):
            self::$metaDefaults[$key] = $params;
            $updated = true;
            break;
        endif;
    endforeach;
    if( $updated == false ):
        self::$metaDefaults[] = $params;
    endif;
  }

  /**
   * getMetaTags
   *
   * @return void
   */
  private static function getMetaTags(){
    if( !empty( self::$metaDefaults ) ):
      $toRet = "";
      foreach( self::$metaDefaults as $key => $val ):
        if( !empty( $val['value'] ) ):
          $toRet .= '<meta ' . $val['type'] . '="' . $val['name'] . '" content="' . $val['value'] . '"/>';
        endif;                
      endforeach;
      return $toRet;
    else:
      self::buildMetaDefaults();
      return self::getMetaTags();
    endif;
  }

  /**
   * getHeader
   *
   * @param mixed $print
   * @return string|print
   */
  public static function getHeader( $print = false ){
    if( $print ):
      echo self::getHeaderHTML();
    else:
      return self::getHeaderHTML();
    endif;    
  }

  /**
   * getHeaderHTML
   *
   * @return string
   */
  private static function getHeaderHTML(){
    $h = "";
    ob_start();
      include_once( self::$headerPath . '_header.php' );
      $h = ob_get_contents();
    ob_end_clean();
    return $h;
  }

  /**
   * addCode
   *
   * @param mixed $code
   * @return void
   */
  public static function addCode( $code = null ){
    array_push( self::$codes, $code );
  }

  /**
   * addStyle
   *
   * @param mixed $url
   * @param mixed $external
   * @return void
   */
  public static function addStyle( $url = null, $external = false, $chain = true ){
    array_push( self::$styles, array( 'path' => $url, 'external' => $external, 'chain' => $chain ) );
  }

  /**
   * getCodes
   *
   * @return void
   */
  public static function getCodes(){
    // Not perfect, but does the job.
    if( !empty( self::$codes ) ):
      $buffer = '<style type="text/css">/*<![CDATA[*/';
      foreach( self::$codes as $key => $style ):
        $buffer .= insaneCSS::parseStyle( $style );
      endforeach;
      $buffer .= '/*]]>*/</style>';
      return $buffer;
    else:
      return "";
    endif;
  }

  /**
   * getStyles
   *
   * @return void
   */
  public static function getStyles( $lazy = false, $ie = false ){
    if( $ie ):
      $buffer = "<!--[if IE]>";
    elseif( $lazy ):
      $buffer = "<!--[if !IE]> -->";
    else:
      $buffer = "";
    endif;
    $internalBuffer = array();
    $chains = array();

    foreach( self::$styles as $key => $style ):
      if( $style['external'] === false ):
        if( $style['chain'] == true ):
          array_push( $chains, $style );
        else:
          array_push( $internalBuffer, $style );
        endif;
      else:
        $buffer .= '<link ' . (  $lazy ? 'lazyload="1"' : '' ) . ' rel="stylesheet" type="text/css" charset="utf-8" href="' . $style['path'] . '" media="all" />';
      endif;
    endforeach;

    foreach( $internalBuffer as $key => $style ):
      $path = insaneCSS::getCSSRoute( $style['path'], true );
      $buffer .= '<link ' . (  $lazy ? 'lazyload="1"' : '' ) . ' rel="stylesheet" type="text/css" charset="utf-8" href="' . $path . '" media="all" />';
    endforeach;
    
    foreach( $chains as $key => $chain ):
      $chains[$key]['link'] = insaneCSS::getCSSRoute( $chain['path'] );
    endforeach;

    $finalLink = insaneConfig::$cssProvider;
    foreach( $chains as $key => $chain ):
      $finalLink .= $chain['link'] . '/';
    endforeach;

    if( !empty( $chains ) ):
      $buffer .= '<link ' . (  $lazy ? 'lazyload="1"' : '' ) . ' rel="stylesheet" type="text/css" charset="utf-8" href="' . $finalLink . '" media="all" />';
    endif;

    if( $ie ):
      $buffer .= "<![endif]-->";
    elseif( $lazy ):
      $buffer .= "<!-- <![endif]-->";
    endif;
    return $buffer;
  }
}

new insaneHeader();

// Default stuff here
insaneHeader::setTitle( "Default Page" );
insaneHeader::setAuthor( "TimWe" );
insaneHeader::setEmail( "ricardo.goulart@timwe.com" );
insaneHeader::setDescription( "Page Description" );
insaneHeader::setKeywords( "Page, tags" );
insaneHeader::setUrl( $_SERVER['REQUEST_URI'] );
<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;
  
// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneRouter", "insaneCSS", "insaneJS" ), __FILE__ );

class insaneFooter {
  /**
   * $footerPath
   *
   * @var null
   */
  private static $footerPath = null;
  /**
   * $codes
   *
   * @var array
   */
  private static $codes = array();

  /**
   * $scripts
   *
   * @var array
   */
  private static $scripts = array();

  /**
   * registerPath
   *
   * @param mixed $path
   * @return void
   */
  public static function registerPath( $path = null ){
    if( !empty( $path ) ):
      self::$footerPath = $path . DIRECTORY_SEPARATOR;
      return true;
    else:
      insaneLogger::log( "No path was provided for insaneFooter context." );
      return false;
    endif;
  }

  /**
   * getFooter
   *
   * @param mixed $print
   * @return string|print
   */
  public static function getFooter( $print = false ){
    if( $print ):
      echo self::getFooterHTML();
    else:
      return self::getFooterHTML();
    endif;    
  }

  /**
   * getFooterHTML
   *
   * @return string
   */
  private static function getFooterHTML(){
    $h = "";
    ob_start();
      include_once( self::$footerPath . '_footer.php' );
      $h = ob_get_contents();
    ob_end_clean();
    return $h;
  }

  /**
   * addCode
   *
   * @param mixed $code
   * @return void
   */
  public static function addCode( $code = null ){
    array_push( self::$codes, $code );
  }

  /**
   * addScript
   *
   * @param mixed $url
   * @param mixed $external
   * @return void
   */
  public static function addScript( $url = null, $external = false, $chain = true ){
    array_push( self::$scripts, array( 'path' => $url, 'external' => $external, 'chain' => $chain ) );
  }

  /**
   * getCodes
   *
   * @return void
   */
  public static function getCodes(){
    // Not perfect, but does the job.
    if( !empty( self::$codes ) ):
      $buffer = '<script type="text/javascript" charset="utf-8">';
      foreach( self::$codes as $key => $script ):
        $buffer .= insaneJS::parseScript( $script );
      endforeach;
      return $buffer . '</script>';
    else:
      return "";
    endif;
  }

  /**
   * getStyles
   *
   * @return void
   */
  public static function getScripts(){
    $buffer = "";
    $internalBuffer = array();
    $chains = array();

    foreach( self::$scripts as $key => $script ):
      if( $script['external'] === false ):
        if( $script['chain'] == true ):
          array_push( $chains, $script );
        else:
          array_push( $internalBuffer, $script );
        endif;
      else:
        $buffer .= '<script type="text/javascript" charset="utf-8" src="' . $script['path'] . '"></script>';
      endif;
    endforeach;

    foreach( $internalBuffer as $key => $script ):
      $path = insaneJS::getJSRoute( $script['path'], true );
      $buffer .= '<script type="text/javascript" charset="utf-8" src="' . $path . '"></script>';
    endforeach;
    
    foreach( $chains as $key => $chain ):
      $chains[$key]['link'] = insaneJS::getJSRoute( $chain['path'] );
    endforeach;

    $finalLink = insaneConfig::$jsProvider;
    foreach( $chains as $key => $chain ):
      $finalLink .= $chain['link'] . '/';
    endforeach;

    if( !empty( $chains ) ):
      $buffer .= '<script type="text/javascript" charset="utf-8" src="' . $finalLink . '"></script>';
    endif;

    return $buffer;
  }
}

new insaneFooter();

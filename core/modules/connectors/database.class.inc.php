<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-07 Happy Sunday
 * @since     3.0.0
 * @version   4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger" ), __FILE__ );

class insaneDatabase {
  /**
   * $dbHost
   *
   * @var null
   */
  private static $dbHost = null;

  /**
   * $dbUser
   *
   * @var null
   */
  private static $dbUser = null;

  /**
   * $dbPass
   *
   * @var null
   */
  private static $dbPass = null;

  /**
   * $dbName
   *
   * @var null
   */
  private static $dbName = null;

  /**
   * $dbPort
   *
   * @var null
   */
  private static $dbPort = null;

  /**
   * $dbCSet
   *
   * @var null
   */
  private static $dbCSet = null;

  /**
   * $dbType
   *
   * @var null
   */
  private static $dbType = null;  

  /**
   * $instances
   *
   * @var undefined
   */
  private static $instances = array();

  /**
   * __construct
   *
   * @return void
   */
  public function __construct(){
    insaneLogger::log( "insaneCache Class initiated!" );
    if( insaneConfig::STAGE === true ):
      insaneLogger::log( "Database environment set to PROD." );
      self::$dbHost = insaneConfig::PROD_DB_HOST;
      self::$dbName = insaneConfig::PROD_DB_NAME;
      self::$dbUser = insaneConfig::PROD_DB_USER;
      self::$dbPass = insaneConfig::PROD_DB_PASS;
      self::$dbPort = insaneConfig::PROD_DB_PORT;
      self::$dbCSet = insaneConfig::PROD_DB_CSET;
      self::$dbType = insaneConfig::PROD_DB_TYPE;
    else:
      insaneLogger::log( "Database environment set to DEV." );      
      self::$dbHost = insaneConfig::DEV_DB_HOST;
      self::$dbName = insaneConfig::DEV_DB_NAME;
      self::$dbUser = insaneConfig::DEV_DB_USER;
      self::$dbPass = insaneConfig::DEV_DB_PASS;
      self::$dbPort = insaneConfig::DEV_DB_PORT;
      self::$dbCSet = insaneConfig::DEV_DB_CSET;
      self::$dbType = insaneConfig::DEV_DB_TYPE;
    endif;

    insaneLogger::log( "Starting a default connector." );
    array_push( self::$instances, self::createConnection() );
    insaneLogger::log( "Done." );    

    // Make sure we kill all pending connections before php execution ends and leaves 
    // some of them ON untill the default database timeout kicks in yo.
    insaneLoader::registerDestruct( __CLASS__, "closeConnection" );
  }

  /**
   * createConnection
   *
   * @param mixed $dbHost
   * @param mixed $dbType
   * @param mixed $dbName
   * @param mixed $dbUser
   * @param mixed $dbPass
   * @param mixed $dbPort
   * @param mixed $dbCSet
   * @return void
   */
  public static function createConnection( $dbHost = null, $dbType = null, $dbName = null, $dbUser = null, $dbPass = null, $dbPort = null, $dbCSet = null ){
    $useHost = !empty( $dbHost ) ? $dbHost : self::$dbHost;
    $useType = !empty( $dbType ) ? $dbType : self::$dbType;
    $useName = !empty( $dbName ) ? $dbName : self::$dbName;
    $useUser = !empty( $dbUser ) ? $dbUser : self::$dbUser;
    $usePass = !empty( $dbPass ) ? $dbPass : self::$dbPass;
    $usePort = !empty( $dbPort ) ? $dbPort : self::$dbPort;
    $useCSet = !empty( $dbCSet ) ? $dbCSet : self::$dbCSet;
    $opt = [
      PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
      PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
      PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    
    try
		{
      return new PDO( $useType . ':host=' . $useHost . ';dbname=' . $useName . ';port=' . $usePort . 'charset=' . $useCSet, $useUser, $usePass, $opt );
    } catch( PDOException $e ){
      die( $e->getMessage() );
    }
  }

  /**
   * query
   *
   * @param mixed $SQLQuery
   * @param mixed $instance
   * @return void
   */
  public static function query( $SQLQuery = "", $instance = null ){
    if( !empty( $SQLQuery ) ):
      $useInstance = 0;
      if( !empty( $instance ) ):
        if( isset( self::$instances[$instance] ) ):
          $useInstance = $instance;
        else:
          insaneLogger::log( "Database instance not found: " . $instance );
        endif;
      endif;

      $connector = self::$instances[$useInstance]->query( $SQLQuery );
      return $connector->fetchAll( PDO::FETCH_ASSOC );
		else:
			return array();
		endif;
  }

  /**
   * getOne
   *
   * @param mixed $SQLQuery
   * @param mixed $instance
   * @return void
   */
  public static function getOne( $SQLQuery = "", $instance = null ){
    return self::query( $SQLQuery . ' LIMIT 1', $instance );
  }

  /**
   * closeConnection
   *
   * @param mixed $instance
   * @return void
   */
  public static function closeConnection( $instance = null ){
    if( !empty( $instance ) ):
      // Close just this one
      self::$instances[$instance] = null;
    else:
      // Close all connections
      foreach( self::$instances as $index => $instance ):
        self::$instances[$index] = null;
      endforeach;
      self::$instances = array();
    endif;
    return true;
  }
}

new insaneDatabase();
<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;
  
// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger" ), __FILE__ );

class insaneRobots {
  /**
   * $path
   *
   * @var null
   */
  private static $path = null;

  /**
   * registerPath
   *
   * @param mixed $path
   * @return void
   */
  public static function registerPath( $path = null ){
    insaneLogger::log( "Starting Robots.txt" );
    if( !empty( $path ) ):
      self::$path = $path . DIRECTORY_SEPARATOR . "robots.txt";
      insaneLogger::log( "Robots bip bop!" );
      return true;
    else:
      insaneLogger::log( "There is no path to robots.txt!" );
      return false;
    endif;
  }

  /**
   * checkRobots
   *
   * @return void
   */
  public static function checkRobots(){
    $fileSize = 0;
    if( file_exists( self::$path ) ):
      insaneLogger::log( "Robots.txt path is valid. Checking integrity..." );
      $fileSize = filesize( self::$path );
    else:
      insaneLogger::log( "Robots.txt path does not exist. Creating a new one now!" );
    endif;

    $buffer = "Sitemap: " . $_SERVER['HTTP_HOST'] . "/" . insaneConfig::$sitemapURL . PHP_EOL;
    $buffer.= "User-Agent: *" . PHP_EOL;
    foreach( insaneConfig::$disallow as $folder ):
      $buffer.= "Disallow: /" . $folder . "/" . PHP_EOL;
    endforeach;

    if( strlen( $buffer ) != $fileSize ):
      insaneLogger::log( "Robots will be generated!" );
      file_put_contents( self::$path, $buffer );
      insaneLogger::log( "Robots were generated!" );
    else:
      insaneLogger::log( "Robots are up to date. (Compared file size, may not be accurate)" );
    endif;
  }
}
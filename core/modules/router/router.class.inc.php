<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;
  
// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneMinifier" ), __FILE__ );

class insaneRouter {
  /**
   * $languages
   *
   * @var boolean
   */
  private static $languages = false;

  /**
   * $grabbingAll
   *
   * @var boolean
   */
  private static $grabbingAll = false;

  /**
   * $routes
   *
   * @var array
   */
  public static $routes = array();

  /**
   * $route
   *
   * @var array
   */
  public static $route = array();

  /**
   * $path
   *
   * @var array
   */
  public static $path = array();

  /**
   * $currentURL
   *
   * @var string
   */
  public static $currentURL = "";

  /**
   * __construct
   *
   * @return void
   */
  public function __construct(){
    insaneLogger::log( "insaneRouter Class initiated!" );
    if( class_exists( "insaneLanguage" ) ):
      self::$languages = true;
    endif;

    // Add route here in array!
    if( !empty( $_SERVER['REQUEST_URI'] ) ):
      self::$path = explode( "/", $_SERVER['REQUEST_URI'] );
      if( class_exists( "insaneLanguage" ) ):
        $tmp = array();
        foreach( self::$path as $key => $val ):
          if( !empty( $val ) ):
            array_push( $tmp, $val );
          endif;  
        endforeach;
      endif;
    endif;

    self::$currentURL = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  }

  /**
   * registerPath
   *
   * @param mixed $path
   * @param mixed $template
   * @param mixed $cacheTime 3600
   * @return void
   */
  public static function register( $path = null, $template = null, $cacheTime = 3600 ){
    if( !array_key_exists( $path, self::$routes ) ):
      if( empty( $template ) ):
        insaneLogger::log( "Error while register path. No template given." );
        return false;
      endif;
      self::$routes[$path] = array(
        'template'  => $template,
        'cache'     => $cacheTime
      );
      return true;
    else:
      insaneLogger::log( "Path route is already present." );
      return false;
    endif;
  }

  /**
   * grabAll
   *
   * @param mixed $path
   * @return void
   */
  public static function grabAll( $path = null ){
    if( !empty( $path ) ):
      self::$route = self::$routes[$path];
      self::$route['router'] = $path;
      self::$grabbingAll = true;
      return true;
    else:
      insaneLogger::log( "Grab all expects a key to be valid!" );
      return false;
    endif;
  }

  /**
   * filter
   *
   * @return boolean
   */
  public static function filter(){
    if( class_exists( "insaneWordpress" ) ):
      insaneWordpress::parseRoutes();
    endif;
    
    $requestedURI = null;
    if( isset( $_SERVER['REQUEST_URI'] ) ):
      if( !empty( $_SERVER["REQUEST_URI"] ) ):
        $requestedURI = ltrim( $_SERVER["REQUEST_URI"], "/" );
      else:
        $requestedURI = false;
      endif;
    else:
      $requestedURI = false;
    endif;
    
    if( $requestedURI === false || $requestedURI == "" ):
      $requestedURI = "/";
    endif;

    // Check languages
    if( self::$languages === true ):
      $checkLanguage = explode( "/", $requestedURI );
      if( count( $checkLanguage ) >= 2 ):
        if( insaneLanguage::languageExists( $checkLanguage[0] ) ):
          // Lets define it as default (in use)
          insaneLanguage::setCurrentLanguage( $checkLanguage[0] );
          if( strstr( $requestedURI, "/" ) ):
            $_url = explode( "/", $requestedURI );
            $_url = !empty( $_url[1] ) ? $_url[1] : "/";
          else:
            $_url = $requestedURI;
          endif;
        else:
          // Not ok!
          return insaneLanguage::showDefault();
        endif;
      else:
        // No language defined in URL.... redirecting to default language. 
        return insaneLanguage::showDefault();
      endif;
    else:
      // Default method
      if( strstr( $requestedURI, "/" ) ):
        $_url = explode( "/", $requestedURI );
        $_url = !empty( $_url[0] ) ? $_url[0] : "/";
      else:
        $_url = $requestedURI;
      endif;
    endif;
    
    foreach( self::$routes as $key => $val ):
      if( $_url == $key ):
        $val["router"] = $key;
        self::$route = $val;
      endif;
    endforeach;

    if( empty( self::$route ) ):
      return false;
    else:
      return true;
    endif;
  }

  /**
   * show404
   *
   * @return void
   */
  public static function show404(){
    header( "insaneBOT: Redirect", true );
	  header( "Location: /404", true, 301 );
    exit();
  }

  /**
   * parseRoute
   *
   * @param mixed $relativeDir
   * @return void
   */
  public static function parseRoute( $relativeDir = null ){
    if( empty( $relativeDir ) ):
      insaneLogger::log( "No relative route define to be used." );
      return false;
    endif;
    self::$route['cache_validity'] = ( self::$route['cache_fileTime'] + self::$route['cache'] ) - time();
    self::$route['cache_expiry']   = self::$route['cache'] + self::$route['cache_fileTime'];

    ob_start();
      include_once( $relativeDir . DIRECTORY_SEPARATOR . self::$route["template"] );
      self::$route["template_contents"] = ob_get_contents();
    ob_end_clean();    

    if( !empty( self::$route['template_contents'] ) ):
      self::$route["cache_fileTime"]           = time();
      self::$route["cache_validity"]           = ( self::$route["cache_fileTime"] + self::$route["cache"] ) - time();
      self::$route["cache_expiry"]             = self::$route["cache_fileTime"] + self::$route["cache"];

      switch( insaneConfig::CONTENT_TYPE ):
        case 'text/css':
          //self::$route["template_contents_minified"]   = insaneMinifier::minifyCSS( self::$route["template_contents"] );
          self::$route["template_contents_minified"]   = self::$route["template_contents"];
          self::$route["template_contents_minified"]   .= "/* Server rr Cached from " . date('Y-m-d H:i:s') . " to " . date( "Y-m-d H:i:s", self::$route["cache_expiry"] ) . " */";
        break;
        case 'text/html':
          self::$route["template_contents_minified"]   = insaneMinifier::minifyHTML( self::$route["template_contents"] );
          self::$route["template_contents_minified"]   .= "<!-- Server rr Cached from " . date('Y-m-d H:i:s') . " to " . date( "Y-m-d H:i:s", self::$route["cache_expiry"] ) . " /-->";
        break;
        case 'application/javascript':
        case 'text/javascript':
          //self::$route["template_contents_minified"]   = insaneMinifier::minifyJS( self::$route["template_contents"] );
          self::$route["template_contents_minified"]   = self::$route["template_contents"];
          self::$route["template_contents_minified"]   .= "/* Server rr Cached from " . date('Y-m-d H:i:s') . " to " . date( "Y-m-d H:i:s", self::$route["cache_expiry"] ) . " */";
        break;
        default:
          self::$route['template_contents_minified'] = self::$route['template_contents'];
        break;
      endswitch;
    else:
      if( !self::$grabbingAll ):
        self::show404();
      else:
        return "";
      endif;
    endif;
  }

  /**
   * getContents
   *
   * @return string
   */
  public static function getContents(){
    return self::$route['template_contents_minified'];
  }

  /**
   * getSimpleRoute
   *
   * @param mixed $desiredPath
   * @return void
   */
  public static function getSimpleRoute( $desiredPath = null ){
    if( !empty( $desiredPath ) ):
      if( self::$languages === true ):
        return '//' . $_SERVER['HTTP_HOST'] . '/' . insaneLanguage::getCurrentLanguageKey() . "/" . $desiredPath;
      else:
        return '//' . $_SERVER['HTTP_HOST'] . '/' . $desiredPath;
      endif;
    else:
      insaneLogger::log( "No path provided for simpleRoutes." );
      return "";
    endif;
  }
}

new insaneRouter();
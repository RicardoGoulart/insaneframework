<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-03 Happy new Year!
 * @version   0.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger" ), __FILE__ );

class insaneMinifier {
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct(){
		insaneLogger::log( "insaneMinifier Class initiated!" );
	}
  /**
  * Removes excessive spaces and line-breaks present in the code.
  * @author       Ricardo Goulart <ricardo@goulart.pt>
  * @version      2.1
  * @param        string contents
  * @return       string|boolean
  */
  public static function minifyHTML( $contents = null ){
    if( !empty( $contents ) ):
			$contentSizeBefore = strlen( $contents );
			
      insaneLogger::log( "Minifying HTML contents..." );
      insaneLogger::log( "File size before: " . $contentSizeBefore . " characters." );
			$search = array(
				'/\>[^\S ]+/s',  // strip whitespaces after tags, except space
				'/[^\S ]+\</s',  // strip whitespaces before tags, except space
				'/(\s)+/s'       // shorten multiple whitespace sequences
			);

			$replace = array(
				'> ',
				' <',
				' '
			);

			$buffer = preg_replace( $search, $replace, $contents );

			// B
			$buffer = str_replace( "> </", "></", $buffer );
			//$buffer = str_replace( "> <", "><", $buffer );
      // E
      $contentSizeAfter = strlen( $buffer );
      insaneLogger::log( "File size after: " . $contentSizeAfter . " characters." );
      insaneLogger::log( "Compressing this file saved: " . ( $contentSizeBefore - $contentSizeAfter ) . " characters." );
      insaneLogger::log( "Compressing this file saved: " . ( ( $contentSizeAfter / 100 ) * $contentSizeBefore ) . "%." );
			return (string) $buffer;
    else:
      insaneLogger::log( "Attempt to minify content failed! Empty content." );
      return '/** No content **/';
    endif;
	}
	
	/**
  * Removes excessive spaces and line-breaks present in the code.
  * @author       Ricardo Goulart <ricardo@goulart.pt>
  * @version      2.1
  * @param        string contents
  * @return       string|boolean
  */
  public static function minifyCSS( $contents = null ){
    if( !empty( $contents ) ):
      $contentSizeBefore = strlen( $contents );

      insaneLogger::log( "Minifying CSS contents..." );
      insaneLogger::log( "File size before: " . $contentSizeBefore . " characters." );
      /* remove comments */
			$buffer = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $contents );
			/* remove tabs, spaces, newlines, etc. */
			$buffer = str_replace( array( "\r\n", "\r", "\n", "\t" ), ' ', $buffer );
			$buffer = preg_replace( '/(\s{2,})/' , ' ' , $buffer );
			/* remove @import links */
			$buffer = preg_replace( '/(@import[^;]+;)/' , '' , $buffer );
			$buffer = preg_replace( '/(\s{0,}(<|>)\s{0,})/' , '$2' , $buffer );
			$buffer = preg_replace( '/(\s{0,}({|})\s{0,})/' , '$2' , $buffer );
			$buffer = preg_replace( '/(\s{0,}(:)\s{0,})/' , '$2' , $buffer );
			$buffer = preg_replace( '/(\s{0,}(;)\s{0,})/' , '$2' , $buffer );
			$buffer = preg_replace( '/(\s{0,}(,)\s{0,})/' , '$2' , $buffer );
			$buffer = str_replace(array(';}'),array('}'), $buffer);
			$buffer = trim( $buffer );

			$contentSizeAfter = strlen( $buffer );
      insaneLogger::log( "File size after: " . $contentSizeAfter . " characters." );
      insaneLogger::log( "Compressing this file saved: " . ( $contentSizeBefore - $contentSizeAfter ) . " characters." );
			insaneLogger::log( "Compressing this file saved: " . ( ( $contentSizeBefore / 100 ) * $contentSizeAfter ) . "%." );
			
			return $buffer;
    else:
      insaneLogger::log( "Attempt to minify content failed! Empty content." );
      return '/** No content **/';
    endif;
	}
	
	public static function minifyJS( $contents = null ){
		if( !empty( $contents ) ):
			$contentSizeBefore = strlen( $contents );

      insaneLogger::log( "Minifying JS contents..." );
      insaneLogger::log( "File size before: " . $contentSizeBefore . " characters." );
      /* remove comments */
			
			// replace all multiple spaces by one space
			$buffer = preg_replace( '!\s+!', ' ' , $contents );
            // Remove comments
			$buffer = preg_replace( '/\/\*(\s*\r*\n*.*?)*\*\//', ' ' , $buffer );
			//$buffer = preg_replace( '/\/\/ .*/', ' ' , $buffer );

            // make it into one long line
			$buffer = str_replace( array("\n","\r"), '  ', $buffer );
            // replace all multiple spaces by one space
			$buffer = preg_replace( '!\s+!', ' ' , $buffer );

			$insideQuotes = array();
			$outsideQuotes = array();

			$tReg = str_replace( "$", "'", '/\\$[^$\\\\]*(?:\\\\.[^$\\\\]*)*\\$|\\"[^\\"\\\\]*(?:\\\\.[^\\"\\\\]*)*\\"/' );
			$saveAllAndReplace = preg_replace_callback(
					$tReg,
					function ( $matches ) use ( &$insideQuotes, &$outsideQuotes ){
							//array_push( $insideQuotes, $matches[0] );
							static $kk = 0;
							$insideQuotes[] = $matches[0];
							$toInsert = "!!==!!#REPLACE-$kk#!!==!!";
							$outsideQuotes[] = $toInsert;
							$kk++;
							return str_replace( $matches[0], $toInsert, $matches[0] );
					},
					$buffer
			);

			$saveAllAndReplace = preg_replace_callback(
					'_((\!|\@|\$|\%|\&|\/|\(|\)|\=|\?|\*|\<|\>|\-|\;|\}|\{|\,|\:|\+|\"|\d)\ (\!|\@|\$|\%|\&|\/|\(|\)|\=|\?|\*|\<|\>|\-|\;|\}|\{|\,|\:|\+|\"|\d))_',
					function ( $matches ){
							return str_replace( " ", "", $matches[0] );
					},
					$saveAllAndReplace
			);

			$saveAllAndReplace = preg_replace_callback(
					'_\ (\!|\@|\$|\%|\&|\/|\(|\)|\=|\?|\*|\<|\>|\-|\;|\}|\{|\,|\:|\+|\"|\d)\ _',
					function ( $matches ){
							return str_replace( " ", "", $matches[0] );
					},
					$saveAllAndReplace
			);

			$buffer = str_replace( $outsideQuotes, $insideQuotes, $saveAllAndReplace );

			$buffer = trim( $buffer );

			// B
			$rmg_nr = substr_count( $buffer, ';};' );
			if( $rmg_nr > 5 ):
					$rmg_buffer = "";
					$rmg_broken = explode( ';};', $buffer );
					$rmg_buffer[] = $rmg_broken[0];
					for( $rmg_i = 1; $rmg_i <= $rmg_nr; $rmg_i++ ):
							if( strlen( $rmg_buffer[count( $rmg_buffer )-1] ) < 200 ):
									$rmg_buffer[count( $rmg_buffer )-1] .= ";};" . $rmg_broken[$rmg_i];
							else:
									if( !in_array( substr( $rmg_buffer[count( $rmg_buffer )], -1 ), array( "-", "!" ) ) ):
											$rmg_buffer[] = "\n;};" . $rmg_broken[$rmg_i];
									else:
											$rmg_buffer[] = ";};" . $rmg_broken[$rmg_i];
									endif;
							endif;
					endfor;
					$buffer = implode( "", $rmg_buffer );
			endif;
			// E

			$contentSizeAfter = strlen( $buffer );
      insaneLogger::log( "File size after: " . $contentSizeAfter . " characters." );
      insaneLogger::log( "Compressing this file saved: " . ( $contentSizeBefore - $contentSizeAfter ) . " characters." );
			insaneLogger::log( "Compressing this file saved: " . ( ( $contentSizeBefore / 100 ) * $contentSizeAfter ) . "%." );
			
			return $buffer;
		else:
			insaneLogger::log( "Attempt to minify content failed! Empty content." );
      return '/** No content **/';
		endif;
	}
}

new insaneMinifier();
<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year
 * @since     3.0.0
 * @version   4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;

// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneDatabase" ), __FILE__ );

class insaneLanguage {
  /**
   * $languages
   *
   * @var array
   */
  public static $languages = array();

  /**
   * $current
   *
   * @var string
   */
  private static $current = "";

  /**
   * registerLanguage
   *
   * @param mixed $language
   * @return void
   */
  public static function registerLanguage( $language = null ){
    if( is_array( $language ) ):
      foreach( $language as $key => $lang ):
        if( is_array( $lang ) ):
          self::$languages[$lang[0]] = $lang;
          continue;
        else:
          self::$languages[$lang] = $language;
          break;
        endif;
      endforeach;
    else:
      insaneLogger::log( "You need to provide an array for the languages. ex.: array( 'pt', 'pt-PT' )" );
      return false;
    endif;
  }

  /**
   * showDefault
   *
   * @return void
   */
  public static function showDefault(){
    if( !empty( self::$languages ) ):
      // Get first language, because is default:
      $default = array_keys( self::$languages );
      header( "Location: /" . $default[0] . "/", true );
      header( "Insane-Reason: You need a default language dude...", true );
      exit();
    else:
      insaneLogger::log( "Cant redirect to language, because there is none defined" );
      return false;
    endif;
  }

  public static function languageExists( $language = null ){
    if( !empty( $language ) ):
      if( array_key_exists( $language, self::$languages ) ):
        return true;
      else:
        return false;
      endif;
    else:
      insaneLogger::log( "Can't check if language exists, because no language was provided to check!" );
      return false;
    endif;
  }

  /**
   * getDefaultLanguageKey
   *
   * @return void
   */
  public static function getDefaultLanguageKey(){
    if( !empty( self::$languages ) ):
      $piece = array_keys( self::$languages );
      return $piece[0];
    else:
      insaneLogger::log( "No languages defined, cant show the default!" );
    endif;
  }

  /**
   * getCurrentLanguageKey
   *
   * @return void
   */
  public static function getCurrentLanguageKey(){
    if( !empty( self::$current ) ):
      return self::$current;
    else:
      insaneLogger::log( "No languages defined, cant show the default!" );
    endif;
  }

  /**
   * setCurrentLanguage
   *
   * @param mixed $language
   * @return void
   */
  public static function setCurrentLanguage( $language = null ){
    if( !empty( $language ) ):
      if( array_key_exists( $language, self::$languages ) ):
        self::$current = $language;
        if( defined( "WPINC" ) ):
          switch_to_locale( self::$languages[$language][2] );
        endif;
        return true;
      else:
        return false;
      endif;
    else:
      insaneLogger::log( "No language passed in the arguments. Plz check!" );
      return false;
    endif;
  }
}

new insaneLanguage();
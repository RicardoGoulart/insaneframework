<?php
/**
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos da Licença Pública Geral GNU como publicada
 * pela Fundação do Software Livre (FSF); na versão 3 da Licença,
 * ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que possa ser útil,
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto
 * com este programa. Se não, veja <http://www.gnu.org/licenses/>.
 *
 * @author    Ricardo Goulart <ricardo@goulart.pt>
 * @date      2018-01-01 Happy new Year!
 * @version   4.1.1
 * @since     4.0.1
*/

if( !defined( "PROTECTED_SPEACH" ) ):
  die( "Unauthorized Insane Access! GTFO!" );
endif;
  
// Load dependencies
insaneLoader::registerDependency( array( "insaneConfig", "insaneLogger", "insaneDatabase", "insaneRouter" ), __FILE__ );

class insaneWordpress {
  /**
   * $wordpressDir
   *
   * @var null
   */
  private static $wordpressDir = null;

  /**
   * $route
   *
   * @var string
   */
  public static $route = "";

  /**
   * $path
   *
   * @var array
   */
  public static $path = array();

  /**
   * $thePost
   *
   * @var null
   */
  private static $thePost = null;
  
  /**
   * parseRoutes
   *
   * @return void
   */
  public static function parseRoutes(){
    if( !empty( insaneRouter::$path ) ):
      self::$path = array();
      foreach( insaneRouter::$path as $key => $val ):
        if( !array_key_exists( $val, insaneLanguage::$languages ) ):
          array_push( self::$path, $val );
        endif;
      endforeach;
      $route = implode( "/", self::$path );
      self::$route = trim( $route, "/" );
    endif;
  }

  /**
   * registerPath
   *
   * @param mixed $path
   * @return void
   */
  public final static function registerPath( $path = null ){
    if( !empty( $path ) ):
      if( file_exists( $path ) ):
        require_once $path;
      else:
        insaneLogger::log( "Oops, file was not found!" );
        return false;
      endif;
    else:
      insaneLogger::log( "Oops, no file path was provided..." );
      return false;
    endif;
  }

  /**
   * routeExists
   *
   * @return void
   */
  public final static function routeExists(){
    if( !empty( self::$route ) ):
      $getContent = get_page_by_path( self::$route );
      if( $getContent ): 
        return true;
      else: 
        $getContent = query_posts( 'name=' . self::$route );
        if( $getContent ): 
          $getContent = array_shift( $getContent );
          return true;
        else: 
          return false;
        endif;
      endif;
    else:
      return false;
    endif;
  }

  /**
   * getContent
   *
   * @return void
   */
  public final static function getContent(){
    if( !empty( self::$route ) ):
      $getContent = get_page_by_path( self::$route );
      if( $getContent ): 
        self::$thePost = $getContent;
        return $getContent;
      else: 
        $getContent = query_posts( 'name=' . self::$route );
        if( $getContent ): 
          $getContent = array_shift( $getContent );
          self::$thePost = $getContent;
          return $getContent;
        else: 
          return false;
        endif;
      endif;
    else:
      return false;
    endif;
  }

/**
 * getMenu
 *
 * @param mixed $name
 * @return void
 */
  public final static function getMenu( $name = null ){
    if( !empty( $name ) ):
      $menu_name = $name;
      $locations = get_nav_menu_locations();
      $menu_id = $locations[ $menu_name ] ;
      $object =  wp_get_nav_menu_object($menu_id);
      $prepareReturn = (object) array( 
        'menu' => $object,
        'items' => wp_get_nav_menu_items( $object ),
        'locations' => $locations,
        'name' => rand( 0, 999 ) . rand( 0, 999 ) . rand( 0, 999 ) . md5( $name )
      );
      if( $prepareReturn->items ):
        _wp_menu_item_classes_by_context( $prepareReturn->items );
        $searchParent = null;
        foreach( $prepareReturn->items as $k => $v ):
          if( !$v->children ):
            $prepareReturn->items[$k]->children = array();
          endif;

          if( $v->menu_item_parent > 0 ):
            $searchParent = $v;
            unset( $prepareReturn->items[$k] );
          else:
            $searchParent = null;
          endif;

          if( !empty( $searchParent ) ):
            foreach( $prepareReturn->items as $rk => $rv ):
              if( $rv->ID == $searchParent->menu_item_parent ):
                $prepareReturn->items[$rk]->children[] = $v;
                $searchParent = null;
              endif;
            endforeach;
          endif;
        endforeach;
      endif;
      return $prepareReturn;
    else:
      return false;
    endif;
    
  }

  /**
   * urlMatch
   *
   * @return void
   */
  public static function urlMatch( $item = null ){
    // verifica se post belongs
    if( empty( self::$thePost ) ):
      self::getContent();
    endif;
    
    if( isset( $item->object_id ) ):
      if( $item->object_id == self::$thePost->ID ):
        // é um Post
        return true;
      endif;

      if( $item->url == insaneRouter::$currentURL ):
        return true;
      endif;

      // cat.
      $cat = get_the_category( self::$thePost );
      if( !empty( $cat ) ):
        foreach( $cat as $k => $c ):
          if( $c->cat_ID == $item->object_id ):
            return true;
          endif;
        endforeach;
      endif;
    endif;    
  }
}

new insaneWordpress();